/**
 * Project: apollo-web-springmvc
 * 
 * File Created at 2016年6月3日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.web.system.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.dx.pf.dal.anno.Table;

/**
 * @ClassName: Menu
 * @Description: 菜单实体
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年6月3日 上午9:10:50
 * @version V1.0
 */
@Table("t_menu")
public class Menu implements Serializable, Comparable<Menu> {

	private static final long serialVersionUID = 7686564936948823660L;
	// 菜单的主键Id
	private long pk_id;
	// 菜单的父菜单Id
	private long parentId;
	// 菜单名称
	private String name;
	// 菜单域名
	private String domainName;
	// 菜单的链接
	private String href;
	// 菜单图标
	private String icon;
	// 菜单排序
	private Integer sort;
	// 是否在菜单中显示[true显示，false不显示]
	private boolean isShow;
	// 所属聚合分类，多个分类以“，”分隔
	private String scope;
	// 子菜单列表
	private List<Menu> childMenu;
	// 菜单中的数据
	private Map<Object, Object> map;
	// 菜单全路径如：/123456987/136548545/4478545558
	private String menuFullPath;

	/**
	 * @return the pk_id
	 */
	public long getPk_id() {
		return pk_id;
	}

	/**
	 * @param pk_id
	 *            the pk_id to set
	 */
	public void setPk_id(long pk_id) {
		this.pk_id = pk_id;
	}

	/**
	 * @return the parentId
	 */
	public long getParentId() {
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName
	 *            the domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href
	 *            the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @return the sort
	 */
	public Integer getSort() {
		return sort;
	}

	/**
	 * @param sort
	 *            the sort to set
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}

	/**
	 * @return the isShow
	 */
	public boolean isShow() {
		return isShow;
	}

	/**
	 * @param isShow
	 *            the isShow to set
	 */
	public void setShow(boolean isShow) {
		this.isShow = isShow;
	}

	/**
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * @param scope
	 *            the scope to set
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * @return the childMenu
	 */
	public List<Menu> getChildMenu() {
		return childMenu;
	}

	/**
	 * @param childMenu
	 *            the childMenu to set
	 */
	public void setChildMenu(List<Menu> childMenu) {
		this.childMenu = childMenu;
	}

	/**
	 * @return the map
	 */
	public Map<Object, Object> getMap() {
		return map;
	}

	/**
	 * @param map
	 *            the map to set
	 */
	public void setMap(Map<Object, Object> map) {
		this.map = map;
	}

	/**
	 * @return the menuFullPath
	 */
	public String getMenuFullPath() {
		return menuFullPath;
	}

	/**
	 * @param menuFullPath
	 *            the menuFullPath to set
	 */
	public void setMenuFullPath(String menuFullPath) {
		this.menuFullPath = menuFullPath;
	}

	// 比较器的实现
	public int compareTo(Menu menu) {
		if (menu != null && menu.getSort() != null && sort != null) {
			return menu.getSort() - sort;
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj != null){
			Menu menu = (Menu)obj;
			if(menu.getPk_id() != 0l){
				return menu.getPk_id() == getPk_id();
			}
		}
		return super.equals(obj);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return super.toString();
	}

	
}
