/**
 * Project: apollo-web-springmvc
 * 
 * File Created at 2016年6月3日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.web.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** 
* @ClassName: MenuController 
* @Description: 菜单管理
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年6月3日 上午9:09:17 
* @version V1.0 
*/
@Controller
@RequestMapping(value="/menu")
public class MenuController {

}
