/**
 * Project: apollo-web-springmvc
 * 
 * File Created at 2016年7月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.web.system.entity;

import java.io.Serializable;

/**
 * @ClassName: JsonResult
 * @Description:封装json结果集
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年7月5日 上午11:11:51
 * @version V1.0
 */
public class JsonResult implements Serializable {

	private static final long serialVersionUID = 2678688889584208510L;

	private Boolean success = false;// 返回是否成功

	private String msg = "";// 返回信息

	private Object obj = null;// 返回其他对象信息

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
}
