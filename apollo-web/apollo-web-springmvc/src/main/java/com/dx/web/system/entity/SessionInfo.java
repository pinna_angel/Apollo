/**
 * Project: apollo-web-springmvc
 * 
 * File Created at 2016年7月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.web.system.entity;

import java.util.List;

/**
 * @ClassName: SessionInfo
 * @Description: 为了统一管理session中，用户自定义变量，所以定义这个类，</br>
 *               将所有用户要存放在session中的信息，全都存放在这里，便于管理.</br>
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年7月5日 上午11:10:24
 * @version V1.0
 */
public class SessionInfo {

	private List<String> permissionUrls = null;// 这个列表记录用户可以访问的url集合

	public List<String> getPermissionUrls() {
		return permissionUrls;
	}

	public void setPermissionUrls(List<String> permissionUrls) {
		this.permissionUrls = permissionUrls;
	}
}
