/**
 * Project: apollo-web-springmvc
 * 
 * File Created at 2016年7月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.web.system.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ControllerAop
 * @Description: 用于拦截所有@RequestMapping注解的方法，也就是Controller的方法.</br>
 *               将方法名称、传递的参数、返回值、IP等等信息存放在数据库表中。</br>
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年7月5日 上午11:16:04
 * @version V1.0
 */
@Aspect
@Component
public class ControllerAop {

//	@Resource(name = "controllerLogService")
//	private ControllerLogService controllerLogService;

	@AfterReturning(value = "@annotation(org.springframework.web.bind.annotation.RequestMapping)", argNames = "returnValue", returning = "returnValue")
	public void afterInsertMethod(JoinPoint joinPoint, Object returnValue) {
		Signature signature = joinPoint.getSignature();

		if (signature.getDeclaringTypeName().equals("sy.controller.aop.ControllerLogController")) {// 这个类的方法不需要记录日志

			return;
		}
		
//		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
//		ControllerLog controllerLog = new ControllerLog();
//		controllerLog.setIp(IpUtil.getIp(request));// 哪个IP访问的方法


		// System.out.println("类型：" + signature.getDeclaringType());


		// System.out.println("类型名" + signature.getDeclaringTypeName());


//		controllerLog.setClassName(signature.getDeclaringTypeName());

		// System.out.println("修改器：" + signature.getModifiers());


		// System.out.println("方法名称:" + signature.getName());


//		controllerLog.setMethodName(signature.getName());
		// System.out.println("方法全名：" + signature.toLongString());


//		controllerLog.setMethodFullName(signature.toLongString());
		// System.out.println("方法短名：" + signature.toShortString());




//		MethodSignature methodSignature = (MethodSignature) signature;
//		Method method = methodSignature.getMethod();
//		MethodName methodNameAnnotation = method.getAnnotation(MethodName.class);
//		if (methodNameAnnotation != null) {
//			// System.out.println("中文方法名：" + methodNameAnnotation.name());
//
//
//			controllerLog.setMethodCnName(methodNameAnnotation.name());
//		} else {
//			controllerLog.setMethodCnName("方法上并没有添加@MethodName(name = '中文方法名')的注解");
//		}
//
//		List<Object> argsList = new ArrayList<Object>();
//		JsonPropertyFilter filter = new JsonPropertyFilter();
//		for (int i = 0; i < joinPoint.getArgs().length; i++) {
//			Object arg = joinPoint.getArgs()[i];
//			if (null != arg) {
//				// String argClassName = arg.getClass().getSimpleName();
//
//
//				if (arg instanceof HttpServletResponse) {
//					argsList.add("HttpServletResponse");
//				} else if (arg instanceof HttpServletRequest) {
//					argsList.add("HttpServletRequest");
//				} else if (arg instanceof HttpSession) {
//					argsList.add("HttpSession");
//				} else {
//					argsList.add(arg);
//				}
//			}
//		}
//		// System.out.println("参数：" + JSON.toJSONString(argsList, filter, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.DisableCircularReferenceDetect));
//
//
//		controllerLog.setArgsContent(JSON.toJSONString(argsList, filter, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.DisableCircularReferenceDetect));
//
//		// System.out.println("返回值：" + JSON.toJSONString(returnValue, filter, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.DisableCircularReferenceDetect));
//
//
//		controllerLog.setReturnValue(JSON.toJSONString(returnValue, filter, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.DisableCircularReferenceDetect));
//		controllerLogService.save(controllerLog);
	}


}
