/**
 * Project: apollo-base-redisClient
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient.transaction;

/** 
* @ClassName: RedisTransaction 
* @Description: redis事务接口 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午9:12:39 
* @version V1.0 
*/
public interface RedisTransaction {
	/**
	 * 开启事务
	 */
	public void startTransaction();

	/**
	 * 提交事务
	 */
	public void commitTransaction();

	/**
	 * 回滚事务
	 */
	public void rollbackTransaction();

}
