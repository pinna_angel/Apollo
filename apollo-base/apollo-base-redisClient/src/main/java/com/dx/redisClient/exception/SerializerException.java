/**
 * Project: dx.redisClient
 * 
 * File Created at 2016年4月22日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient.exception;

/** 
* @ClassName: SerializerException 
* @Description: 序列化异常
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月22日 下午5:36:18 
* @version V1.0 
*/
public class SerializerException extends Exception {

	private static final long serialVersionUID = -7059307642777098969L;

	public SerializerException(){}
	
	/**
	 * @param message
	 */
	public SerializerException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public SerializerException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SerializerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public SerializerException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
