/**
 * Project: dx.redisClient
 * 
 * File Created at 2016年4月22日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient.constants;

/** 
* @ClassName: DateType 
* @Description: redis的基本数据类型枚举 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月22日 下午5:19:48 
* @version V1.0 
*/
public enum DateType {

	string("string"), 
	list("list"), 
	set("set"), 
	zset("zset"), 
	hash("hash"),
	Geo("Geo");

	private String value;

	private DateType(String typeStr) {
		this.value = typeStr;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
