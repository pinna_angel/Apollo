/**
 * Project: dx.redisClient
 * 
 * File Created at 2016年4月22日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient;

import redis.clients.jedis.JedisPool;

/** 
* @ClassName: JedisClient 
* @Description: Jedis客户端 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月22日 下午5:13:07 
* @version V1.0 
*/
public class JedisClient {

	public static final Integer DEFAULT_EXPIRE_TIME = 60 * 60 * 24;
	
	private JedisPool jedisPool;

	private JedisClient() {};

}
