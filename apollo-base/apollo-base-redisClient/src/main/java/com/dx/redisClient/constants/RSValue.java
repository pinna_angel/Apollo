/**
 * Project: dx.redisClient
 * 
 * File Created at 2016年4月23日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient.constants;

/** 
* @ClassName: RSValue 
* @Description: 返回结果的常用常量
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月23日 上午9:57:36 
* @version V1.0 
*/
public final class RSValue {

	public static final String OK = "OK";

	public static final String nuli = "null";
}
