/**
 * Project: dx.redisClient
 * 
 * File Created at 2016年4月22日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient.exception;

/** 
* @ClassName: RedisClientException 
* @Description: redis客户端异常定义 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月22日 下午5:37:48 
* @version V1.0 
*/
public class RedisClientException extends Exception{
	
	private static final long serialVersionUID = 4960771175469448119L;

	public RedisClientException(){}
	
	/**
	 * @param message
	 */
	public RedisClientException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public RedisClientException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RedisClientException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RedisClientException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
