/**
 * Project: dx.redisClient
 * 
 * File Created at 2016年4月26日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient.exception;

/** 
* @ClassName: ClusterException 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月26日 下午3:34:11 
* @version V1.0 
*/
public class ClusterException  extends RuntimeException {
	
    public ClusterException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClusterException(String message) {
        super(message);
    }
}
