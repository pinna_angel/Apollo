/**
 * Project: dx.redisClient
 * 
 * File Created at 2016年4月22日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient.constants;

/** 
* @ClassName: ValueType 
* @Description: 定义value数据的类型，序列化，json串,压缩后的数据
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月22日 下午5:22:44 
* @version V1.0 
*/
public enum ValueType {
	serializer,
	protobuf,
	gzip,
	json;
}
