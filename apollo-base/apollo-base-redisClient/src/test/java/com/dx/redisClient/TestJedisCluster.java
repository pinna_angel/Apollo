/**
 * Project: apollo-base-redisClient
 * 
 * File Created at 2016年4月29日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.redisClient;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Before;
import org.junit.Test;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

/** 
* @ClassName: TestJedisCluster 
* @Description: redis jedis 的集群测试
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月29日 下午8:40:33 
* @version V1.0 
*/
public class TestJedisCluster {


	public static JedisCluster jedisCluster;

	public static String REDIS_CONFIG_FILE_NAME = "redis.properties";
	public static Properties prop;

	// hostAndPort
	private static Pattern pattern = Pattern.compile("^.+[:]\\d{1,5}\\s*$");
	public static String addressKeyPrefix = "";
	
	/**
	 * initSize:可选，指定连接池初始连接数,默认为10
		maxSize:可选，指定连接池最大连接数,默认为100
		minIdleSize:可选，指定连接池最小空闲连接数,默认为10
		maxIdleSize:可选，指定连接池最大空闲连接数,默认为20
	 */
	// #pool
	public static int maxWaitMillis = -1;
	public static int maxTotal = 1000;
	public static int minIdle = 8;
	public static int maxIdle = 100;
	// reids
	public static int timeout = 300000;
	public static int maxRedirections = 6;//应该大于等于主节点数

	@Before
	public void init() {
		long time1 = System.nanoTime();
		System.out.println("init jedis client...");
		try {
			initParma();
			jedisCluster = new JedisCluster(parseHostAndPort(), timeout, maxRedirections, initConnectionPool());
		} catch (Exception e) {
			e.printStackTrace();
		}
		long time2 = System.nanoTime();
		System.out.print("run time :" + (time2 - time1) + "(ns) , ");
		System.out.println("finish init jedis client,you can do something you like...");
	}

	@Test
	public void test1() {
		System.out.println(jedisCluster.getClusterNodes());
	}

	@Test
	public void setKeytoCluster(){
		for (int i = 0; i < 6; i++) {
			long id= System.currentTimeMillis();
			String key = "id:" + id;
			String value = "test cluster,give some suprise,hah...";
			jedisCluster.setex(key, 300, value);
		}
		System.out.println("exeute finish flush all");
	}
	
	@Test
	public void CleanCluster() {
		jedisCluster.flushAll();
		System.out.println(" finish flush all");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 获取配置文件信息初始化
	 * 
	 * @throws Exception
	 */
	private static void initParma() throws Exception {
		String path = getResourcePath(REDIS_CONFIG_FILE_NAME);
		initLoadproperties(path);
		addressKeyPrefix = prop.getProperty("addressKeyPrefix") != null ? prop.getProperty("addressKeyPrefix") : addressKeyPrefix;
		maxWaitMillis = prop.getProperty("pool.maxWaitMillis") != null ? Integer.valueOf(prop.getProperty("pool.maxWaitMillis")) : maxWaitMillis;
		maxTotal = prop.getProperty("pool.maxTotal") != null ? Integer.valueOf(prop.getProperty("pool.maxTotal")) : maxTotal;
		minIdle = prop.getProperty("pool.minIdle") != null ? Integer.valueOf(prop.getProperty("pool.minIdle")) : minIdle;
		maxIdle = prop.getProperty("pool.maxIdle") != null ? Integer.valueOf(prop.getProperty("pool.maxIdle")) : maxIdle;
		timeout = prop.getProperty("redis.timeout") != null ? Integer.valueOf(prop.getProperty("redis.timeout")) : timeout;
		maxRedirections = prop.getProperty("redis.maxRedirections") != null ? Integer.valueOf(prop.getProperty("redis.maxRedirections")) : maxRedirections;
	}

	/**
	 * 获取连接池
	 * @return
	 */
	private static GenericObjectPoolConfig initConnectionPool() {
		JedisPoolConfig genericObjectPoolConfig = new JedisPoolConfig();
//		GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
		genericObjectPoolConfig.setMaxWaitMillis(maxWaitMillis);
		genericObjectPoolConfig.setMaxTotal(maxTotal);
		genericObjectPoolConfig.setMinIdle(minIdle);
		genericObjectPoolConfig.setMaxIdle(maxIdle);
		genericObjectPoolConfig.setTestOnBorrow(true); 
		genericObjectPoolConfig.setTestOnReturn(true);
		genericObjectPoolConfig.setTestWhileIdle(true);
		return genericObjectPoolConfig;
	}

	/**
	 * 获取host
	 * @return
	 * @throws Exception
	 */
	private static Set<HostAndPort> parseHostAndPort() throws Exception {
		try {
			Set<HostAndPort> haps = new HashSet<HostAndPort>();
			for (Object key : prop.keySet()) {
				if (!((String) key).startsWith(addressKeyPrefix)) {
					continue;
				}
				String val = (String) prop.get(key);
				boolean isIpPort = pattern.matcher(val).matches();
				if (!isIpPort) {
					throw new IllegalArgumentException("ip 或 port 不合法");
				}
				String[] ipAndPort = val.split(":");
				HostAndPort hap = new HostAndPort(ipAndPort[0], Integer.parseInt(ipAndPort[1]));
				haps.add(hap);
			}
			return haps;
		} catch (IllegalArgumentException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new Exception("解析 jedis 配置文件失败", ex);
		}
	}

	/**
	 * 设置配置文件路径，如果路径为空直接返回失败
	 * @param configFilePath
	 * @throws Exception
	 */
	public static void initLoadproperties(String configFilePath) throws Exception {
		FileInputStream fis = null;
		Reader fReader = null;
		try {
			// fis = new FileInputStream(configFilePath+"/"+REDIS_CONFIG_FILE_NAME);
			fis = new FileInputStream(configFilePath);
			fReader = new InputStreamReader(fis, "UTF-8");
			Properties properties = new Properties();
			properties.load(fReader);
			prop = properties;
		} catch (Exception e) {
			throw new Exception("jedis初始化失败，请检查配置文件路径。", e);
		} finally {
			if (fReader != null) {
				fReader.close();
			}
			if (fis != null) {
				fis.close();
			}
		}
	}

	/**
	 * 获取java maven项目下的resource中的文件
	 * @param path
	 * @return
	 */
	public static String getResourcePath(String path) {
		// System.out.println(this.getClass().getClassLoader().getResourceAsStream("/resource/redis.properties"));
		// System.out.println(Thread.currentThread().getContextClassLoader().getResource("redis.properties").getPath());
		return Thread.currentThread().getContextClassLoader().getResource(path).getPath();
	}

	/**
	 * 当前的路径
	 * @param clazz
	 * @return
	 */
	public static String getCurrentPath(Class<?> clazz) {
		String path = "";
		try {
			path = clazz.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		} catch (Exception e) {
			path = clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
		}
		path = path.replaceFirst("file:/", "");
		path = path.replaceAll("!/", "");
		path = path.substring(0, path.lastIndexOf("/"));
		if (path.substring(0, 1).equalsIgnoreCase("/")) {
			String osName = System.getProperty("os.name").toLowerCase();
			if (osName.indexOf("window") >= 0) {
				path = path.substring(1);
			}
		}
		return path;
	}

}
