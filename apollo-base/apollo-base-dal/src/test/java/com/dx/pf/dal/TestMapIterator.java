/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal;

import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.dx.pf.dal.entity.User;

/** 
* @ClassName: TestMapIterator 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月5日 上午10:37:03 
* @version V1.0 
*/
public class TestMapIterator {
	private static List<String> list = new ArrayList<String>();
	
	private static Map<String,String> map = new ConcurrentHashMap<String,String>(); 
	
	
	private static void init() {
		for (int i = 0; i < 2000; i++) {
			list.add("key" + i);
			map.put("key" + i, "value" + i);
		}
	}
	
	public static void main(String[] args) {
		init();
		
		long time11 = System.nanoTime();
		for (String str : list) {
			String key = str;
			String value = map.get(str); 
		}
		long time12 = System.nanoTime();

		System.out.println(time12 - time11);

		long time21 = System.nanoTime();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue(); 
		}
		long time22 = System.nanoTime();

		System.out.println(time22 - time21);
		
		long time31 = System.nanoTime();
		for (Iterator ite = map.entrySet().iterator() ;ite.hasNext(); ) {
			Map.Entry<String,String> entry = (Entry<String, String>) ite.next();
			String key = entry.getKey();
			String value = entry.getValue();
		}
		long time32 = System.nanoTime();
		System.out.println(time32 - time31);
	}
}

