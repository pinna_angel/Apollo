/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月6日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.sql;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.dx.pf.dal.constants.Operator;
import com.dx.pf.dal.constants.OrderBy;
import com.dx.pf.dal.entity.Person;
import com.dx.pf.dal.jdbc.sql.Condition;
import com.dx.pf.dal.jdbc.sql.SqlHelper;

/** 
* @ClassName: SqlHelper 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月6日 上午11:24:51 
* @version V1.0 
*/
public class SqlHelperTest {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
//		StringBuilder sql= new StringBuilder(128);
//		System.out.println(sql.length());
		//SqlHelper.newInstance().select(Person.class).setSeleteFields(new String[]{}).builder().
		
		for (int i = 0; i < 10; i++) {
		
		try {
		Condition condi1 = new Condition(new String[]{"name","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
		Condition condi2 = new Condition(new String[]{"name0","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
		condi1.or(condi2);
		long time11 = System.nanoTime();
		SqlHelper sqlHelper_s = SqlHelper.newInstance().select(Person.class)
				.setSeleteFields(new String[]{})
				.setWhere(condi1)
				.setPage(1, 20)
				.addOrderBy("pk_id", OrderBy.DESC).addOrderBy("age", OrderBy.ASC)
				.builder();
		System.out.println(sqlHelper_s.toString());
		Map<String,Object> paramMap = sqlHelper_s.getNamedPerprotyParamMap();
		for (Iterator<Entry<String, Object>> itr = paramMap.entrySet().iterator(); itr.hasNext();) {
			Entry<String, Object> entry = itr.next();
			String columnName = entry.getKey();
			System.out.print(columnName+" ");
		}
		System.out.println();
		long time12 = System.nanoTime();
		System.out.println(time12 - time11);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		try {
			long time11 = System.nanoTime();
			Condition condi1 = new Condition(new String[]{"name","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
			Condition condi2 = new Condition(new String[]{"name0","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
			condi1.or(condi2);
			SqlHelper sqlHelper_d = SqlHelper.newInstance().delete(Person.class).setWhere(condi1).builder();
			System.out.println(sqlHelper_d.toString());
			Map<String,Object> paramMap = sqlHelper_d.getNamedPerprotyParamMap();
			for (Iterator<Entry<String, Object>> itr = paramMap.entrySet().iterator(); itr.hasNext();) {
				Entry<String, Object> entry = itr.next();
				String columnName = entry.getKey();
				System.out.print(columnName+" ");
			}
			System.out.println();
			long time12 = System.nanoTime();
			System.out.println(time12 - time11);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			Person person = new Person();
			person.setPkId(12364565464564l);
			person.setAge(20);
			person.setName("AAA");
			person.setBodyhight(180f);
			
			long time11 = System.nanoTime();
			Condition condi1 = new Condition(new String[]{"name","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
			Condition condi2 = new Condition(new String[]{"name0","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
			condi1.or(condi2);
			SqlHelper sqlHelper_i =SqlHelper.newInstance().insert(Person.class).setEntity(person).builder();
			System.out.println(sqlHelper_i.toString());
			Map<String,Object> paramMap = sqlHelper_i.getNamedPerprotyParamMap();
			for (Iterator<Entry<String, Object>> itr = paramMap.entrySet().iterator(); itr.hasNext();) {
				Entry<String, Object> entry = itr.next();
				String columnName = entry.getKey();
				System.out.print(columnName+" ");
			}
			System.out.println();
			long time12 = System.nanoTime();
			System.out.println(time12 - time11);
		} catch (Exception e) {
			// TODO: handle exception
		}
		/*
			Condition condi21 = new Condition(new String[]{"name","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
			Condition condi22 = new Condition(new String[]{"name","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
			condi21.or(condi22);
			System.out.println(condi21.toSQLString());	
		*/	
		try {
//			Person person = new Person();
//			person.setAge(20);
//			person.setName("AAA");
			//person.setBodyhight(180f);
			Map<String,Object> parmaMap = new HashMap<String,Object>();
			parmaMap.put("name", "AAA");
			parmaMap.put("age", 20);
			parmaMap.put("bodyhight", 1.73f);
			
			long time11 = System.nanoTime();
			Condition condi1 = new Condition(new String[]{"name","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
			Condition condi2 = new Condition(new String[]{"name","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
			condi1.or(condi2);
			SqlHelper sqlHelper = SqlHelper.newInstance().update(Person.class)
					.setUpdate(parmaMap)
					.setWhere(condi1)
					.builder();
			String sql = sqlHelper.toString();
			Map<String,Object> paramMap = sqlHelper.getNamedPerprotyParamMap();
			for (Iterator<Entry<String, Object>> itr = paramMap.entrySet().iterator(); itr.hasNext();) {
				Entry<String, Object> entry = itr.next();
				String columnName = entry.getKey();
				System.out.print(columnName+" ");
			}
			System.out.println();
			System.out.println(sql);
			long time12 = System.nanoTime();
			//String.format("Customer[id=%d, firstName='%s', lastName='%s']",id, firstName, lastName);
			System.out.println(time12 - time11);
			System.out.println();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		}
//		try {
//		Condition condi1 = new Condition(new String[]{"name","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
//		Condition condi2 = new Condition(new String[]{"name0","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
//		Condition condi3 = new Condition(new String[]{"name1","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
//		Condition condi4 = new Condition(new String[]{"name2","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
//		Condition condi5 = new Condition(new String[]{"name3","age"}, new Operator[]{Operator.EQ,Operator.IN}, new Object[]{"wu",new Integer[]{1,2,3}});
////		condi1.or(condi2).or(condi3);
//		condi1.or(condi2.or(condi3)).and(condi4).or(condi5);
//		System.out.println(condi1.toSQLString());
//	} catch (SQLException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	} 
		
//		int times = 10000;
//		DecimalFormat df = new DecimalFormat("0.00");//格式化小数 
//		long time21 = System.nanoTime();
//		for (int i = 0; i < times; i++) {
//			SqlHelper.newInstance().select(Person.class).setSeleteFields(new String[]{}).builder().toString();
//		}
//		long time22 = System.nanoTime();
//		System.out.println(df.format(((float)(time22 - time21)/times)));
		
//		System.out.println(SqlHelper.newInstance().select(Person.class).count().builder().toString());
		
//		List<Object> a = new ArrayList<Object>();
//		System.out.println(a instanceof List);
//		
//		String[] arr = new String[2];
//		System.out.println(arr.getClass().isArray());
//		
//		System.out.println(Arrays.asList(arr));
		
		System.exit(0);
	}

}
