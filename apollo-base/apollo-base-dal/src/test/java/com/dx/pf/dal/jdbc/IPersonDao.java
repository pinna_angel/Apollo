/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月9日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc;

import com.dx.pf.dal.entity.Person;
import com.dx.pf.dal.jdbc.dao.IApolloDao;

/** 
* @ClassName: IPersonDao 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月9日 上午11:02:04 
* @version V1.0 
*/
public interface IPersonDao extends IApolloDao<Person> {

}
