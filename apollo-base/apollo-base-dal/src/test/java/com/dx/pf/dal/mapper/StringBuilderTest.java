/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.mapper;

/** 
* @ClassName: StringBuilderTest 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月5日 上午11:52:21 
* @version V1.0 
*/
public class StringBuilderTest {

	public static void main(String[] args) {
		int length = 10;
		
		StringBuilder sql = new StringBuilder();
		//StringBuffer sql = new StringBuffer();
		long time11 = System.nanoTime();
		for (int i = 0; i < length; i++) {
			sql.append("col_").append(i).append(",");
		}
//		if(sql.toString().endsWith(",")){
//			sql = sql.replace(sql.length() - 1, sql.length(), "");
//		}
		sql.deleteCharAt(sql.length() - 1);
		
		long time12 = System.nanoTime();
		System.out.println(time12 - time11);
		System.out.println(sql);
		
		//======================
		System.out.println("===================================");
		
		
		sql = new StringBuilder();
		
		long time21 = System.nanoTime();
		boolean flag = false;
		for (int i = 0; i < length; i++) {
			if(flag){
				sql.append(",");
			}
			sql.append("col_").append(i);
			flag = true;
		}
		long time22 = System.nanoTime();
		System.out.println(time22 - time21);
		System.out.println(sql);
		
		System.exit(0);
	}
}
