/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月9日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/** 
* @ClassName: JDBCTempleTest 
* @Description: 测试
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月9日 上午9:55:50 
* @version V1.0 
*/
public class JDBCTempleTest {

	private String act = "applicationContext.xml";
	@Before
	public void init(){
		ApplicationContext context = new FileSystemXmlApplicationContext(getResourcePath(act));
		
	}
	
	@Test
	public void testSelect(){
		
	}
	
	
	
	
	
	/**
	 * 获取java maven项目下的resource中的文件
	 * @param path
	 * @return
	 */
	public static String getResourcePath(String path) {
		return Thread.currentThread().getContextClassLoader().getResource(path).getPath();
	}
}
