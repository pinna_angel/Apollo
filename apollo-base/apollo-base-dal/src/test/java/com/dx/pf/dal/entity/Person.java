package com.dx.pf.dal.entity;

import java.io.Serializable;
import java.util.Date;

import com.dx.pf.dal.anno.Column;
import com.dx.pf.dal.anno.Id;
import com.dx.pf.dal.anno.Skip;
import com.dx.pf.dal.anno.Table;

/**
* @ClassName: Person 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月3日 下午8:02:26 
* @version V1.0
 */
@Table("person_0001")
public class Person implements Serializable{
	
	private static final long serialVersionUID = -2492590591854169822L;
	
	@Id
	@Column("pk_id")
	private long pkId;
	
	@Column()
	private String name;
	
	@Column()
	private int age;
	
	@Skip
	private Date borthday;
	
	@Column()
	private float bodyhight;
	
	
	/**
	 * @return the pkId
	 */
	public long getPkId() {
		return pkId;
	}
	/**
	 * @param pkId the pkId to set
	 */
	public void setPkId(long pkId) {
		this.pkId = pkId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the borthday
	 */
	public Date getBorthday() {
		return borthday;
	}
	/**
	 * @param borthday the borthday to set
	 */
	public void setBorthday(Date borthday) {
		this.borthday = borthday;
	}
	/**
	 * @return the bodyhight
	 */
	public float getBodyhight() {
		return bodyhight;
	}
	/**
	 * @param bodyhight the bodyhight to set
	 */
	public void setBodyhight(float bodyhight) {
		this.bodyhight = bodyhight;
	}
	
	
}
