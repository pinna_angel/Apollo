/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月9日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc;

import com.dx.pf.dal.entity.Person;
import com.dx.pf.dal.jdbc.dao.ApolloDaoImpl;

/** 
* @ClassName: PersonDaoImpl 
* @Description: 用户访问
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月9日 上午10:51:06 
* @version V1.0 
*/
public class PersonDaoImpl extends ApolloDaoImpl<Person> implements IPersonDao{

}
