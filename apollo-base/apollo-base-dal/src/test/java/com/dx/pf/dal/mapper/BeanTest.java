/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月3日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.mapper;

import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.BeanUtils;

import com.dx.pf.dal.entity.Person;
import com.dx.pf.dal.entity.User;

import net.sf.cglib.beans.BeanGenerator;
import net.sf.cglib.beans.BeanMap;

/** 
* @ClassName: BeanTest 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月3日 下午8:30:18 
* @version V1.0 
*/
public class BeanTest {

	private final static Map<String,Object> objectMap = new ConcurrentHashMap<String,Object>();
	private final static Map<String,PropertyDescriptor[]> propertyDescriptorMap = new ConcurrentHashMap<String,PropertyDescriptor[]>();
	
	public void getPropertyDescriptor(){
		PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(Person.class);
		for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
			//propertyDescriptor.setValue(attributeName, value);
			
			BeanGenerator gen =new BeanGenerator();
			gen.setSuperclass(Person.class);
			Object person= gen.create();
			
			//propertyDescriptor.getWriteMethod().invoke(person, args);
			System.out.println(propertyDescriptor.getName());
			
			//BeanMap.create(bean);
		}
	}
	
	public void getBeanMap(){
		Person person = new Person();;  
		person.setName("wuzhenfang");
		person.setAge(32);  
		person.setBorthday(new Date()); 
		person.setBodyhight(1.73f); 
		BeanMap bean = BeanMap.create(person);
		
		System.out.println(bean.get("name"));
	}
	//值类型转换类ConversionService，Spring中有具体的实现类，里面已经内建了大部分常用的转换，比如字符串转日期、字符串转数值等。
	//spring中有一个更强大的操作bean的类BeanWrapper，它的强大体现在下面两个方面：
	//1支持设置嵌套属性
	//2支持属性值的类型转换（设置ConversionService）。
	//BeanWrapper beanWrapper = PropertyAccessorFactory.forBeanPropertyAccess( obj);
	//beanWrapper.setConversionService(conversionService); //值类型转换，跟spring mvc无缝集成
	//beanWrapper.setAutoGrowNestedPaths( true); //对于null值，是否自动创建新对象
	//beanWrapper.setPropertyValue("a[0].b[dd].a", "value");
	
	private void initObject(){
		try {
			Object obj = Person.class.newInstance();
			objectMap.put(Person.class.getName(), obj);
			
			PropertyDescriptor[] propertyDescriptorsPerson = BeanUtils.getPropertyDescriptors(Person.class);
			propertyDescriptorMap.put(Person.class.getName(), propertyDescriptorsPerson);
			
			Object user = User.class.newInstance();
			objectMap.put(User.class.getName(), user);
			
			PropertyDescriptor[] propertyDescriptorsUser = BeanUtils.getPropertyDescriptors(User.class);
			propertyDescriptorMap.put(User.class.getName(), propertyDescriptorsUser);
			
			
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setBeanValue(){
		PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(Person.class);
		for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
			Method writeMethod = propertyDescriptor.getWriteMethod();
			if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
				writeMethod.setAccessible(true);
			}
//			try {
//				writeMethod.invoke(entity, resultSet.getObject(column));
//			} catch (Exception e) {
//			}
			
			//BeanCopier.create(source.getClass(), target.getClass(), false); 
		}
	}
	
	/**
	 *  高效，大概6000纳秒
	 * @param clazz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private static Object testNewInstance(Class clazz) throws InstantiationException, IllegalAccessException{
		return clazz.newInstance();
	}
	
	/**
	 * 非常的低效，大概需要 8992948纳秒
	 * @param srcObj
	 * @return
	 */
	private static Object depthClone(Object srcObj){  
        Object cloneObj = null;  
        try {  
            ByteArrayOutputStream out = new ByteArrayOutputStream();  
            ObjectOutputStream oo = new ObjectOutputStream(out);  
            oo.writeObject(srcObj);  
              
            ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());  
            ObjectInputStream oi = new ObjectInputStream(in);  
            cloneObj = oi.readObject();           
        } catch (IOException e) {  
            e.printStackTrace();  
        } catch (ClassNotFoundException e) {  
            e.printStackTrace();  
        }  
        return cloneObj;  
    }  
	
	public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, NoSuchMethodException {
		BeanTest bean = new BeanTest();
//		new BeanTest().getPropertyDescriptor();
//		new BeanTest().getBeanMap();
		
		bean.initObject();
		
		long time1 = System.nanoTime();
		User user = (User) testNewInstance(User.class);
		
		//43802825
		//User user = (User) org.apache.commons.beanutils.BeanUtils.cloneBean(objectMap.get(User.class.getName()));
		//User user = (User) depthClone(objectMap.get(User.class.getName()));
		long time2 = System.nanoTime();
		
		System.out.println(time2-time1);
		
		
		BeanUtils.instantiate(User.class);
		PropertyDescriptor[] personproperty =  propertyDescriptorMap.get(User.class.getName());
		for (PropertyDescriptor propertyDescriptor : personproperty) {
			Method method = propertyDescriptor.getWriteMethod();
			if(method != null){
				method.invoke(user, "1111");
			}
		}
		
		System.out.println();
		
		System.out.println(user.getLoginName());
		System.out.println(user.getPassword());
		
		System.out.println();
		
		for (Iterator ite = objectMap.entrySet().iterator() ;ite.hasNext(); ) {
			Map.Entry<String,Object> entry = (Entry<String, Object>) ite.next();
			if(entry.getKey().equals(User.class.getName())){
				User userInMap = (User) entry.getValue();
				System.out.println(userInMap.getLoginName());
				System.out.println(userInMap.getPassword());
			}
		}
		
		System.exit(0);
	}
}
