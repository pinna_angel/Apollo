/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月3日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.dx.pf.dal.entity.Person;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.sf.cglib.beans.BeanMap;

/** 
* @ClassName: CGlibTest 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月3日 上午12:14:02 
* @version V1.0 
*/
public class CGlibTest {

	/**
	 * 将对象装换为map
	 * @param bean
	 * @return
	 */
	public static <T> Map<String, Object> beanToMap(T bean) {
		Map<String, Object> map = Maps.newHashMap();
		if (bean != null) {
			BeanMap beanMap = BeanMap.create(bean);
			for (Object key : beanMap.keySet()) {
				map.put(key + "", beanMap.get(key));
			}
		}
		return map;
	}

	/**
	 * 将map装换为javabean对象
	 * @param map
	 * @param bean
	 * @return
	 */
	public static <T> T mapToBean(Map<String, Object> map, T bean) {
		BeanMap beanMap = BeanMap.create(bean);
		beanMap.putAll(map);
		return bean;
	}

	/**
	 * 将List<T>转换为List<Map<String, Object>>
	 * @param objList
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static <T> List<Map<String, Object>> objectsToMaps(List<T> objList) {
		List<Map<String, Object>> list = Lists.newArrayList();
		if (objList != null && objList.size() > 0) {
			Map<String, Object> map = null;
			T bean = null;
			for (int i = 0, size = objList.size(); i < size; i++) {
				bean = objList.get(i);
				map = beanToMap(bean);
				list.add(map);
			}
		}
		return list;
	}

	/**
	 * 将List<Map<String,Object>>转换为List<T>
	 * @param maps
	 * @param clazz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T> List<T> mapsToObjects(List<Map<String, Object>> maps, Class<T> clazz) throws InstantiationException, IllegalAccessException {
		List<T> list = Lists.newArrayList();
		if (maps != null && maps.size() > 0) {
			Map<String, Object> map = null;
			T bean = null;
			for (int i = 0, size = maps.size(); i < size; i++) {
				map = maps.get(i);
				bean = clazz.newInstance();
				mapToBean(map, bean);
				list.add(bean);
			}
		}
		return list;
	}
//====================================================================
	int size = 1000000;  
    
    List catchList = null;  
      
    public List getInitData() {  
        if (catchList != null ) {  
            return catchList;  
        }  
        catchList = new ArrayList(size); 
        for (int i = 0; i < size; i++) {  
        	Person settle = new Person();;  
            settle.setName("wuzhenfang");
            settle.setAge(32);  
            settle.setBorthday(new Date()); 
            settle.setBodyhight(1.73f); 
            catchList.add(settle);  
        }  
        return catchList;  
    }  
	
	public long testReflectance() throws Exception {  
        List<Person> list = getInitData();  
//      System.out.println("开始" + DateUtil.format(new Date()));  
        long start = System.currentTimeMillis();  
        Class clazz = Person.class;  
        List<Method> listMethods = new ArrayList<Method>();  
        for (Method method : clazz.getMethods()) {  
            if (method.getName().startsWith("get")) {  
                listMethods.add(method);  
            }  
        }  
          
        for (Person settle: list) {  
//          StringBuilder sb = new StringBuilder();  
            for (Method method : listMethods) {  
                method.invoke(settle);  
            }  
//          System.out.println(sb);  
        }  
//      System.out.println(System.currentTimeMillis() - start);  
//      System.out.println("完成：" + DateUtil.format(new Date()));  
        return System.currentTimeMillis() - start;  
    }  
      
    public long testCglib() throws Exception {  
        List<Person> list = getInitData();  
//      System.out.println("开始" + DateUtil.format(new Date()));  
        long start = System.currentTimeMillis();  
        Set keyset = BeanMap.create(list.get(0)).keySet();  
        for (Person settle: list) {  
            BeanMap map = BeanMap.create(settle);  
            for (Object key : keyset) {  
                map.get(key);  
            }  
              
        }  
//      System.out.println(System.currentTimeMillis() - start);  
//      System.out.println("完成：" + DateUtil.format(new Date()));  
        return System.currentTimeMillis() - start;  
    }  
      
    public static void main(String[] args) throws Exception {  
//      new CglibSpeedTest().testReflectance();  
    	CGlibTest test = new CGlibTest();  
        System.out.println("次数：" + test.size);  
        for (int i = 0; i < 20; i++) {  
            System.out.println(test.testCglib() + "\t" + test.testReflectance());  
        }  
    }  

}
