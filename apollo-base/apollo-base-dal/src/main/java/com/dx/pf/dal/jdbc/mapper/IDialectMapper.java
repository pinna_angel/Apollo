/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.mapper;

import java.util.Map;

/** 
* @ClassName: IDialectMapping 
* @Description: 方言映射
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 下午12:10:49 
* @version V1.0 
*/
public interface IDialectMapper {

	/**
	 * 获取映射
	 * @return
	 */
	public Map<String, Class<?>> getMapper();
	
}
