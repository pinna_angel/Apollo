/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.mapper;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/** 
* @ClassName: SqlServerTypeMapper 
* @Description: SqlServer 类型映射
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 下午12:15:30 
* @version V1.0 
*/
public class SqlServerTypeMapper implements IDialectMapper {

	/**
	 * SqlServer 数据类型对应类
	 */
	public static final Map<String, Class<?>> mapping = new HashMap<String, Class<?>>() {
		private static final long serialVersionUID = -1168963664693153914L;
		{
			put("INT", Integer.class);
			put("TINYINT", Integer.class);
			put("SMALLINT", Integer.class);
			put("VARCHAR", String.class);
			put("NVARCHAR", String.class);
			put("CHAR", String.class);
			put("NCHAR", String.class);
			put("UNIQUEIDENTIFIER", String.class);
			put("BIT", Boolean.class);
			put("FLOAT", Double.class);
			put("DECIMAL", BigDecimal.class);
			put("MONEY", BigDecimal.class);
			put("SMALLMONEY", BigDecimal.class);
			put("NUMERIC", BigDecimal.class);
			put("REAL", Float.class);
			put("BINARY", byte[].class);
			put("VARBINARY", byte[].class);
			put("TEXT", Clob.class);
			put("NTEXT", Clob.class);
			put("IMAGE", Blob.class);
			put("SQL_VARIANT", byte[].class);
			put("SMALLDATETIME", Date.class);
			put("TIMESTAMP", Date.class);
			put("DATETIME", Date.class);
			put("DATETIME2", Date.class);
			put("DATE", Date.class);
			put("TIME", Date.class);
		}
	};

	@Override
	public Map<String, Class<?>> getMapper() {
		return mapping;
	}

}
