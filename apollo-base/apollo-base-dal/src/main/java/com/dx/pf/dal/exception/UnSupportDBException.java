/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.exception;

/** 
* @ClassName: UnSupportDBException 
* @Description: 不支持的数据库异常
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 下午1:40:51 
* @version V1.0 
*/
public class UnSupportDBException extends RuntimeException {

	private static final long serialVersionUID = -1131775885541376692L;

	private static final String MESSAGE = "Can not support database type [%s].";

	public UnSupportDBException(final String databaseType) {
		super(String.format(MESSAGE, databaseType));
	}

}
