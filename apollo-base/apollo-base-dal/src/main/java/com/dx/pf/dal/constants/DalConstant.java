/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.constants;

/** 
* @ClassName: DalConstant 
* @Description: dal使用的常量
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月5日 上午11:47:08 
* @version V1.0 
*/
public interface DalConstant {

	public final static String bracket_l = " ( ";

	public final static String bracket_r = " ) ";

	public final static String sql_eq = " = ";

	public final static String sql_pause = " , ";
	
	public final static String sql_colon = ":";

	public final static String sql_question = " ? ";
	
	public final static String sql_spaces = " ";
	 
	public final static String sql_select = "SELECT ";

	public final static String sql_delete = "DELETE FROM ";

	public final static String sql_update = "UPDATE ";

	public final static String sql_insert = "INSERT INTO ";

	public final static String sql_from = " FROM ";
	
	public final static String sql_set = " SET ";
	
	public final static String sql_value = " VALUES ";
	
	public final static String sql_Where = " WHERE ";

	public final static String sql_orderby = " ORDER BY ";

	public final static String sql_limit = " LIMIT ";
	
	public final static String sql_and = " AND ";
	
	public final static String sql_or = " OR ";

}

