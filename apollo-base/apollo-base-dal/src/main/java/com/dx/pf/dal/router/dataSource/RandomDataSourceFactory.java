/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月7日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.router.dataSource;

import com.alibaba.druid.pool.DruidDataSourceFactory;

/** 
* @ClassName: RandomDataSourceFactory 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月7日 下午3:20:49 
* @version V1.0 
*/
public class RandomDataSourceFactory extends DruidDataSourceFactory {

	
}
