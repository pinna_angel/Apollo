/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月6日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

/** 
* @ClassName: BeanPropertyAccessRowMapper 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月6日 上午9:35:37 
* @version V1.0 
*/
public class BeanPropertyAccessRowMapper<T> extends BeanPropertyRowMapper<T>{

	/* (non-Javadoc)
	 * @see org.springframework.jdbc.core.BeanPropertyRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public T mapRow(ResultSet rs, int rowNumber) throws SQLException {
		// TODO Auto-generated method stub
		return super.mapRow(rs, rowNumber);
	}

	
	
}
