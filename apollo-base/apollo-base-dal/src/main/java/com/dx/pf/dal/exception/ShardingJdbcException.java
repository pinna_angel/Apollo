/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.exception;

/** 
* @ClassName: ShardingJdbcException 
* @Description: JDBC分片异常
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 下午1:39:34 
* @version V1.0 
*/
public class ShardingJdbcException {

}
