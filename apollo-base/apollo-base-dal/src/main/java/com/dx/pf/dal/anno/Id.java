/**
 * 
 */
package com.dx.pf.dal.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * @ClassName: Id 
 * @Description: 数据库主键注解
 * @author wuzhenfang(zhenfangwu@capitalbio.com) 
 * @date 2015年5月20日 上午11:09:21 
 * @version V1.0 
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Id {

}
