/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月9日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

/** 
* @ClassName: MapAccessResultSetExtractor 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月9日 下午2:45:31 
* @version V1.0 
*/
public class MapAccessResultSetExtractor implements ResultSetExtractor<List<Map<String, Object>>> {

	@Override
	public List<Map<String, Object>> extractData(ResultSet rs) throws SQLException, DataAccessException {
		ResultSetMetaData rm = rs.getMetaData();
		int ccount = rm.getColumnCount();

		List<Map<String, Object>> mapls = new ArrayList<Map<String, Object>>();
		while (rs.next()) {
			Map<String, Object> map = new HashMap<String, Object>();
			for (int i = 1; i <= ccount; i++) {
				String label = rm.getColumnLabel(i);
				Object obj = rs.getObject(label);
				map.put(label, obj);
			}
			mapls.add(map);
		}
		return mapls;

	}

}
