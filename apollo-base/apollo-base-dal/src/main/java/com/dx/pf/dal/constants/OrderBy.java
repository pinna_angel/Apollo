/**
 * 
 */
package com.dx.pf.dal.constants;

/** 
 * @ClassName: OrderBy 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author wuzhenfang(zhenfangwu@capitalbio.com) 
 * @date 2015年5月30日 上午8:41:49 
 * @version V1.0 
 */
public enum OrderBy {

	ASC(" asc "),DESC(" desc ");
	
	private String sqlVal;
	
	private OrderBy( String sqlVal ){
		this.sqlVal = sqlVal;
	}

	public String getSqlVal() {
		return sqlVal;
	}
	
}
