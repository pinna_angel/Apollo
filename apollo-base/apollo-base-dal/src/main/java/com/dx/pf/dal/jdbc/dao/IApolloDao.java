/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月9日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.dao;

import java.io.Serializable;
import java.util.Map;

import com.dx.pf.dal.exception.DalException;
import com.dx.pf.dal.jdbc.sql.Condition;

/** 
* @ClassName: IApolloDao 
* @Description: 阿波罗框架访问数据库基础Dao接口
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月9日 上午10:17:00 
* @version V1.0 
*/
public interface IApolloDao<T extends Serializable> {

	public boolean insert(T entity) throws DalException;

	public boolean insert(long pkId, T entity) throws DalException;

	public boolean update(Map<String, Object> paramMap, long pkId) throws DalException;

	public boolean update(Map<String, Object> paramMap, Condition condition) throws DalException;

}
