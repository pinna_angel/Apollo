/**
 * 
 */
package com.dx.pf.dal.constants;

/** 
 * @ClassName: Operator 
 * @Description: 操作类型
 * @author wuzhenfang(zhenfangwu@capitalbio.com) 
 * @date 2015年5月31日 下午2:23:46 
 * @version V1.0 
 */
public enum Operator {
	/**等于*/
	EQ(" = "),
	/**不能于*/
	NOT_EQ(" <> "),
	/**大于*/
	GT(" > "),
	/**大于等于*/
	GE(" >= "),
	/**小于*/
	LT(" < "),
	/**小于等于*/
	LE(" <= "),
	/**in*/
	IN(" in "),
	/**is*/
	IS(" is "),
	/**is not*/
	IS_NOT(" IS NOT ");
	
	private String sqlVal;
	
	private Operator(String sqlVal){
		this.sqlVal = sqlVal;
	}
	
	public String getSqlVal() {
		return sqlVal;
	}
}
