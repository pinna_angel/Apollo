/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月4日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.cache;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import com.dx.pf.dal.Logger;
import com.dx.pf.dal.anno.Column;
import com.dx.pf.dal.anno.Id;
import com.dx.pf.dal.anno.Skip;
import com.dx.pf.dal.anno.Table;
import com.dx.pf.dal.exception.DalException;
import com.dx.pf.dal.exception.SQLParserException;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/** 
* @ClassName: EntryInfo 
* @Description: dal实体访问信息的详细信息
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月4日 上午10:25:12 
* @version V1.0 
*/
public class EntryInfo implements Serializable {

	private static final long serialVersionUID = -1212904951508480322L;

	private final static Logger logger = Logger.getLogger(EntryInfo.class);
	/** 实体名称 **/
	private String clazzName;

	/** 实体的class类，该形式已经完成连接加载校验准备工作 **/
	private Class<?> clazz;

	/** 类的注解 **/
	private Annotation [] classAnnotations;
	
	/** 表名称 */
	private String tableName;
	
	/** 主键ID */
	private String idColName;
	private String idFiledName;
	
	/** bean fields */
	private List<Field> fields = new ArrayList<Field>();

	/** 实体变量参数描述 **/
	private List<PropertyDescriptor> propertyDescriptors = new ArrayList<PropertyDescriptor>();

	/** 列名称 */
	private List<String> colNameList = new ArrayList<String>();

	/** 实体变量名称 */
	private List<String> fieldNameList = new ArrayList<String>();

	/** 实体变量字段名称对应变量属性 **/
	private Map<String, PropertyDescriptor> fieldNamePropertyDesc = new ConcurrentHashMap<String, PropertyDescriptor>();

	/**字段对应的注解**/
	private Map<Field, Annotation[]> fieldAnnotations = new ConcurrentHashMap<Field, Annotation[]>();

	/** 字段名称对应 标注在field上的注解字段名,双向map */
	private BiMap<String, String> field2ColName = HashBiMap.create();

	public EntryInfo() {
		
	}

	public EntryInfo(Object object) {
		this(object.getClass());
	}

	public EntryInfo(Class<?> clazz) {
		parseEntryInfo(clazz);
	}

	/**
	 * @return the clazzName
	 */
	public String getClazzName() {
		return clazzName;
	}

	/**
	 * 
	 * @param clazzName the clazzName to set
	 */
	public void setClazzName(String clazzName) {
		this.clazzName = clazzName;
	}

	/**
	 * 获取class
	 * @return the clazz
	 */
	public Class<?> getClazz() {
		return clazz;
	}
	
	/**
	 * 获取实体对象
	 * @return
	 */
	public Object newInstance() {
		// 该方式获取类实体时间大概为5000纳秒，比depthClone都要快和BeanUtils.cloneBean
		try {
			return getClazz().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new DalException("实例化实体对象错误。");
		}
	}
	/**
	 * 设置class
	 * @param clazz the clazz to set
	 */
	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	/**
	 * 获取实体变量属性
	 * @return the fields
	 */
	public List<Field> getFields() {
		return fields;
	}

	/**
	 * 设置实体变量字段
	 * @param fields the fields to set
	 */
	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public void addFields(Field field) {
		this.fields.add(field);
	}
	
	public void addAllFields(List<Field> fields) {
		this.fields.addAll(fields);
	}
	
	/**
	 * 获取实体变量属性描述
	 * @return the propertyDescriptors
	 */
	public List<PropertyDescriptor> getPropertyDescriptors() {
		return propertyDescriptors;
	}

	/**
	 * 设置实体变量属性描述
	 * @param propertyDescriptors the propertyDescriptors to set
	 */
	public void setPropertyDescriptors(List<PropertyDescriptor> propertyDescriptors) {
		this.propertyDescriptors = propertyDescriptors;
	}

	public void addPropertyDescriptors(PropertyDescriptor propertyDescriptor) {
		this.propertyDescriptors.add(propertyDescriptor);
	}
	
	public void addAllPropertyDescriptors(List<PropertyDescriptor> propertyDescriptors) {
		this.propertyDescriptors.addAll(propertyDescriptors);
	}
	/**
	 * 获取类注解
	 * @return the classAnnotations
	 */
	public Annotation[] getClassAnnotations() {
		return classAnnotations;
	}

	/**
	 * 设置类注解
	 * @param classAnnotations the classAnnotations to set
	 */
	public void setClassAnnotations(Annotation[] classAnnotations) {
		this.classAnnotations = classAnnotations;
	}
	
	/**
	 * 获取表名称
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * 设置表名称
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * 获取主键字段名称
	 * @return the idColName
	 */
	public String getIdColName() {
		return idColName;
	}

	/**
	 * 只是主键字段名称
	 * @param idColName the idColName to set
	 */
	public void setIdColName(String idColName) {
		this.idColName = idColName;
	}

	/**
	 * @return the idFiledName
	 */
	public String getIdFiledName() {
		return idFiledName;
	}

	/**
	 * @param idFiledName the idFiledName to set
	 */
	public void setIdFiledName(String idFiledName) {
		this.idFiledName = idFiledName;
	}

	/**
	 * 获取DB字段列表
	 * @return the colNameList
	 */
	public List<String> getColNameList() {
		return colNameList;
	}

	/**
	 * 设置db字段列表
	 * @param colNameList the colNameList to set
	 */
	public void setColNameList(List<String> colNameList) {
		this.colNameList = colNameList;
	}
	/**
	 * 添加db字段信息
	 * @param colName
	 */
	public void addColNameList(String colName) {
		this.colNameList.add(colName);
	}
	/**
	 * 添加db字段列表
	 * @param colNameList
	 */
	public void addAllColNameList(List<String> colNameList) {
		this.colNameList.addAll(colNameList);
	}
	
	/**
	 * 获取实体字段列表
	 * @return the fieldNameList
	 */
	public List<String> getFieldNameList() {
		return fieldNameList;
	}

	/**
	 * 设置实体字段列表
	 * @param fieldNameList the fieldNameList to set
	 */
	public void setFieldNameList(List<String> fieldNameList) {
		this.fieldNameList = fieldNameList;
	}

	/**
	 * 添加实体变量信息
	 * @param fieldNameList the fieldNameList to set
	 */
	public void addFieldNameList(String fieldName) {
		this.fieldNameList.add(fieldName);
	}
	
	/**
	 * 添加实体变量列表
	 * @param fieldNameList the fieldNameList to set
	 */
	public void addAllFieldNameList(List<String> fieldNameList) {
		this.fieldNameList.addAll(fieldNameList);
	}
	
	/**
	 * 获取添加实体字段名称对应的实体字段描述 
	 * @return the filedNamePropertyDesc
	 */
	public Map<String, PropertyDescriptor> getFieldNamePropertyDesc() {
		return fieldNamePropertyDesc;
	}

	/**
	 * 根据实体字段名称获取字段的实体属性描述
	 * @param filedName
	 * @return
	 */
	public PropertyDescriptor getFieldNamePropertyDesc(String fieldName) {
		return fieldNamePropertyDesc.get(fieldName);
	}
	
	/**
	 * 设置添加实体字段名称对应的实体字段描述 
	 * @param filedNamePropertyDesc the filedNamePropertyDesc to set
	 */
	public void setFieldNamePropertyDesc(Map<String, PropertyDescriptor> fieldNamePropertyDesc) {
		this.fieldNamePropertyDesc = fieldNamePropertyDesc;
	}

	/**
	 * 添加实体字段名称对应的实体字段描述 
	 * @param fieldName
	 * @param propertyDescriptor
	 */
	public void putFieldNamePropertyDesc(String fieldName, PropertyDescriptor propertyDescriptor) {
		this.fieldNamePropertyDesc.put(fieldName, propertyDescriptor);
	}
	
	/**
	 * 批量添加实体字段名称对应的实体字段描述 
	 * @param filedNamePropertyDesc
	 */
	public void putAllFieldNamePropertyDesc(Map<String, PropertyDescriptor> filedNamePropertyDesc) {
		this.fieldNamePropertyDesc.putAll(filedNamePropertyDesc);
	}
	
	/**
	 * 获取实体中字段对应的注解信息
	 * @return the fieldAnnotations
	 */
	public Map<Field, Annotation[]> getFieldAnnotations() {
		return fieldAnnotations;
	}

	/**
	 * 根据实体字段名称获取对应字段的注解信息
	 * @param fieldName
	 * @return
	 */
	public Annotation[] getAnnotationsByFieldName(String fieldName) {
		return fieldAnnotations.get(fieldName);
	}
	
	/**
	 * 这是字段对应 的注解信息
	 * @param fieldAnnotations the fieldAnnotations to set
	 */
	public void setFieldAnnotations(Map<Field, Annotation[]> fieldAnnotations) {
		this.fieldAnnotations = fieldAnnotations;
	}

	/**
	 * 添加字段对应 的注解信息
	 * @param field
	 * @param fieldAnnos
	 */
	public void putFieldAnnotations(Field field, Annotation[] fieldAnnos) {
		this.fieldAnnotations.put(field, fieldAnnos);
	}
	
	/**
	 * 添加字段对应 的注解信息
	 * @param fieldAnnotations
	 */
	public void putAllFieldAnnotations(Map<Field, Annotation[]> fieldAnnotations) {
		this.fieldAnnotations.putAll(fieldAnnotations);
	}
	
	/**
	 * 获取双向map<实体变量名称,字段名称>map
	 * @return the field2ColName
	 */
	public BiMap<String, String> getField2ColName() {
		return field2ColName;
	}

	/**
	 * 根据获取实体字段获取实体字段名称
	 * @param columnName
	 * @return
	 */
	public String getFieldNameByColumnName(String columnName){
		return getField2ColName().inverse().get(columnName);
	}
	
	/**
	 * 根据实体变量字段名称获取数据库字段名称
	 * @param fieldName
	 * @return
	 */
	public String getColumnNameByFieldName(String fieldName){
		return getField2ColName().get(fieldName);
	}
	
	/**
	 * 设置双向map<实体变量名称,字段名称>
	 * @param field2ColName the field2ColName to set
	 */
	public void setField2ColName(BiMap<String, String> field2ColName) {
		this.field2ColName = field2ColName;
	}
	
	/**
	 * 设置双向map<实体变量名称,字段名称>
	 * @param fieldName
	 * @param columnName
	 */
	public void putField2ColName(String fieldName,String columnName) {
		this.field2ColName.put(fieldName, columnName);
	}
	
	/**
	 * 批量设置双向map<实体变量名称,字段名称>
	 * @param field2ColNameMap
	 */
	public void putAllField2ColName(BiMap<String, String> field2ColNameMap) {
		this.field2ColName.putAll(field2ColName);
	}
	
	/**
	 * 通过类clazz来解析成缓存
	 * @param clazz
	 */
	public void parseEntryInfo(Class<?> clazz){
		boolean isDBEntry = false;
		Annotation[] clazzAnno = clazz.getAnnotations();
		//解析类表
		for (Annotation annotation : clazzAnno) {
			if(annotation instanceof Table){//是否为表，如果为表则设置表名称
				String tableName = ((Table) annotation).value();
				if(StringUtils.isEmpty(tableName)){
					tableName = clazz.getSimpleName();//获取类名称
					this.setTableName(tableName.substring(0, 1).toLowerCase()+tableName.substring(1));
				}else{
					this.setTableName(tableName);
				}
				this.setClazz(clazz);
				this.setClazzName(clazz.getName());//类的全名称
				this.setClassAnnotations(clazzAnno);
				isDBEntry = true;//设置是否为数据库属性
				break;
			}
		}
		//判断是为数据库实体才继续解析
		if(isDBEntry){
			//解析字段变量属性
			Field[] fields = clazz.getDeclaredFields();
			//this.setFields(fields);//设置字段（这么设置包含不需要对应关系的字段信息）
			
			for (Field field : fields) {
				Annotation[] fieldAnnos = field.getDeclaredAnnotations();
				for (Annotation fieldAnno : fieldAnnos) {
					if(fieldAnno instanceof Skip){
						continue;//忽略字段
					}else if(fieldAnno instanceof Column || fieldAnno instanceof Id){//既是主键Id也是普通
						String fieldName = field.getName();
						if(fieldAnno instanceof Id){//主键
							this.setIdFiledName(fieldName);
							continue;//忽略后面的属性设置
						}
						String columnName = ((Column) fieldAnno).value();
						if(StringUtils.isEmpty(columnName)){//列名称为空
							columnName = fieldName;
						}
						this.addFields(field);
						this.putFieldAnnotations(field, fieldAnnos);
						//普通列
						this.putField2ColName(fieldName, columnName);
						this.addColNameList(columnName);
						this.addFieldNameList(fieldName);
						
						PropertyDescriptor fieldPropertyDesc = BeanUtils.getPropertyDescriptor(clazz, fieldName);
						if(null != fieldPropertyDesc){
							this.putFieldNamePropertyDesc(fieldName, fieldPropertyDesc);
							this.addPropertyDescriptors(fieldPropertyDesc);
						}else{
							logger.error("字段缺少get/set方法");
							throw new SQLParserException("字段缺少get/set方法");
						}
					}else{
						//无注解列--后面可以使用缓存无注解列来cglib重新组装一个新的entry
					}
				}
			}
			this.setIdColName(getColumnNameByFieldName(getIdFiledName()));//设置Id属性的字段
		}
	}
}
