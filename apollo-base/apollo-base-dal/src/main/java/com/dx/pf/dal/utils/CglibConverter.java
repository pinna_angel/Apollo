/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年6月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.utils;

import java.util.Date;

import com.alibaba.fastjson.JSONArray;
import com.dx.pf.commons.utils.DateUtil;

import net.sf.cglib.core.Converter;

/** 
* @ClassName: CglibConverter 
* @Description: Cglib转换器，针对Cglib不能转换的类型手动处理 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年6月5日 上午8:57:01 
* @version V1.0 
*/
public class CglibConverter implements Converter {

	/** true为Y */
	public static final String BOOLEAN_TRUE = "Y";

	/** false为N */
	public static final String BOOLEAN_FALSE = "N";

	/**
	 * @see net.sf.cglib.core.Converter#convert(java.lang.Object,
	 *      java.lang.Class, java.lang.Object)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object convert(Object value, Class target, Object context) {
		if (value == null) {
			return null;
		}
		Class targetClass = target;
		// 日期 Date --> String
		if (Date.class.isInstance(value) && targetClass.isAssignableFrom(String.class)) {
			return DateUtil.getDefaultDate((Date) value);
		}

		// 日期 Date --> long
		if (Date.class.isInstance(value) && targetClass.isAssignableFrom(long.class)) {
			return ((Date) value).getTime();
		}

		// boolean --> String
		if (Boolean.class.isInstance(value) && targetClass.isAssignableFrom(String.class)) {
			return ((Boolean) value == true) ? BOOLEAN_TRUE : BOOLEAN_FALSE;
		}

		// String --> boolean
		if (String.class.isInstance(value)
			&& (targetClass.isAssignableFrom(Boolean.TYPE) || targetClass.isAssignableFrom(Boolean.class))) {
			return ((String) value).equals(BOOLEAN_TRUE) ? true : false;
		}
		// String --> JSONArray
		if (String.class.isInstance(value) && targetClass.isAssignableFrom(JSONArray.class)) {
			return JSONArray.parse((String) value);
		}
		return value;
	}
}
