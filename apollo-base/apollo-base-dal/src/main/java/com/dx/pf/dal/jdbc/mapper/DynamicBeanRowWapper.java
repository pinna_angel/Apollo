/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月6日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import net.sf.cglib.beans.BeanGenerator;
import net.sf.cglib.beans.BeanMap;

/** 
* @ClassName: DynamicBeanRowWapper 
* @Description:动态bean的rowwapper
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月6日 上午9:06:59 
* @version V1.0 
*/
public class DynamicBeanRowWapper<T> implements RowMapper<T> {

	/** 实体Object */
	private Object object = null;

	/** 属性map */
	private BeanMap beanMap = null;

	public DynamicBeanRowWapper() {

	}

	public DynamicBeanRowWapper(Map<String, Object> propertyMap) {
		this.object = generateBean(propertyMap);
		this.beanMap = BeanMap.create(this.object);
	}

	@Override
	public T mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 给bean属性赋值
	 * 
	 * @param property
	 *            属性名
	 * @param value
	 *            值
	 */
	public void setValue(String property, Object value) {
		beanMap.put(property, value);
	}

	/**
	 * 通过属性名得到属性值
	 * @param property 属性名
	 * @return 值
	 */
	public Object getValue(String property) {
		return beanMap.get(property);
	}

	/**
	 * 生成动态的bean
	 * @param propertyMap
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private T generateBean(Map<String, Object> propertyMap) {
		BeanGenerator generator = new BeanGenerator();
		Set<String> keySet = propertyMap.keySet();
		for (Iterator<String> i = keySet.iterator(); i.hasNext();) {
			String key = (String) i.next();
			generator.addProperty(key, propertyMap.get(key).getClass());
		}
		object = generator.create();
		return (T) object;
	}
}
