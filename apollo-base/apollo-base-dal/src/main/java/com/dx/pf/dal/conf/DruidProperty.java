/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.conf;

import javax.xml.bind.Marshaller;

/** 
* @ClassName: DruidProperty 
* @Description: druid的数据源配置文件属性
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月5日 下午2:48:13 
* @version V1.0 
*/
public class DruidProperty {

	private String isShard;
	private String name;
	private String username;
	private String password;
	private String url;
	private String initialSize;
	private String minIdle;
	private String maxActive;
	/* 是否缓存preparedStatement，也就是PSCache，在mysql下建议关闭，分库分表较多的数据库，建议配置为false */
	private String poolPreparedStatements;
	private String maxOpenPreparedStatements;
	/* 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能 */
	private String testOnBorrow;
	/* 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能 */
	private String testOnReturn;
	/* 建议配置为true，不影响性能 */
	private String testWhileIdle;
	private String filters;
	private String connectionProperties;
	/* 合并多个DruidDataSource的监控数据 */
	private String useGlobalDataSourceStat;
	/* 保存DruidDataSource的监控记录到日志中 */
	private String timeBetweenLogStatsMillis;
	private Marshaller marshaller;
	private String dbSize;
	private boolean isShow;
	private int dataSourceIndex;
	private boolean init_method;
	private String tbSuffix = "_0000";
	
}
