/**
 * 
 */
package com.dx.pf.dal.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * @ClassName: Table 
 * @Description: 定义表名称注解
 * @author wuzhenfang(zhenfangwu@capitalbio.com) 
 * @date 2015年5月20日 上午9:45:46 
 * @version V1.0 
 */
@Target({
	ElementType.TYPE
})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Table {
	/**
	 * @Description: 对应表名
	 * @return String
	 * @throws
	 */
	public String value() default "";
}
