/**
 * 
 */
package com.dx.pf.dal.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * @ClassName: Skip 
 * @Description: bean中字段设置为skip则非数据库中的字段
 * @author wuzhenfang(zhenfangwu@capitalbio.com) 
 * @date 2015年5月29日 上午10:44:42 
 * @version V1.0 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Skip {

}
