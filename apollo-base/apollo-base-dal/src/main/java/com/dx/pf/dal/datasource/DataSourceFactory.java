/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.datasource;

import com.alibaba.druid.pool.DruidDataSource;

/** 
* @ClassName: DataSourceFactory 
* @Description: 数据源工厂
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午11:57:24 
* @version V1.0 
*/
public interface DataSourceFactory {

	/**
     * 获取数据源
     * @return 数据源
     */
	public DruidDataSource getDataSource();
}
