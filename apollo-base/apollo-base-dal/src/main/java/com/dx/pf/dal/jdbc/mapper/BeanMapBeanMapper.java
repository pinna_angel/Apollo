/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.mapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.AbstractSqlParameterSource;

import com.dx.pf.dal.Logger;
import net.sf.cglib.beans.BeanMap;

/** 
* @ClassName: BeanMapBeanMapper 
* @Description: 使用cglib中的BeanMap处理解析bean实体对象到map中
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月5日 下午3:46:13 
* @version V1.0 
*/
public class BeanMapBeanMapper extends AbstractSqlParameterSource implements BeanMapper {

	private final static Logger logger = Logger.getLogger(BeanMapBeanMapper.class);

	private final Map<String, Object> columnValueMap = new HashMap<String, Object>();

	public List<Object> valueList;// 值列表

	public BeanMapBeanMapper() {
	}

	public BeanMapBeanMapper(Object object) {
		init(object);
	}

	private void init(Object object) {
		if (null == object) {
			logger.error("传入的对象为null。", new NullPointerException());
			throw new NullPointerException("传入的对象为null。");
		}

		BeanMap beanMap = BeanMap.create(object);
		for (Object key : beanMap.keySet()) {
			Object value = beanMap.get(key);
			if (null != value) {
				columnValueMap.put((String) key, value);
				valueList.add(value);
				//TODO
			}
		}
	}

	@Override
	public boolean hasValue(String paramName) {
		return null != columnValueMap.get(paramName);
	}

	@Override
	public Object getValue(String paramName) throws IllegalArgumentException {
		return columnValueMap.get(paramName);
	}

	@Override
	public Map<String, Object> mapBean(Object object) throws SQLException {
		return columnValueMap;
	}

	@Override
	public int getSizePropertyValue() throws SQLException {
		return valueList.size();
	}

}
