/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月1日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.dialect;

/** 
* @ClassName: MySQLDialect 
* @Description: mysql方言
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月1日 上午1:19:34 
* @version V1.0 
*/
public class MySQLDialect {

	public String getLimitString(String sql, int offset, int offsetSize) {
		StringBuilder builder = new StringBuilder();
		return builder.append(sql).append(" limit ").append(offset).append(",").append(offsetSize).toString();
	}
}
