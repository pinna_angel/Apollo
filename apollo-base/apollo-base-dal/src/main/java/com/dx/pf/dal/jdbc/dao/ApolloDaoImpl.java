/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月9日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import com.dx.pf.dal.constants.OrderBy;
import com.dx.pf.dal.exception.DalException;
import com.dx.pf.dal.jdbc.SimpleJdbcTemplate;
import com.dx.pf.dal.jdbc.entity.PageHelper;
import com.dx.pf.dal.jdbc.sql.Condition;

/** 
* @ClassName: ApolloDaoImpl 
* @Description: 阿波罗框架访问数据库基础Dao实现
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月9日 上午10:16:34 
* @version V1.0 
*/
@SuppressWarnings("unchecked")
public class ApolloDaoImpl<T extends Serializable> implements IApolloDao<T> {

	private SimpleJdbcTemplate<T> jdcbTemplate;

	private Class<T> clazz;
	
	//获取泛型类型
	{
		clazz = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		if (jdcbTemplate != null) {
			jdcbTemplate.setClazz(clazz);
		}
	}

	@Override
	public boolean insert(T entity) throws DalException {
		jdcbTemplate.insert(entity);
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean insert(long pkId, T entity) throws DalException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Map<String, Object> paramMap, long pkId) throws DalException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Map<String, Object> paramMap, Condition condition) throws DalException {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 分页查询
	 * @param columnNames 查询列表可以为空，可以指定列表
	 * @param condition
	 * @param pageHelper 
	 * @param orderByMap
	 * @return
	 * @throws Exception
	 */
	public PageHelper<T> findEntityPage(String[] columnNames, Condition condition, PageHelper<T> pageHelper, Map<String, OrderBy> orderByMap) throws Exception {
		int totalCount = 0;
		if (pageHelper.getTotalCount() < 1) {
			totalCount = jdcbTemplate.count(condition);
		}
		List<T> list = (List<T>) jdcbTemplate.select(columnNames, condition, orderByMap, pageHelper.getPageNo(), pageHelper.getPageSize());
		if (list != null) {
			PageHelper<T> pages = new PageHelper<T>(pageHelper.getPageSize(), pageHelper.getPageNo());
			pages.setrSum(list.size());
			pages.setItems(list);
			pages.setTotalCount(totalCount);
			return pages;
		} else {
			return null;
		}
	}

	/**
	 * @return the jdcbTemplate
	 */
	public SimpleJdbcTemplate<T> getJdcbTemplate() {
		return jdcbTemplate;
	}

	/**
	 * @param jdcbTemplate
	 *            the jdcbTemplate to set
	 */
	public void setJdcbTemplate(SimpleJdbcTemplate<T> jdcbTemplate) {
		this.jdcbTemplate = jdcbTemplate;
	}

}
