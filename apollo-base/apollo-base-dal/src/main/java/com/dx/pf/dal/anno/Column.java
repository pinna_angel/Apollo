/**
 * 
 */
package com.dx.pf.dal.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * @ClassName: Column 
 * @Description: 定义表中字段注解
 * @author wuzhenfang(zhenfangwu@capitalbio.com) 
 * @date 2015年5月20日 上午9:50:43 
 * @version V1.0 
 */
@Target({ElementType.METHOD,
		ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
	
	/**
	 * @Description: 定义字段名称
	 * @return String
	 * @throws
	 */
	public String value() default "";
	
}
