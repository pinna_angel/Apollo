/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月1日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.router;

/** 
* @ClassName: RoutingStrategy 
* @Description: 数据源路由策略
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月1日 下午3:52:05 
* @version V1.0 
*/
public interface RoutingStrategy {

	public void getRouting();
	
}
