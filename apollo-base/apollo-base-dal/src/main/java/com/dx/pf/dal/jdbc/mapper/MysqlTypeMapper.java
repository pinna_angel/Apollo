/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.mapper;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/** 
* @ClassName: MysqlTypeMapping 
* @Description: mysql的字段映射关系
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 下午12:11:59 
* @version V1.0 
*/
public class MysqlTypeMapper implements IDialectMapper {

	/**
	 * mysql 数据类型对应类
	 */
	public static final Map<String, Class<?>> mapping = new HashMap<String, Class<?>>() {
		private static final long serialVersionUID = -1168963664693153914L;
		{
			put("INT", Integer.class);
			put("TINYINT", Integer.class);
			put("VARCHAR", String.class);
			put("CHAR", String.class);
			put("BLOB", String.class);
			put("TEXT", String.class);
			put("INTEGER", Long.class);
			put("TINYINT", Integer.class);
			put("SMALLINT", Integer.class);
			put("MEDIUMINT", Integer.class);
			put("BIT", Boolean.class);
			put("BIGINT", BigInteger.class);
			put("FLOAT", Float.class);
			put("DOUBLE", Double.class);
			put("DECIMAL", BigDecimal.class);
			put("BOOLEAN", Integer.class);
			put("ID", Long.class);
			put("DATE", Date.class);
			put("Time", Time.class);
			put("DATETIME", Date.class);
			put("TIMESTAMP", Date.class);
			put("YEAR", Date.class);
			put("LONGTEXT", String.class);
		}
	};

	@Override
	public Map<String, Class<?>> getMapper() {
		return mapping;
	}

}
