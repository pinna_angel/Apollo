/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月1日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.router;

/** 
* @ClassName: RoutingDBKeyHolder 
* @Description: 数据源路由holder
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月1日 上午1:33:48 
* @version V1.0 
*/
public class RoutingDBKeyHolder {

	/**放置数据源路由对应的key，采用堆栈的方式，增加扩展性*/
	private static final ThreadLocal<String> routingDbKeyHolder = new ThreadLocal<String>();
	
	
	/**
	 * 返回当前数据源的key
	 * @return
	 */
	public static String getCurrentDataSourceKey(){
		return routingDbKeyHolder.get();
	}
	
	/**
	 * 设置返钱数据源的key
	 * @param dataSourceKey
	 */
	public static void setCurrentDataSourceKey(String dataSourceKey){
		routingDbKeyHolder.set(dataSourceKey);
	}
	
	/**
	 * 清除当前数据源key
	 */
	public static void clean(){
		//TODO
		routingDbKeyHolder.remove();
		//routingDbKeyHolder.set(null);;
	}
}
