/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.mapper;

import java.sql.SQLException;
import java.util.Map;

/** 
* @ClassName: BeanMapper 
* @Description: 实体对象映射成map
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月5日 上午10:00:46 
* @version V1.0 
*/
public interface BeanMapper {

	/**
     * 实现该方法，用来将bean实体转换成map，key为数据表的column字段名称，对象为对应的值
     * @param object 实体对象
     */
    public Map<String,Object> mapBean(Object object) throws SQLException;
    
    /**
     * 获取解析bean时有值的属性个数
     * @return
     * @throws SQLException
     */
    public int getSizePropertyValue() throws SQLException;
}
