/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月6日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.sql;

import java.util.Map;

import com.dx.pf.dal.Logger;
import com.dx.pf.dal.exception.SQLParserException;

/** 
* @ClassName: SqlHelper 
* @Description: sql组装器
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月6日 上午10:17:38 
* @version V1.0 
*/
public class SqlHelper {

	private final static Logger logger = Logger.getLogger(SqlHelper.class);

	private static SqlHelper SqlHelper = null;

	private SqlOperator operate = null;

	private StringBuilder sql = new StringBuilder(512);

	// 非单例模式，主要是为了链式调用不使用new的方式
	private SqlHelper() {
	}

	/**
	 * 获取实例
	 * @return
	 */
	public static SqlHelper newInstance() {
		SqlHelper = new SqlHelper();
		return SqlHelper;
	}

	/**
	 * 插入操作
	 * @param clazz
	 * @return
	 */
	public SqlHelperInsert insert(Class<?> clazz) {
		if (operate != null) {
			logger.error("SQL已经设置了操作，不能重复设置。");
			throw new SQLParserException("SQL已经设置了操作，不能重复设置。");
		}
		operate = new SqlHelperInsert(this, clazz);
		return (SqlHelperInsert) operate;
	}

	/**
	 *更新操作
	 * @param clazz
	 * @return
	 */
	public SqlHelperUpdate update(Class<?> clazz) {
		if (operate != null) {
			logger.error("SQL已经设置了操作，不能重复设置。");
			throw new SQLParserException("SQL已经设置了操作，不能重复设置。");
		}
		operate = new SqlHelperUpdate(this, clazz);
		return (SqlHelperUpdate) operate;
	}

	/**
	 * 删除操作
	 * @param clazz
	 * @return
	 */
	public SqlHelperDelete delete(Class<?> clazz) {
		if (operate != null) {
			logger.error("SQL已经设置了操作，不能重复设置。");
			throw new SQLParserException("SQL已经设置了操作，不能重复设置。");
		}
		operate = new SqlHelperDelete(this, clazz);
		return (SqlHelperDelete) operate;
	}

	/**
	 * 查询操作
	 * @param clazz
	 * @return
	 */
	public SqlHelperSelect select(Class<?> clazz) {
		if (operate != null) {
			logger.error("SQL已经设置了操作，不能重复设置。");
			throw new SQLParserException("SQL已经设置了操作，不能重复设置。");
		}
		operate = new SqlHelperSelect(this, clazz);
		return (SqlHelperSelect) operate;
	}

	/**
	 * 获取名称参数
	 * @return
	 */
	public Map<String,Object> getNamedPerprotyParamMap(){
		return operate.executeMap;
	}
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		sql.append(operate.toString());
		return sql.toString();
	}
}
