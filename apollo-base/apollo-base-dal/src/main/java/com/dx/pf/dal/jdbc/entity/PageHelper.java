/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月6日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/** 
* @ClassName: PageHelper 
* @Description: 分页
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月6日 上午10:35:18 
* @version V1.0 
*/
public class PageHelper<T> implements Serializable {

	private static final long serialVersionUID = 1020034096427408707L;

	/** 总行数 */
	private long totalCount;
	/** 每页多少条记录 */
	private int pageSize;
	/** 当前第几页 */
	private int pageNo;
	/** 结果集数量 */
	private int rSum = 0;
	/** 结果集 */
	private List<T> items = new ArrayList<T>();

	public PageHelper(int pageSize, int pageNo) {
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	/**
	 * 取得总页数。
	 * @return 总页数
	 */
	public int getCountPages() {
		return (int) Math.ceil((double) totalCount / pageSize);
	}

	/**
	 * 取得总页数。
	 * @return 总页数
	 */
	public int getPages() {
		return (int) Math.ceil((double) totalCount / pageSize);
	}

	/**
	 * 取得当前页显示的项的起始序号 (1-based)。
	 * @return 起始序号
	 */
	public int getBeginIndex() {
		if (pageNo > 0) {
			return (pageSize * (pageNo - 1));
		} else {
			return 0;
		}
	}

	/**
	 * 取得当前页显示的末项序号 (1-based)。
	 * @return 末项序号
	 */
	public int getEndIndex() {
		if (pageNo > 0) {
			return (int) Math.min(pageSize * pageNo, totalCount);
		} else {
			return 0;
		}
	}

	/**
	 * 取得首页页码。
	 * @return 首页页码
	 */
	public int getFirstPage() {
		return calcPage(1);
	}

	/**
	 * 取得末页页码。
	 * @return 末页页码
	 */
	public int getLastPage() {
		return calcPage(getPages());
	}

	/**
	 * 取得前一页页码。
	 * @return 前一页页码
	 */
	public int getPreviousPage() {
		return calcPage(pageNo - 1);
	}

	/**
	 * 取得前n页页码
	 * @param n  前n页
	 * @return 前n页页码
	 */
	public int getPreviousPage(int n) {
		return calcPage(pageNo - n);
	}

	/**
	 * 取得后一页页码。
	 * @return 后一页页码
	 */
	public int getNextPage() {
		return calcPage(pageNo + 1);
	}

	/**
	 * 取得后n页页码。
	 * @param n 后n面
	 * @return 后n页页码
	 */
	public int getNextPage(int n) {
		return calcPage(pageNo + n);
	}

	/**
	 * 计算页数，但不改变当前页。
	 * @param page 页码
	 * @return 返回正确的页码(保证不会出边界)
	 */
	protected int calcPage(int page) {
		int pages = getPages();
		if (pages > 0) {
			return (page < 1) ? 1 : ((page > pages) ? pages : page);
		}
		return 0;
	}

	/**
	 * 判断指定页码是否被禁止，也就是说指定页码超出了范围或等于当前页码。
	 * @param page 页码
	 * @return boolean 是否为禁止的页码
	 */
	public boolean isDisabledPage(int page) {
		return ((page < 1) || (page > getPages()) || (page == this.pageNo));
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getrSum() {
		return rSum;
	}

	public void setrSum(int rSum) {
		this.rSum = rSum;
	}

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

}
