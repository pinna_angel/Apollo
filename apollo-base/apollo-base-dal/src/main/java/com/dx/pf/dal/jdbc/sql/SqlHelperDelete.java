/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月6日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.sql;

import java.util.Map;

import com.dx.pf.dal.Logger;
import com.dx.pf.dal.constants.DalConstant;

/**
 * @ClassName: SqlDelete
 * @Description: 删除语句的helper
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年5月6日 上午10:27:19
 * @version V1.0
 */
public class SqlHelperDelete extends SqlOperator {

	private final static Logger logger = Logger.getLogger(SqlHelperDelete.class);

	/** 查询sql */
	private StringBuilder deletesql = new StringBuilder(128);

	/** where部分 */
	private StringBuilder selwhere = new StringBuilder(64);

	
	public SqlHelperDelete(SqlHelper sqlHelper, Class<?> clazz) {
		super(sqlHelper, clazz);
		super.sql = new StringBuilder("DELETE FROM ");
	}

	/**
	 * 设置where条件
	 * @param condition
	 * @return
	 */
	public SqlHelperDelete setWhere(Condition condition) {
		selwhere.append(DalConstant.sql_Where).append(condition.getCondiSQL());
		putAllexecuteMap(condition.getConditionParam());
		return this;
	}

	@Override
	public SqlHelper builder() {
		deletesql.append(super.tableName).append(" ");
		deletesql.append(selwhere);
		super.sql.append(deletesql);
		logger.debug(super.sql.toString());
		return super.sqlHelper;
	}

	@Override
	protected void putAllexecuteMap(Map<String, Object> paramMap) {
		super.executeMap = paramMap;
	}

}
