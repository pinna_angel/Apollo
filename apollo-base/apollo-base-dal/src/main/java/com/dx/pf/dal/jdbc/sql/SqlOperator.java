/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月6日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc.sql;

import java.util.HashMap;
import java.util.Map;

import com.dx.pf.dal.cache.DalCache;
import com.dx.pf.dal.cache.EntryInfo;

/**
 * @ClassName: SqlOperator
 * @Description: sql的操作语句抽象类
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年5月6日 上午10:38:10
 * @version V1.0
 */
public abstract class SqlOperator {

	protected SqlHelper sqlHelper;
	
	protected StringBuilder sql = new StringBuilder(512);

	/** 参数 */
	protected Map<String, Object> paramMap = new HashMap<String, Object>();
	
	/**执行参数map,需要与sql中的:ColumnName对应*/
	protected Map<String, Object> executeMap = new HashMap<String, Object>();

	protected Class<?> clazz = null;

	protected EntryInfo entryInfo;
	
	protected String tableName;
	
	protected String pkColName;
	
	protected String shardColName;

	public SqlOperator(SqlHelper sqlHelper ,Class<?> clazz) {
		this.sqlHelper = sqlHelper;
		this.clazz = clazz;
		this.entryInfo = DalCache.getInstance().getEntryPropertyMap(clazz);
		this.tableName = entryInfo.getTableName();
		this.pkColName = entryInfo.getIdColName();
	}

	/**
	 * 构建操作sql
	 * @return
	 */
	public abstract SqlHelper builder();

	/**
	 * 切换数据源
	 * @param shardColName
	 * @param shardExpression
	 * @return
	 */
	public SqlOperator swapTable(String shardColName,String shardExpression){
		//TODO
		return this;
	}
	/**
	 * 参数是否存在，存在则需要换名
	 * @param paramName
	 * @return
	 */
	public boolean isHave(String paramName) {
		return paramMap.get(paramName) != null;
	}

	/**
	 * 获取参数名和值
	 * @return
	 */
	public Map<String, Object> getParamMap() {
		return paramMap;
	}

	/**
	 * @return the executeMap
	 */
	public Map<String, Object> getExecuteMap() {
		return executeMap;
	}

	/**
	 * 设置NamedparameterJDBC的执行参数
	 * @param paramMap
	 */
	protected abstract void putAllexecuteMap(Map<String, Object> paramMap);
	
	/**
	 * 获取sql的字符串
	 */
	@Override
	public String toString() {
		return sql.toString();
	}
}
