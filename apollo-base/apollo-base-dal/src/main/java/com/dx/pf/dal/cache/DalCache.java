/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月4日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.dx.pf.dal.exception.DalException;

/** 
* @ClassName: DalCache 
* @Description: 作为dal的bean属性缓存
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月4日 上午10:19:43 
* @version V1.0 
*/
public class DalCache {

	/**用来缓存存储实体属性**/
	private Map<String,EntryInfo> entryPropertyMap = new ConcurrentHashMap<String,EntryInfo>();
	
	/**用来缓存实体与实体的的关联关系**/
	private Map<String,EntryRelationInfo> entryRelationMap = new ConcurrentHashMap<String,EntryRelationInfo>();
	
	protected final Lock lock = new ReentrantLock();
	
	private DalCache() {
	}

	/**
	 * 获取dal的缓存单例
	 * @return
	 */
	public static DalCache getInstance() {
		return InstanceCache.instance;
	}

	/**
	 * 获取实体信息
	 * @param typeName 类全名
	 * @return the entryPropertyMap
	 */
	public EntryInfo getEntryPropertyMap(Class<?> clazz) {
		EntryInfo entryInfo = entryPropertyMap.get(clazz.getName());
		if(null == entryInfo){
			lock.lock();
			try {
				entryInfo = new EntryInfo(clazz);
				putEntryPropertyMap(clazz.getName(), entryInfo);
			} catch (Exception e) {
				throw new DalException("通过类clazz来解析成缓存错误。");
			}finally {
				lock.unlock();
			}
		}
		return entryInfo;
	}

	/**
	 * 添加实体信息
	 * @param typeName 类全名
	 * @param entryInfo 实体信息
	 * @param entryPropertyMap the entryPropertyMap to set
	 */
	public void putEntryPropertyMap(String typeName, EntryInfo entryInfo) {
		this.entryPropertyMap.put(typeName, entryInfo);
	}
	
	/**
	 * 批量添加实体信息
	 * @param entryPropertyMap the entryPropertyMap to set
	 */
	public void putAllEntryPropertyMap(Map<String, EntryInfo> entryPropertyMap) {
		this.entryPropertyMap.putAll(entryPropertyMap);;
	}

	/**
	 * 获取实体关系
	 * @return the entryRelationMap
	 */
	public EntryRelationInfo getEntryRelationMap(String name) {
		return entryRelationMap.get(name);
	}

	/**
	 * 添加实体关系
	 * @param entryRelationMap the entryRelationMap to set
	 */
	public void putEntryRelationMap(String name, EntryRelationInfo entryRelationInfo) {
		this.entryRelationMap.put(name, entryRelationInfo);
	}
	
	/**
	 * 添加表实体关系
	 * @param name
	 * @param entryRelationInfos
	 */
	public void putEntryRelationMap(String[] name, EntryRelationInfo[] entryRelationInfos) {
		Map<String, EntryRelationInfo> relationMap = new HashMap<String, EntryRelationInfo>();
		int index = 0;
		for (String key : name) {
			relationMap.put(key, entryRelationInfos[index]);
			index++;
		}
		this.entryRelationMap.putAll(relationMap);
	}
	
	/**
	 * 添加表实体关系
	 * @param relationMap
	 */
	public void putAllEntryRelationMap(Map<String, EntryRelationInfo> relationMap) {
		this.entryRelationMap.putAll(relationMap);
	}
	
	
	/**
	 * 清除实体信息
	 */
	public void cleanEntryInfo(){
		try {
			lock.lock();
			entryPropertyMap = new ConcurrentHashMap<String,EntryInfo>();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			lock.unlock();
		}
	}
	
	/**
	 * 清除实体关系信息
	 */
	public void cleanEntryRelationInfo(){
		try {
			lock.lock();
			entryRelationMap = new ConcurrentHashMap<String,EntryRelationInfo>();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			lock.unlock();
		}
	}
	
	/**
	* @ClassName: InstanceCache 
	* @Description: 静态内部类实现单例模式
	* @author wuzhenfang(wzfbj2008@163.com)
	* @date 2016年5月4日 下午2:09:46 
	* @version V1.0
	 */
	private static class InstanceCache {
		private static DalCache instance = new DalCache();
	}
}
