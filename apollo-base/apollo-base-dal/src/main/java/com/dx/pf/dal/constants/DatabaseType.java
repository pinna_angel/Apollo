/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.constants;

import com.dx.pf.dal.exception.UnSupportDBException;

/** 
* @ClassName: DatabaseType 
* @Description: 数据库类型 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 下午1:47:22 
* @version V1.0 
*/
public enum DatabaseType {

	H2, MySQL, Oracle, SQLServer, DB2;
	
	/**
     * 获取数据库类型枚举.
     * @param databaseProductName 数据库类型
     * @return 数据库类型枚举
     */
    public static DatabaseType valueFrom(final String databaseProductName) {
        try {
            return DatabaseType.valueOf(databaseProductName);
        } catch (final IllegalArgumentException ex) {
            throw new UnSupportDBException(databaseProductName);
        }
    }
	
}
