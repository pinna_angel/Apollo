/**
 * Project: apollo-base-dal
 * 
 * File Created at 2016年5月1日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.dal.jdbc;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.dx.pf.dal.Logger;
import com.dx.pf.dal.constants.OrderBy;
import com.dx.pf.dal.jdbc.mapper.BeanMapBeanMapper;
import com.dx.pf.dal.jdbc.sql.Condition;
import com.dx.pf.dal.jdbc.sql.SqlHelper;
import com.dx.pf.dal.jdbc.sql.SqlHelperSelect;

/** 
* @ClassName: SimpleJdbcTemplate 
* @Description: JDCB的自定义实现
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月1日 下午10:43:11 
* @version V1.0 
*/
public class SimpleJdbcTemplate<T> extends JdbcTemplate {


	private final static Logger logger = Logger.getLogger(BeanMapBeanMapper.class);

	private NamedParameterJdbcOperations nameParameterjdbc;

	private JdbcOperations jdbc;

	private Class<T> clazz;

	public SimpleJdbcTemplate() {
	}

	public SimpleJdbcTemplate(DataSource dataSource) {
		super(dataSource);
	}

	public boolean insert(T entity) {
		boolean retflag = false;
		try {
			SqlHelper sqlHelper = SqlHelper.newInstance().insert(clazz).setEntity(entity).builder();
			int count = nameParameterjdbc.update(sqlHelper.toString(), sqlHelper.getNamedPerprotyParamMap());
			retflag = count > 0;
		} catch (SQLException e) {
			logger.error("插入数据错误。", e);
		}
		return retflag;
	}

	public boolean delete(Condition condition) {
		boolean retflag = false;
		SqlHelper sqlHelper = SqlHelper.newInstance().delete(clazz).setWhere(condition).builder();
		int count = nameParameterjdbc.update(sqlHelper.toString(), sqlHelper.getNamedPerprotyParamMap());
		retflag = count > 0;
		return retflag;
	}

	public boolean update(Map<String, Object> paramMap, Condition condition) {
		boolean retflag = false;
		SqlHelper sqlHelper = SqlHelper.newInstance().update(clazz).setWhere(condition).builder();
		int count = nameParameterjdbc.update(sqlHelper.toString(), sqlHelper.getNamedPerprotyParamMap());
		retflag = count > 0;
		return retflag;
	}

	public int count(Condition condition) {
		SqlHelper sqlHelper = SqlHelper.newInstance().select(clazz).count().setWhere(condition).builder();
		int count = nameParameterjdbc.queryForObject(sqlHelper.toString(), sqlHelper.getNamedPerprotyParamMap(), Integer.class);
		return count;
	}

	public List<T> select(String[] columnNames, Condition condition, Map<String, OrderBy> orderByMap, int pageNo, int pageSize) {
		SqlHelperSelect selecter = SqlHelper.newInstance().select(clazz);
		if (columnNames != null && columnNames.length > 0) {
			selecter.setSeleteFields(columnNames);
		}
		if (condition != null) {
			selecter.setWhere(condition);
		}
		if (orderByMap != null && orderByMap.size() > 0) {
			for (Iterator<Entry<String, OrderBy>> itr = orderByMap.entrySet().iterator(); itr.hasNext();) {
				Entry<String, OrderBy> entry = itr.next();
				selecter.addOrderBy(entry.getKey(), entry.getValue());
			}
		}
		if (pageSize != 0 && pageNo > 0) {
			selecter.setPage(pageNo, pageSize);
		}
		SqlHelper sqlHelper = selecter.builder();

		List<T> list = (List<T>) nameParameterjdbc.queryForList(sqlHelper.toString(), sqlHelper.getNamedPerprotyParamMap(), clazz);
		return list;
	}

	/**
	 * @return the nameParameterjdbc
	 */
	public NamedParameterJdbcOperations getNameParameterjdbc() {
		return new NamedParameterJdbcTemplate(super.getDataSource());
	}

	/**
	 * @param nameParameterjdbc
	 * the nameParameterjdbc to set
	 */
	public void setNameParameterjdbc(NamedParameterJdbcOperations nameParameterjdbc) {
		this.nameParameterjdbc = nameParameterjdbc;
	}

	/**
	 * @return the jdbc
	 */
	public JdbcOperations getJdbc() {
		return jdbc;
	}

	/**
	 * @param jdbc the jdbc to set
	 */
	public void setJdbc(JdbcOperations jdbc) {
		this.jdbc = jdbc;
	}

	/**
	 * @return the clazz
	 */
	public Class<?> getClazz() {
		return clazz;
	}

	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

}
