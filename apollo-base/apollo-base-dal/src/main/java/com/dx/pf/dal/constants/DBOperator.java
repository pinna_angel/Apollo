/**
 * 
 */
package com.dx.pf.dal.constants;

/** 
 * @ClassName: DBOperator 
 * @Description: 数据库操作
 * @author wuzhenfang(zhenfangwu@capitalbio.com) 
 * @date 2015年6月1日 下午2:46:53 
 * @version V1.0 
 */
public enum DBOperator {
	select,update,insert,delete;
}
