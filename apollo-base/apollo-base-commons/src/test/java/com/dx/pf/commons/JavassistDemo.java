package com.dx.pf.commons;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;

/**
 * Created by Administrator on 2016/2/24.
 */
public class JavassistDemo {

    public static void main(String args[]) throws Exception  {
        ClassPool classPool = ClassPool.getDefault();
        CtClass cc = classPool.makeClass("foo");
        //定义code方法
        CtMethod method = CtNewMethod.make("public void code(){}", cc);
        //插入方法代码
        method.insertBefore("System.out.println(\"I'm a Programmer,Just Coding.....\");");
        method.insertAfter("int a = 1;");
        cc.addMethod(method);
        //保存生成的字节码
        cc.writeFile("c://Users//Administrator//Desktop//clazz");

    }
}
