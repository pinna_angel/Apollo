/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月26日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.io;

/** 
* @ClassName: StreamProgress 
* @Description: Stream进度条
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月26日 上午10:46:13 
* @version V1.0 
*/
public interface StreamProgress {

	/**
	 * 开始
	 */
	public void start();

	/**
	 * 进行中
	 * @param progressSize 已经进行的大小
	 */
	public void progress(long progressSize);

	/**
	 * 结束
	 */
	public void finish();
}
