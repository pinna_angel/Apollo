/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月16日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.constants;

/** 
* @ClassName: SystemPropertyConstant 
* @Description: 系统参数
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月16日 下午2:19:41 
* @version V1.0 
*/
public class SystemPropertyConstant {

	public final static String OS_NAME 				= "os.name";
	
	public final static String OS_VERSION			= "os.version";
	
	public final static String OS_ARCH 				= "os.arch";
	
	public final static String USER_LANGYAGE 		= "user.language";
}
