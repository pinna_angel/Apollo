/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.anno;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.dx.pf.commons.constants.AsyncConstant;

/** 
* @ClassName: Async 
* @Description: 异步处理注解 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:38:14 
* @version V1.0 
*/
@Target({ TYPE, FIELD, METHOD })
@Retention(RUNTIME)
public @interface Async {

	/**
	 * <p>
	 * 调用超时设置-单位毫秒(默认0-不超时)
	 * </p>
	 * @return
	 */
	public long timeout() default AsyncConstant.ASYNC_DEFAULT_TIME_OUT;

}
