/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.encrypt;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.dx.pf.commons.utils.StringUtil;


/**
 * @ClassName: Md5Util
 * @Description: MD5加密算法
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年3月30日 下午5:42:26
 * @version V1.0
 */
public class Md5Util {

	private static Charset charset = Charset.forName("UTF-8");

	public static byte[] md5(byte[] array) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] result = md.digest(array);
			return result;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] md5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] data = str.getBytes(charset);
			byte[] result = md.digest(data);
			return result;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static String md5Str(String str) {
		return StringUtil.toHexString(md5(str));
	}

	public static void main(String[] args) {
		System.out.println(Md5Util.md5Str("wuzhenfang"));
	}
}
