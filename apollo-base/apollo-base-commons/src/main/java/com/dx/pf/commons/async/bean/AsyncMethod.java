/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.bean;

import java.lang.reflect.Method;

/** 
* @ClassName: AsyncMethod 
* @Description: 异步执行方法 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:43:16 
* @version V1.0 
*/
public class AsyncMethod {

	public AsyncMethod(Object object, Method method, long timeout) {
		this.object = object;
		this.method = method;
		this.timeout = timeout;
	}

	private Object object;
	private Method method;
	private long timeout;

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
