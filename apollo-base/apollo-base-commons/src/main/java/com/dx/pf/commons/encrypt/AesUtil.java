/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.encrypt;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * @ClassName: AesUtil
 * @Description: aes加解密工具类，注意，该类为非线程安全
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年3月30日 下午5:24:02
 * @version V1.0
 */
public class AesUtil {

	private Cipher decryptCipher;
	private Cipher encrptCipher;

	public AesUtil(byte[] key) {
		if (key.length == 16) {
			try {
				SecretKey aeskey = new SecretKeySpec(key, "AES"); // 加密
				encrptCipher = Cipher.getInstance("AES");
				encrptCipher.init(Cipher.ENCRYPT_MODE, aeskey);
				decryptCipher = Cipher.getInstance("AES");
				decryptCipher.init(Cipher.DECRYPT_MODE, aeskey);
			} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
				throw new RuntimeException(e);
			}
		} else {
			throw new RuntimeException("默认只支持16byte的密钥");
		}
	}

	public void setPublicKey(byte[] publicKeyBytes) {
		throw new RuntimeException("AES为对称加密，无公钥");
	}

	public void setPrivateKey(byte[] privateKeyBytes) {
		throw new RuntimeException("AES为对称加密，无私钥");
	}

	public byte[] encrypt(byte[] src) {
		try {
			return encrptCipher.doFinal(src);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new RuntimeException(e);
		}
	}

	public byte[] decrypt(byte[] src) {
		try {
			return decryptCipher.doFinal(src);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new RuntimeException(e);
		}
	}

	public void setKey(byte[] key) {
	}

	public byte[] sign(byte[] src) {
		throw new RuntimeException("aes无签名功能");
	}

	public boolean check(byte[] src, byte[] sign) {
		throw new RuntimeException("aes无签名功能");
	}
}
