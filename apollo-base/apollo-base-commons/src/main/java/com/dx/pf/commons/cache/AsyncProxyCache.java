/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.cache;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.dx.pf.commons.anno.Async;
import com.dx.pf.commons.async.bean.AsyncMethod;
import com.dx.pf.commons.reflect.ClassUtil;
import com.dx.pf.commons.reflect.ReflectUtil;

/** 
* @ClassName: AsyncProxyCache 
* @Description: 异步代理缓存
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:41:01 
* @version V1.0 
*/
public class AsyncProxyCache {

	private static ConcurrentMap<String, Class<?>> proxyClasss = new ConcurrentHashMap<String, Class<?>>(100);
	private static ConcurrentMap<String, AsyncMethod> proxyMethods = new ConcurrentHashMap<String, AsyncMethod>(1000);

	/**
	 * <p>如果存在对应的key的ProxyClass就返回，没有则返回null</p>
	 * @param key
	 * @return
	 */
	public static Class<?> getProxyClass(String key) {
		return proxyClasss.get(key);
	}

	/**
	 * <p>注册对应的proxyClass到Map</p>
	 * @param key
	 * @param proxyClass
	 */
	public static void registerProxy(String key, Class<?> proxyClass) {
		proxyClasss.putIfAbsent(key, proxyClass);
	}

	public static void putAsyncMethod(String key, AsyncMethod asyncMethod) {
		proxyMethods.putIfAbsent(key, asyncMethod);
	}

	public static void registerMethod(Object bean, long timeout) {
		Method[] methods = bean.getClass().getDeclaredMethods();
		if (methods == null || methods.length == 0) {
			return;
		}
		for (Method method : methods) {
			Async annotation = ReflectUtil.findAsyncAnnatation(bean, method);
			if (annotation != null) {
				AsyncMethod asyncMethod = new AsyncMethod(bean, method, annotation.timeout());
				putAsyncMethod(ClassUtil.buildkey(bean, method), asyncMethod);
			}
		}
	}

	public static boolean containMethod(String key) {
		return proxyMethods.containsKey(key);
	}

	public static AsyncMethod getAsyncMethod(String key) {
		return proxyMethods.get(key);
	}
}
