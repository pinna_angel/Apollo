/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.inject;

import java.lang.reflect.Method;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.expression.spel.support.ReflectionHelper;
import org.springframework.stereotype.Component;

import com.dx.pf.commons.async.core.AsyncExecutor;
import com.dx.pf.commons.async.temple.AsyncTemplate;
import com.dx.pf.commons.constants.AsyncConstant;
import com.dx.pf.commons.reflect.ReflectUtil;
import com.dx.pf.commons.utils.StringUtil;
import com.dx.pf.commons.utils.ValidateUtil;

/** 
* @ClassName: AsyncSpringAOPSupport 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午2:31:52 
* @version V1.0 
*/
@Component
public class AsyncSpringAOPSupport  extends SpringBeanPostProcessor implements Ordered{

	@Value("${async.corePoolSize}")
    private String corePoolSize;
    
    @Value("${async.maxPoolSize}")
    private String maxPoolSize;
    
    @Value("${async.maxAcceptCount}")
    private String maxAcceptCount;
    
    @Value("${async.rejectedExecutionHandler}")
    private String rejectedExecutionHandler;
    
    @Value("${async.allowCoreThreadTimeout}")
    private String allowCoreThreadTimeout;
    
    @Value("${async.keepAliveTime}")
    private String keepAliveTime;
    
    @Value("${async.traced}")
    private String traced;
    
    public Object processAasynBean(Object bean, String beanName,boolean supportTransactional){
	
	Method[] methods = bean.getClass().getDeclaredMethods();
	if(methods == null || methods.length ==0){
	    return bean;
	}
	for (Method method : methods) {
	    if (ReflectUtil.findAsyncAnnatation(bean, method) != null) {
		return AsyncTemplate.getAsyncProxy(AsyncTemplate.ProxyType.CGLIB).buildProxy(bean, AsyncConstant.ASYNC_DEFAULT_SCAN_TIME_OUT, false);
	    }
	}
	return bean;
    }
    
    public int getOrder(){
	return Ordered.LOWEST_PRECEDENCE;
    }

    @Override
    public void destroy() throws Exception {
	AsyncExecutor.setIsDestroyed(true);
	AsyncExecutor.destroy();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
	Integer corePoolSize = null;
	Integer maxPoolSize = null;
	Integer maxAcceptCount = null;
	Long keepAliveTime = AsyncConstant.ASYNC_DEFAULT_KEEPALIVETIME;
	Boolean allowCoreThreadTimeout = true;
	
	if(!StringUtil.isEmpty(this.corePoolSize) && ValidateUtil.isNumber(this.corePoolSize)){
	    corePoolSize = Integer.valueOf(this.corePoolSize);
	}
	if(!StringUtil.isEmpty(this.maxPoolSize) && ValidateUtil.isNumber(this.maxPoolSize)){
	    maxPoolSize = Integer.valueOf(this.maxPoolSize);
	}
	if(!StringUtil.isEmpty(this.maxAcceptCount) && ValidateUtil.isNumber(this.maxAcceptCount)){
	    maxAcceptCount = Integer.valueOf(this.maxAcceptCount);
	}
	if(!StringUtil.isEmpty(this.keepAliveTime) && ValidateUtil.isNumber(this.keepAliveTime)){
	    keepAliveTime = Long.valueOf(this.keepAliveTime);
	}
	if(!StringUtil.isEmpty(this.allowCoreThreadTimeout)){
	    allowCoreThreadTimeout = Boolean.parseBoolean(this.allowCoreThreadTimeout);
	}
	if(!StringUtil.isEmpty(this.traced)){
	    AsyncConstant.ASYNC_DEFAULT_TRACE_LOG = Boolean.parseBoolean(this.traced);
	}
	
	AsyncExecutor.initPool(corePoolSize, maxPoolSize, maxAcceptCount, rejectedExecutionHandler,keepAliveTime,allowCoreThreadTimeout);
    }
}
