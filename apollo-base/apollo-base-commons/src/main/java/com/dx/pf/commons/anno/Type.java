/**
 * Project: core.common
 * 
 * File Created at 2016年3月31日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
* @ClassName: Type 
* @Description: 类或者表的注解
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年3月31日 上午10:25:44 
* @version V1.0 
*/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Type {
	/**
	 * 表名
	 * @return
	 */
	String value()default "";
}
