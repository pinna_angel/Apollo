/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月16日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.constants;

/** 
* @ClassName: SymbolConstant 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月16日 下午6:04:42 
* @version V1.0 
*/
public class SymbolConstant {

	/**,*/
	public final static String COMMA = ",";
	
	public final static String SLASH		 		 = "/";
	public final static String BACK_SLASH	 = "\\";
	/**:*/
	public final static String COLON    = ":"; 

	/** ; */
	public final static String SEMICOLON = ";";
	
	/**[*/
	public final static String SQUARE_BRACKETS_LEFT       = "[";
	/**]*/
	public final static String SQUARE_BRACKETS_RIGHT      = "]";
	
	public final static String CURLY_BRACKETS_LEFT        = "{";
	public final static String CURLY_BRACKETS_RIGHT       = "}";
	/**(*/
	public final static String PARENTHESES_BRACKETS_LEFT  = "(";
	/**)*/
	public final static String PARENTHESES_BRACKETS_RIGHT = ")";
	
	public final static String QUESTION_SIGN = "?";
	public final static String AND_SIGN = "&";
	/** # */
	public final static String POUND = "#";
	/** * */
	public final static String ASTERISK = "*";
	
	/** $ */
	public final static String DOLLAR_SIGN = "$";
	
	public final static String ELLIPSIS_THREE = "...";
	public final static String ELLIPSIS_SIX   = "......";
	
	/** = */
	public final static String EQUAL_SIGN = "=";
	/**-*/
	public final static String MINUS_SIGN = "-";
	public final static String PLUS_SIGN= "+";
	public final static String MULTIPLICATION_SIGN = "*";
	public final static String PERCENT = "%";
	
}
