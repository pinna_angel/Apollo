/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.reflect;

import java.io.StringWriter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ClassUtils;

/** 
* @ClassName: TypeUtil 
* @Description: 类型操作工具类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年3月30日 下午4:52:30 
* @version V1.0 
*/
public class TypeUtil<T> {

	private Type type;

	/**
	 * 获得超类的参数类型，取第一个参数类型 
	 */
	public TypeUtil() {
		ParameterizedType tmp = (ParameterizedType) (this.getClass().getGenericSuperclass());
		type = tmp.getActualTypeArguments()[0];
	}

	public Type getType() {
		return type;
	}
	
	/**
	 * 判断类型是否为数组
	 * @param type
	 * @return
	 */
	public static boolean isTypeArray(Class<?> type) {
		return (type != null && type.isArray());
	}

	/**
	 * 判断类型是否为{@link Collection}的实现类
	 * @param type
	 * @return
	 */
	public static boolean isTypeCollection(Class<?> type) {
		return (type != null && ClassUtils.isAssignable(type, Collection.class));
	}

	/**
	 * 判断类型是否为{@link List}的实现类
	 * @param type
	 * @return
	 */
	public static boolean isTypeList(Class<?> type) {
		return (type != null && ClassUtils.isAssignable(type, List.class));
	}

	/**
	 * 判断类型是否为基础类型
	 * @param type
	 * @return
	 */
	public static boolean isTypePrimitive(Class<?> type) {
		return (type != null && type.isPrimitive());
	}

	/**
	 * 判断类型是否为void（返回值）
	 * @param type
	 * @return
	 */
	public static boolean isTypeVoid(Class<?> type) {
		return (type != null && void.class.equals(type));
	}

	/**
	 * 判断类型是否为{@link Number}的父类
	 * @param type
	 * @return
	 */
	public static boolean isTypeNumber(Class<?> type) {
		return (type != null && ClassUtils.isAssignable(type, Number.class));
	}

	/**
	 * 判断类型是否为{@link Map}的实现类
	 * @param type
	 * @return
	 */
	public static boolean isTypeMap(Class<?> type) {
		return (type != null && ClassUtils.isAssignable(type, Map.class));
	}

	/**
	 * 判断类型是否为可进行String类操作的类型： CharSequence的实现类 (包括 StringBuffer、
	 * StringBuilder等)，StringWriter父类。
	 * @param type
	 * @return
	 */
	public static boolean isTypeString(Class<?> type) {
		// Consider any CharSequence (including StringBuffer and StringBuilder)
		// as a String.
		return (CharSequence.class.isAssignableFrom(type) || StringWriter.class.isAssignableFrom(type));
	}

	/**
	 * 判断是否为<code>java.util.Date</code>类型，但不为数据库的日期类型
	 */
	public static boolean isNotJdbcDateType(Class<?> type) {
		boolean isSqlDateType = java.sql.Date.class.isAssignableFrom(type)
				|| java.sql.Time.class.isAssignableFrom(type) || java.sql.Timestamp.class.isAssignableFrom(type);

		return (java.util.Date.class.isAssignableFrom(type) && !isSqlDateType);
	}
}
