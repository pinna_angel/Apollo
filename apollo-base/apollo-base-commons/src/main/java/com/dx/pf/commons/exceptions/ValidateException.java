/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月17日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.exceptions;

import com.dx.pf.commons.utils.StringUtil;

/** 
* @ClassName: ValidateException 
* @Description: 验证异常
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月17日 下午11:52:22 
* @version V1.0 
*/
public class ValidateException extends Exception {

	private static final long serialVersionUID = -7963525210922217184L;

	public ValidateException() {
	}

	public ValidateException(String msg) {
		super(msg);
	}

	public ValidateException(String messageTemplate, Object... params) {
		super(StringUtil.format(messageTemplate, params));
	}

	public ValidateException(Throwable throwable) {
		super(throwable);
	}

	public ValidateException(String msg, Throwable throwable) {
		super(msg, throwable);
	}
}
