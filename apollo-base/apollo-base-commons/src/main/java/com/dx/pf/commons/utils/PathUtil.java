/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

/** 
* @ClassName: PathUtil 
* @Description: 资源路径Util
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午7:45:15 
* @version V1.0 
*/
public class PathUtil {

	/**
	 * 获取java maven项目下的resource中的文件
	 * @param path
	 * @return
	 */
	public static String getResourcePath(String path) {
		return Thread.currentThread().getContextClassLoader().getResource(path).getPath();
	}
	
	/**
	 * 当前的路径
	 * @param cls
	 * @return
	 */
	public static String getCurrentPath(Class<?> clazz) {
		String path = "";
		try {
			path = clazz.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		} catch (Exception e) {
			path = clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
		}
		path = path.replaceFirst("file:/", "");
		path = path.replaceAll("!/", "");
		path = path.substring(0, path.lastIndexOf("/"));
		if (path.substring(0, 1).equalsIgnoreCase("/")) {
			String osName = System.getProperty("os.name").toLowerCase();
			if (osName.indexOf("window") >= 0) {
				path = path.substring(1);
			}
		}
		return path;
	}
	
	/**
	 * ClassPath (通常properties 目录)
	 * @return
	 */
	public static String classPath() {
		return Thread.currentThread().getContextClassLoader().getResource("").getPath();
	}

	/**
	 * Web跟路径 (通常为样式文件/静态页面 目录) (也可以根据request获取:request.getRealPath("/") )
	 * @return
	 */
	public static String webPath() {
		String realPath = classPath();
		int wei = realPath.lastIndexOf("WEB-INF/classes/");
		if (wei > -1) {
			realPath = realPath.substring(0, wei);
		}
		return realPath;
	}

	/**
	 * WEB-INF目录 (通常为template文件 目录)
	 * @return
	 */
	public static String webInfPath() {
		String realPath = classPath();
		int wei = realPath.lastIndexOf("WEB-INF/classes/");
		if (wei > -1) {
			realPath = realPath.substring(0, wei);
		}
		return realPath + "WEB-INF/";
	}

}
