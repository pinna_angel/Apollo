/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月26日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.reflect;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/** 
* @ClassName: Singleton 
* @Description: 单例类<br>
 * 提供单例对象的统一管理，当调用get方法时，如果对象池中存在此对象，返回此对象，否则创建新对象返回 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月26日 下午5:38:34 
* @version V1.0 
*/
public class Singleton {

	private static Map<Class<?>, Object> pool = new ConcurrentHashMap<Class<?>, Object>();

	private Singleton() {
		// 类对象
	}

	/**
	 * 获得指定类的单例对象<br>
	 * 对象存在于池中返回，否则创建，每次调用此方法获得的对象为同一个对象<br>
	 * @param clazz 类
	 * @return 单例对象
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public static <T> T get(Class<T> clazz, Object... params) throws Exception {
		T obj = (T) pool.get(clazz);

		if (null == obj) {
			synchronized (Singleton.class) {
				obj = (T) pool.get(clazz);
				if (null == obj) {
					obj = ClassUtil.newInstance(clazz, params);
					pool.put(clazz, obj);
				}
			}
		}

		return obj;
	}

	/**
	 * 获得指定类的单例对象<br>
	 * 对象存在于池中返回，否则创建，每次调用此方法获得的对象为同一个对象<br>
	 * @param className 类名
	 * @param params 构造参数
	 * @return 单例对象
	 * @throws Exception 
	 */
	public static <T> T get(String className, Object... params) throws Exception {
		final Class<T> clazz = ClassUtil.loadClass(className);
		return get(clazz, params);
	}

	/**
	 * 移除指定Singleton对象
	 * @param clazz 类
	 */
	public static void remove(Class<?> clazz) {
		pool.remove(clazz);
	}

	/**
	 * 清除所有Singleton对象
	 */
	public static void destroy() {
		pool.clear();
	}
}
