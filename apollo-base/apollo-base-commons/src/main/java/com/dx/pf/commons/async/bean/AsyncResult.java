/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.bean;

import java.io.Serializable;

/** 
* @ClassName: AsyncResult 
* @Description: 异步执行返回结果包装类</br>
* 				主要用于 void,array及Integer,Long,String,Boolean等Final修饰类.</br>
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:43:42 
* @version V1.0 
*/
public class AsyncResult<T> implements Serializable {

	private static final long serialVersionUID = 62481303716791572L;

	private boolean isSuccess;

	private T data;

	public AsyncResult() {
	}

	public AsyncResult(T data) {
		this.data = data;
	}

	public AsyncResult(boolean isSuccess, T data) {
		this.isSuccess = isSuccess;
		this.data = data;
	}

	/**
	 * @return the isSuccess
	 */
	public boolean isSuccess() {
		return isSuccess;
	}

	/**
	 * @param isSuccess the isSuccess to set
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public T getData() {
		return data;
	}

	public AsyncResult<T> setData(T data) {
		this.data = data;
		return this;
	}

}
