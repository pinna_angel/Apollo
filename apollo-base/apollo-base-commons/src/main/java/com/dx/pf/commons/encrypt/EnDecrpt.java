/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.encrypt;

/** 
* @ClassName: EnDecrpt 
* @Description: 加解密接口，注意，该接口的实现，都是非线程安全的，必须在每一个线程中都新建一个对象才可以使用
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年3月30日 下午5:35:14 
* @version V1.0 
*/
public interface EnDecrpt {

	/**
	 * 设置对称加密使用的密钥
	 * @param key
	 */
	public void setKey(byte[] key);

	/**
	 * 设置非对称加密的公钥
	 * @param publicKeyBytes
	 */
	public void setPublicKey(byte[] publicKeyBytes);

	/**
	 * 设置非对称加密的私钥
	 * @param privateKeyBytes
	 */
	public void setPrivateKey(byte[] privateKeyBytes);

	/**
	 * 加密原始信息
	 * @param src
	 * @return
	 */
	public byte[] encrypt(byte[] src);

	/**
	 * 解析加密信息
	 * @param src
	 * @return
	 */
	public byte[] decrypt(byte[] src);

	/**
	 * 对内容进行签名，返回签名信息
	 * @param src
	 * @return
	 */
	public byte[] sign(byte[] src);

	/**
	 * 使用签名信息对原文进行验证，返回验证结果
	 * @param src 原文内容
	 * @param sign 签名内容
	 * @return
	 */
	public boolean check(byte[] src, byte[] sign);
}
