/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月16日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.constants;

/** 
* @ClassName: EncodingConstant 
* @Description: 编码常量
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月16日 下午5:58:38 
* @version V1.0 
*/
public class EncodingConstant {
	
	public final static String GBK 			= "GBK";
	
	public final static String UTF8			= "UTF-8";
	
	public final static String ISO88591 	= "ISO8859-1";
}
