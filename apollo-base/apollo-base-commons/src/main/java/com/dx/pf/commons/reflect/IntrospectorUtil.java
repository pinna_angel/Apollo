/**
 * Project: core.common
 * 
 * File Created at 2016年4月1日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.reflect;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * @ClassName: IntrospectorUtils
 * @Description: PropertyDescriptor处理器，用于Bean的解析处理
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年4月1日 上午11:05:59
 * @version V1.0
 */
public class IntrospectorUtil {

	private static final Map<Class<?>, Map<String, PropertyDescriptor>> PROPERTY_DESCRIPTOR_CACHE = new WeakHashMap<Class<?>, Map<String, PropertyDescriptor>>();

	/**
	 * 获取指定类型所有的PropertyDescriptor
	 * @param clazz
	 * @return
	 * @throws IntrospectionException
	 */
	public static Map<String, PropertyDescriptor> getPropertyDescriptors(Class<?> clazz) throws IntrospectionException {
		Map<String, PropertyDescriptor> propertyDescriptors = PROPERTY_DESCRIPTOR_CACHE.get(clazz);

		if (propertyDescriptors == null) {
			BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
			propertyDescriptors = new HashMap<String, PropertyDescriptor>();
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor descriptor : descriptors) {
				String property = descriptor.getName();
				propertyDescriptors.put(property, descriptor);
			}
		}

		return propertyDescriptors;
	}
}
