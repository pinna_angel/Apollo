/**
 * Project: core.common
 * 
 * File Created at 2016年4月1日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

/** 
* @ClassName: UTF82GBKUtil 
* @Description: UTF-8转换GBK
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月1日 下午6:03:25 
* @version V1.0 
*/
public class UTF82GBKUtil {

	public static void main(String[] args) {
		//GBK编码格式源码路径 
			String srcDirPath = "D:\\Android\\WorkSpace\\TimeMirror-master\\TimeMirror-master\\src"; 
			//转为UTF-8编码格式源码路径 
			String utf8DirPath = "D:\\Android\\WorkSpace\\TimeMirror-master\\TimeMirror-master\\s"; 
			        
			//获取所有java文件 
			Collection<File> javaGbkFileCol =  FileUtils.listFiles(new File(srcDirPath), new String[]{"java"}, true); 
			        
			for (File javaGbkFile : javaGbkFileCol) { 
			      //UTF8格式文件路径 
			      String utf8FilePath = utf8DirPath+javaGbkFile.getAbsolutePath().substring(srcDirPath.length()); 
			       //使用GBK读取数据，然后用UTF-8写入数据 
			      try {
					FileUtils.writeLines(new File(utf8FilePath), "UTF-8", FileUtils.readLines(javaGbkFile, "GBK"));
				} catch (IOException e) {
					e.printStackTrace();
				}        
			}	
	}
}
