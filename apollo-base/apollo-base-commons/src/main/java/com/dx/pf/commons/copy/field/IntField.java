/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.copy.field;

import java.lang.reflect.Field;

/**
 * @ClassName: IntField
 * @Description: int字段复制
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年3月30日 下午5:08:29
 * @version V1.0
 */
public class IntField extends ObjectCopyField {
	public IntField(Field srcField, Field targetField) {
		super(srcField, targetField);
	}

	@Override
	public void copy(Object src, Object target) {
		unsafe.putInt(target, targetOffset, unsafe.getInt(src, srcOffset));
	}
}
