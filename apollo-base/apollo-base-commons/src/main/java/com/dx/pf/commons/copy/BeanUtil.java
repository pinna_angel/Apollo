/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年7月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.copy;

/**
 * @ClassName: BeanUtil
 * @Description: Bean工具类，主要用于属性值拷贝</br>
 *               一般情况下都用在更新操作上，用新对象的信息覆盖掉旧对象信息，然后update</br>
 *               copy属性可以include或者exclude属性 copy属性还可以不拷贝值为null的</br>
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年7月5日 上午10:56:56
 * @version V1.0
 */
public class BeanUtil {
	
	/**
	 * copy属性值
	 * @param source源
	 * @param target目标
	 */
	public static void copyProperties(Object source, Object target) {
		copyProperties(source, target, null, null, true);
	}

	/**
	 * copy属性值，不copy为空的属性值
	 * @param source源
	 * @param target目标
	 */
	public static void copyNotNullProperties(Object source, Object target) {
		copyProperties(source, target, null, null, false);
	}

	/**
	 * copy属性值
	 * @param source 源
	 * @param target 目标
	 * @param includes 复制哪些属性
	 * @param excludes 排除哪些属性
	 * @param copyNotNullProperty 是否copy为空的属性
	 */
	public static void copyProperties(Object source, Object target, String[] includes, String[] excludes, Boolean copyNotNullProperty) {
		//TODO 
	}

	/**
	 * copy属性值
	 * @param source 源
	 * @param target 目标
	 * @param includes 复制哪些属性
	 */
	public static void copyPropertiesInclude(Object source, Object target, String[] includes) {
		copyProperties(source, target, includes, null, true);
	}

	/**
	 * copy属性值，不copy为空的属性值
	 * @param source 源
	 * @param target 目标
	 * @param includes 复制哪些属性
	 */
	public static void copyNotNullPropertiesInclude(Object source, Object target, String[] includes) {
		copyProperties(source, target, includes, null, false);
	}

	/**
	 * copy属性值
	 * @param source 源
	 * @param target 目标
	 * @param excludes 排除哪些属性
	 */
	public static void copyPropertiesExclude(Object source, Object target, String[] excludes) {
		copyProperties(source, target, null, excludes, true);
	}

	/**
	 * copy属性值，不copy为空的属性值
	 * @param source 源
	 * @param target 目标
	 * @param excludes 排除哪些属性
	 */
	public static void copyNotNullPropertiesExclude(Object source, Object target, String[] excludes) {
		copyProperties(source, target, null, excludes, false);
	}
}
