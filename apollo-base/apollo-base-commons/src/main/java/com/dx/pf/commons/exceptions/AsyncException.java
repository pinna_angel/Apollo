/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.exceptions;

/** 
* @ClassName: AsyncException 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:58:37 
* @version V1.0 
*/
public class AsyncException extends RuntimeException {

	private static final long serialVersionUID = -486019930735027531L;

	public AsyncException() {
		super();
	}

	public AsyncException(String message, Throwable cause) {
		super(message, cause);
	}

	public AsyncException(String message) {
		super(message);
	}

	public AsyncException(Throwable cause) {
		super(cause);
	}
}
