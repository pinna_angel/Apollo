/**
 * Project: core.common
 * 
 * File Created at 2016年4月4日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
/** 
* @ClassName: FinalFieldUtil 
* @Description:修改final字段的值的工具类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月4日 上午2:21:44 
* @version V1.0 
*/
public class FinalFieldUtil {
	public static void setFinalFieldValue(Object target, Field field, Object value) {
    	try {
			field.setAccessible(true);
			final Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
			field.set(target, value);
			//设置完后再将修饰符改回去
			modifiersField.setInt(field, field.getModifiers() | Modifier.FINAL);
			modifiersField.setAccessible(false);
			field.setAccessible(false);
		} catch (Exception e) {
		}  
    }
}
