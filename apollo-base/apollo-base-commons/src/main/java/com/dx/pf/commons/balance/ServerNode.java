/**
 * Project: core.common
 * 
 * File Created at 2016年4月2日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.balance;

/** 
* @ClassName: ServerNode 
* @Description: 机器节点
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月2日 上午8:46:23 
* @version V1.0 
*/
public class ServerNode {

	private String name;

    private String ip;

    public ServerNode(String name, String ip) {
        this.name = name;
        this.ip = ip;
    }

    public ServerNode(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        if (name != null && !"".equals(name)) {
            return ip + "-" + name;
        }
        return ip;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        ServerNode node = (ServerNode) o;
        if (node.getIp() == null && ip == null && node.getName() == null && name == null) return true;
        if (name == null && node.getName() != null) return false;
        if (ip == null && node.getIp() != null) return false;
        assert ip != null;
        assert name != null;
        return name.equals(node.getName()) && ip.equals(node.getIp());
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        return result;
    }
}
