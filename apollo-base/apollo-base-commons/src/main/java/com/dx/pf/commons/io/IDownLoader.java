/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.io;

import java.io.File;

/** 
* @ClassName: IDownLoader 
* @Description: 下载器
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午7:39:07 
* @version V1.0 
*/
public interface IDownLoader {

	/**
	 * 下载
	 * @param url 下载地址
	 * @param path 下载路径,如果为Null,或者为“”，将默认当前工作路径
	 * @param timeout 超时时间 ，0的话为不限制时间，直到程序出现异常
	 * @return 下载后文件路径
	 * @throws Exception
	 */
	public File downLoad(String url, String path, int timeout) throws Exception;

}
