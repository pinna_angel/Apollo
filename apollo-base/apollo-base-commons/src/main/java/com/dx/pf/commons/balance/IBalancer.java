/**
 * Project: core.common
 * 
 * File Created at 2016年4月2日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.balance;

/** 
* @ClassName: Balancer 
* @Description: 负载均衡入口
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月2日 上午8:44:44 
* @version V1.0 
*/
public interface IBalancer {

	/**
     * uri路由
     * @param uriStr 热门uri字符串
     * @return 服务器节点
     */
    public ServerNode navigate(String uriStr);
}
