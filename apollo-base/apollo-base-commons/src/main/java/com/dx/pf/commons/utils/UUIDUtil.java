/**
 * Project: core.common
 * 
 * File Created at 2016年4月4日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

import java.util.UUID;

/** 
* @ClassName: UUIDUtil 
* @Description: UUID
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月4日 上午2:17:55 
* @version V1.0 
*/
public class UUIDUtil {
	public static String uuid(){
		String uuid =  UUID.randomUUID().toString();
		return uuid.replace("-", "");
	}
}
