/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.encrypt;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * @ClassName: DesUtil
 * @Description: DES 加解密工具类，注意，该类是非线程安全的
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年3月30日 下午5:25:51
 * @version V1.0
 */
public class DesUtil {

	private Cipher decryptCipher;
	private Cipher encrptCipher;

	/**
	 * 设置des加密使用的密钥
	 * @param key
	 */
	public void setKey(byte[] key) {
		try {
			SecretKey deskey = new SecretKeySpec(key, "DES"); // 加密
			encrptCipher = Cipher.getInstance("DES");
			encrptCipher.init(Cipher.ENCRYPT_MODE, deskey);
			decryptCipher = Cipher.getInstance("DES");
			decryptCipher.init(Cipher.DECRYPT_MODE, deskey);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 加密原始信息
	 * @param src
	 * @return
	 */
	public byte[] encrypt(byte[] src) {
		try {
			return encrptCipher.doFinal(src);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 解析加密信息
	 * @param src
	 * @return
	 */
	public byte[] decrypt(byte[] src) {
		try {
			return decryptCipher.doFinal(src);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new RuntimeException(e);
		}
	}

	public void setPublicKey(byte[] publicKeyBytes) {
		throw new RuntimeException("DES为对称加密，无公钥");
	}

	public void setPrivateKey(byte[] privateKeyBytes) {
		throw new RuntimeException("DES为对称加密，无私钥");
	}

	public byte[] sign(byte[] src) {
		throw new RuntimeException("des无签名功能");
	}

	public boolean check(byte[] src, byte[] sign) {
		throw new RuntimeException("des无签名功能");
	}
}
