/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月29日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.verify;

import com.dx.pf.commons.utils.StringUtil;

/** 
* @ClassName: Verify 
* @Description: 校验对象是否为空 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月29日 下午9:04:26 
* @version V1.0 
*/
public class Verify {

	public static void Null(Object target, String msg, Object... params) {
		if (target != null) {
			throw new RuntimeException(StringUtil.format(msg, params));
		}
	}

	public static void notNull(Object target, String msg, Object... params) {
		if (target == null) {
			throw new RuntimeException(StringUtil.format(msg, params));
		}
	}

	public static void False(boolean target, String msg, Object... params) {
		if (target) {
			throw new RuntimeException(StringUtil.format(msg, params));
		}
	}

	public static void True(boolean target, String msg, Object... params) {
		if (target == false) {
			throw new RuntimeException(StringUtil.format(msg, params));
		}
	}

	/**
	 * 对象target必须是类type的实例，否则抛出异常
	 * @param target
	 * @param type
	 * @param msg
	 * @param params
	 */
	public static void matchType(Object target, Class<?> type, String msg, Object... params) {
		if (target.getClass().equals(type) == false) {
			throw new RuntimeException(StringUtil.format(msg, params));
		}
	}

	/**
	 * 两个对象需要相等，否则抛出异常
	 * @param o1
	 * @param o2
	 * @param msg
	 * @param params
	 */
	public static void equal(Object o1, Object o2, String msg, Object... params) {
		if (o1.equals(o2) == false) {
			throw new RuntimeException(StringUtil.format(msg, params));
		}
	}

	public static void error(String msg, Object... params) {
		throw new RuntimeException(StringUtil.format(msg, params));
	}

	public static void exist(Object entity, String msg, Object... params) {
		if (entity == null) {
			throw new RuntimeException(StringUtil.format(msg, params));
		}
	}

}
