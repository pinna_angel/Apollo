/**
 * Project: core.common
 * 
 * File Created at 2016年4月5日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

/** 
* @ClassName: ZipUtil 
* @Description: 压缩工具类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月5日 上午10:59:37 
* @version V1.0 
*/
public class ZipUtil {

	private static final String DEFAULT_CHARSET = "UTF-8";

    /**
     * 压缩文件夹
     * @param zipFileName 打包后文件的名称，含路径
     * @param sourceFolder 需要打包的文件夹或者文件的路径
     * @param zipPathName 打包目的文件夹名,为空则表示直接打包到根
     */
    public static void zip(String zipFileName, String sourceFolder, String zipPathName) throws Exception {
        ZipOutputStream out = null;
        try {
            File zipFile = new File(zipFileName);

            FileUtil.createDirs(zipFile.getParent());
            out = new ZipOutputStream(zipFile);
            out.setEncoding(DEFAULT_CHARSET);
            if (StringUtils.isNotBlank(zipPathName)) {
                zipPathName = FilenameUtils.normalizeNoEndSeparator(zipPathName, true) + "/";
            } else {
                zipPathName = "";
            }
            zip(out, sourceFolder, zipPathName);
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception(e);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * 压缩文件夹
     *
     * @param zipFile
     *            a {@link java.lang.String} object.
     * @param source
     *            a {@link java.lang.String} object.
     */
    public static void zip(String zipFile, String source) throws Exception {
        File file = new File(source);
        zip(zipFile, source, file.isFile() ? StringUtils.EMPTY : file.getName());
    }

    /**
     * 压缩文件夹
     *
     * @param zipFile
     *            a {@link java.io.File} object.
     * @param source
     *            a {@link java.io.File} object.
     */
    public static void zip(File zipFile, File source) throws Exception {
        zip(zipFile.getAbsolutePath(), source.getAbsolutePath());
    }

    private static void zip(ZipOutputStream zos, String file, String pathName) throws IOException {
        File file2zip = new File(file);
        if (file2zip.isFile()) {
            zos.putNextEntry(new ZipEntry(pathName + file2zip.getName()));
            IOUtils.copy(new FileInputStream(file2zip.getAbsolutePath()), zos);
            zos.flush();
            zos.closeEntry();
        } else {
            File[] files = file2zip.listFiles();
            if (ArrayUtils.isNotEmpty(files)) {
                for (File f : files) {
                    if (f.isDirectory()) {
                        zip(zos, FilenameUtils.normalizeNoEndSeparator(f.getAbsolutePath(), true), 
                                FilenameUtils.normalizeNoEndSeparator(pathName + f.getName(), true) + "/");
                    } else {
                        zos.putNextEntry(new ZipEntry(pathName + f.getName()));
                        IOUtils.copy(new FileInputStream(f.getAbsolutePath()), zos);
                        zos.flush();
                        zos.closeEntry();
                    }
                }
            }
        }
    }

    /**
     * 解压
     *
     * @param fromZipFile
     *            zip文件路径
     * @param unzipPath
     *            解压路径
     */
    @SuppressWarnings("unchecked")
    public static final void unzip(String fromZipFile, String unzipPath) throws Exception {

        FileOutputStream fos = null;
        InputStream is = null;
        String path1 = StringUtils.EMPTY;
        String tempPath = StringUtils.EMPTY;

        if (!new File(unzipPath).exists()) {
            new File(unzipPath).mkdir();
        }
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(fromZipFile, DEFAULT_CHARSET);
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new Exception(e1);
        }
        File temp = new File(unzipPath);
        String strPath = temp.getAbsolutePath();
        Enumeration<ZipEntry> enu = zipFile.getEntries();
        ZipEntry zipEntry = null;
        while (enu.hasMoreElements()) {
            zipEntry = (ZipEntry) enu.nextElement();
            path1 = zipEntry.getName();
            if (zipEntry.isDirectory()) {
                tempPath = FilenameUtils.normalizeNoEndSeparator(strPath + File.separator + path1, true);
                File dir = new File(tempPath);
                dir.mkdirs();
                continue;
            } else {

                BufferedInputStream bis = null;
                BufferedOutputStream bos = null;
                try {
                    is = zipFile.getInputStream(zipEntry);
                    bis = new BufferedInputStream(is);
                    path1 = zipEntry.getName();
                    tempPath = FilenameUtils.normalizeNoEndSeparator(strPath + File.separator + path1, true);
                    FileUtil.createDirs(new File(tempPath).getParent());
                    fos = new FileOutputStream(tempPath);
                    bos = new BufferedOutputStream(fos);

                    IOUtils.copy(bis, bos);
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new Exception(e);
                } finally {
                    IOUtils.closeQuietly(bis);
                    IOUtils.closeQuietly(bos);
                    IOUtils.closeQuietly(is);
                    IOUtils.closeQuietly(fos);
                }
            }
        }
    }
}
