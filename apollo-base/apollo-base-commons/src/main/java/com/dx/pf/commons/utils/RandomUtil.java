/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

import java.util.concurrent.ThreadLocalRandom;

/** 
* @ClassName: RandomUtil 
* @Description: 随机数 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午7:58:08 
* @version V1.0 
*/
public class RandomUtil {

	private static final String numbers = "0123456789";
	
	private static final String lowerChars = "abcdefghijklmnopqrstuvwxyz";

	/**
	 * 随机字符串(数字组成)
	 * @param length
	 */
	public static String randNumberStr(int length) {
		StringBuffer result = new StringBuffer();
		ThreadLocalRandom rd = ThreadLocalRandom.current();
		for (int i = 0; i < length; i++) {
			result.append(numbers.charAt(Math.abs(rd.nextInt()) % numbers.length()));
		}
		return result.toString();
	}

	/**
	 * 随机字符串(字母组成)
	 * @param length
	 */
	public static String randLowerChars(int length) {
		StringBuffer result = new StringBuffer();
		ThreadLocalRandom rd = ThreadLocalRandom.current();
		for (int i = 0; i < length; i++) {
			result.append(lowerChars.charAt(Math.abs(rd.nextInt()) % lowerChars.length()));
		}
		return result.toString();
	}

	public static void main(String[] args) {
		System.out.println(randLowerChars(4));
	}

}
