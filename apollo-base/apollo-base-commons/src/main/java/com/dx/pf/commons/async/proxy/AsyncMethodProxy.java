/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.proxy;

import org.springframework.cglib.proxy.Callback;
import org.springframework.cglib.proxy.Enhancer;

import com.dx.pf.commons.cache.AsyncProxyCache;
import com.dx.pf.commons.constants.AsyncConstant;
import com.dx.pf.commons.exceptions.AsyncException;
import com.dx.pf.commons.log.Logger;
import com.dx.pf.commons.reflect.ClassUtil;
import com.dx.pf.commons.reflect.ReflectUtil;

/** 
* @ClassName: AsyncMethodProxy 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午4:42:43 
* @version V1.0 
*/
public class AsyncMethodProxy implements AsyncProxy {

	private final static Logger logger = Logger.getLogger(AsyncMethodProxy.class);

    @Override
    public Object buildProxy(Object target,boolean all) {
	return buildProxy(target, AsyncConstant.ASYNC_DEFAULT_TIME_OUT,all);
    }

    @Override
    public Object buildProxy(Object target, long timeout,boolean all) {
	Class<?> targetClass = ClassUtil.getClass(target);
	if (target instanceof Class) {
	    throw new AsyncException("target is not object instance");
	}
	Class<?> proxyClass = AsyncProxyCache.getProxyClass(targetClass.getName());
	if (proxyClass == null) {
	    Enhancer enhancer = new Enhancer();
	    if (targetClass.isInterface()) {
		enhancer.setInterfaces(new Class[] { targetClass });
	    } else {
		enhancer.setSuperclass(targetClass);
	    }
	    enhancer.setNamingPolicy(AsyncNamingPolicy.INSTANCE);
	    enhancer.setCallbackType(AsyncMethodInterceptor.class);
	    proxyClass = enhancer.createClass();
	    logger.debug("create proxy class:" + targetClass);
	    AsyncProxyCache.registerProxy(targetClass.getName(), proxyClass);
	    AsyncProxyCache.registerMethod(target,timeout);
	}
	Enhancer.registerCallbacks(proxyClass, new Callback[] { new AsyncMethodInterceptor(target, timeout,all)});
	Object proxyObject = null;
	try {
	    proxyObject = ReflectUtil.newInstance(proxyClass);
	} finally {
	    Enhancer.registerStaticCallbacks(proxyClass, null);
	}

	return proxyObject;
    }
}
