/**
 * Project: core.common
 * 
 * File Created at 2016年4月1日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

/** 
* @ClassName: ClassLoaderUtil 
* @Description: 获取类加载器
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月1日 上午11:13:12 
* @version V1.0 
*/
public class ClassLoaderUtil {

	/**
	 * 获取类加载器
	 * @return
	 */
	public static ClassLoader getClassLoader() {
		ClassLoader contextCL = Thread.currentThread().getContextClassLoader();
		ClassLoader loader = contextCL == null ? ClassLoaderUtil.class.getClassLoader() : contextCL;
		return loader;
	}
}
