/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.constants;

/** 
* @ClassName: OSType 
* @Description: 操作系统类型 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午7:50:00 
* @version V1.0 
*/
public enum OSType {

	Any("any"), 
	Linux("Linux"), 
	Mac_OS("Mac OS"),
	Mac_OS_X("Mac OS X"), 
	Windows("Windows"), 
	OS2("OS/2"), 
	Solaris("Solaris"), 
	SunOS("SunOS"), 
	MPEiX("MPE/iX"), 
	HP_UX("HP-UX"), 
	AIX("AIX"), 
	OS390("OS/390"), 
	FreeBSD("FreeBSD"), 
	Irix("Irix"), 
	Digital_Unix("Digital Unix"), 
	NetWare_411("NetWare"), 
	OSF1("OSF1"), 
	OpenVMS("OpenVMS"), 
	Others("Others");

	private OSType(String desc) {
		this.description = desc;
	}

	public String toString() {
		return description;
	}

	private String description;

}
