/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.proxy;

import org.springframework.cglib.core.DefaultNamingPolicy;

/** 
* @ClassName: AsyncNamingPolicy 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午4:44:35 
* @version V1.0 
*/
public class AsyncNamingPolicy extends DefaultNamingPolicy {
	
	public static final AsyncNamingPolicy INSTANCE = new AsyncNamingPolicy();

	@Override
	protected String getTag() {
		return "ByAsyncCGLIB";
	}
}
