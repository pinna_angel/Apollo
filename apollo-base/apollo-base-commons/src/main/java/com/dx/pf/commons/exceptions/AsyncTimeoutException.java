/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.exceptions;

/** 
* @ClassName: AsyncTimeoutException 
* @Description: 异步超时异常
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:59:12 
* @version V1.0 
*/
public class AsyncTimeoutException extends RuntimeException {

	private static final long serialVersionUID = 6186776020755449006L;

	public AsyncTimeoutException() {
		super();
	}

	public AsyncTimeoutException(String message, Throwable cause) {
		super(message, cause);
	}

	public AsyncTimeoutException(String message) {
		super(message);
	}

	public AsyncTimeoutException(Throwable cause) {
		super(cause);
	}

}
