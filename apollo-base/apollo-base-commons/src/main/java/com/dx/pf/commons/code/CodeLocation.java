/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月29日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.code;

/** 
* @ClassName: CodeLocation 
* @Description: 本地代码工具 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月29日 下午9:06:20 
* @version V1.0 
*/
public class CodeLocation {

	/**
	 * 调用该方法所在的行数信息
	 * @return
	 */
	public static String getCodeLocation() {
		return getCodeLocation(1);
	}

	/**
	 * 获取方法调用的信息.1代表调用这个方法所在的行,2代表再上一层
	 * @param deep
	 * @return
	 */
	public static String getCodeLocation(int deep) {
		StackTraceElement stackTraceElement = new Throwable().getStackTrace()[deep];
		int index = stackTraceElement.getClassName().lastIndexOf(".") + 1;
		StringBuilder strb = new StringBuilder(stackTraceElement.getClassName());
		strb.append(".");
		strb.append(stackTraceElement.getMethodName());
		strb.append("(");
		strb.append(stackTraceElement.getClassName().substring(index));
		strb.append(".java:");
		strb.append(stackTraceElement.getLineNumber());
		strb.append(")");
		return strb.toString();
	}
}
