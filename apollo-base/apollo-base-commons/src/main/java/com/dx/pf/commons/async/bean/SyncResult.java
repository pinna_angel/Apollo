/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.bean;

import java.io.Serializable;

/** 
* @ClassName: SyncResult 
* @Description: 同步执行返回结果包装类</br>
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:46:42 
* @version V1.0 
*/
public class SyncResult<T> implements Serializable {

	private static final long serialVersionUID = -674107746390159385L;

	private boolean isSuccess;
	
	private T value;

	private Throwable throwable;

	/**
	 * @return the isSuccess
	 */
	public boolean isSuccess() {
		return isSuccess;
	}

	/**
	 * @param isSuccess the isSuccess to set
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	
	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	public boolean isSucceed() {
		return throwable == null;
	}
}
