/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月29日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

/** 
* @ClassName: JVMUtil 
* @Description: JVM虚拟机工具
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月29日 下午9:18:39 
* @version V1.0 
*/
public class JVMUtil {

	/**
	 * 返回虚拟机使用内存量的一个估计值,内容为"xxM"
	 * @return
	 */
	public static String usedMemory() {
		Runtime runtime = Runtime.getRuntime();
		return (runtime.totalMemory() - runtime.freeMemory()) / 1024 / 1024 + "M";
	}
	
	/**
	 * 增加JVM停止时要做处理事件
	 */
	public static void addShutdownHook( Runnable runnable ) {
		Runtime.getRuntime().addShutdownHook( new Thread( runnable ) );
	}
}
