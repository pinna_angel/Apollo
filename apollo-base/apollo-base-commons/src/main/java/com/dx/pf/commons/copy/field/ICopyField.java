/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.copy.field;

/** 
* @ClassName: CopyField 
* @Description: 拷贝字段接口
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年3月30日 下午4:44:36 
* @version V1.0 
*/
public interface ICopyField {

	/**
     * 复制源对象的目标属性的值到目标对象的目标属性中
     * @param src
     * @param target
     */
    public void copy(Object src, Object target);
}
