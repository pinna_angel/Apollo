/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.core;

import com.dx.pf.commons.async.bean.Profiler;

/** 
* @ClassName: AsyncProfiler 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:53:30 
* @version V1.0 
*/
public class AsyncProfiler {

	private static ThreadLocal<Profiler> threadMap = new ThreadLocal<Profiler>();

	public static Profiler getAndSet(Profiler profiler) {
		if (profiler == null) {
			profiler = new Profiler(0);
		}
		threadMap.set(profiler);
		return profiler;
	}

	public static Profiler getAndIncrement() {
		Profiler profiler = threadMap.get();
		if (profiler == null) {
			return new Profiler(0);
		} else {
			profiler.getAndIncrement();
		}
		return profiler;
	}

	public static Profiler get() {
		return getAndSet(threadMap.get());
	}

	public static void release() {
		threadMap.remove();
	}
}
