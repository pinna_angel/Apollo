/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.copy.field;

import java.lang.reflect.Field;

import com.dx.pf.commons.reflect.ReflectUtil;

import sun.misc.Unsafe;

/** 
* @ClassName: ObjectCopyField 
* @Description: 对象复制字段 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年3月30日 下午4:46:09 
* @version V1.0 
*/
@SuppressWarnings("restriction")
public class ObjectCopyField implements ICopyField {

	protected static Unsafe unsafe = ReflectUtil.getUnsafe();
	protected long srcOffset;
	protected long targetOffset;

	public ObjectCopyField(Field srcField, Field targetField) {
		srcOffset = unsafe.objectFieldOffset(srcField);
		targetOffset = unsafe.objectFieldOffset(targetField);
	}

	/**
	 * object类型的就直接使用这个实现即可
	 */
	public void copy(Object src, Object target) {
		unsafe.putObject(target, targetOffset, unsafe.getObject(src, srcOffset));
	}

}
