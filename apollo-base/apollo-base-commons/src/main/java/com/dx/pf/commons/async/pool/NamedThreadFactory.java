/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.pool;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import com.dx.pf.commons.constants.AsyncConstant;

/** 
* @ClassName: NamedThreadFactory 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午3:55:04 
* @version V1.0 
*/
public class NamedThreadFactory implements ThreadFactory {

	final private String name;
	final private boolean daemon;
	final private ThreadGroup group;
	final private AtomicInteger threadNumber = new AtomicInteger(1);

	public NamedThreadFactory() {
		this(AsyncConstant.ASYNC_DEFAULT_THREAD_NAME, true);
	}

	public NamedThreadFactory(String name, boolean daemon) {
		this.name = name;
		this.daemon = daemon;
		SecurityManager s = System.getSecurityManager();
		group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
	}

	public Thread newThread(Runnable r) {
		Thread t = new Thread(group, r, name + "-" + threadNumber.getAndIncrement(), 0);
		t.setDaemon(daemon);
		if (t.getPriority() != Thread.NORM_PRIORITY) {
			t.setPriority(Thread.NORM_PRIORITY);
		}
		return t;
	}
}
