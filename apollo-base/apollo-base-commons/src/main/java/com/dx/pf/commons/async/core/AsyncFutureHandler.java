/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.async.core;

/** 
* @ClassName: AsyncFutureHandler 
* @Description: 异步执行完成事件接口
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:51:52 
* @version V1.0 
*/
public interface AsyncFutureHandler<T> {

	/**
	 * <p> 代理调用完成 </p>
	 * @param isTimeout 是否超时 </br>
	 *            true：超时则v返回null</br>
	 *            false:V v则有结果
	 * @param v
	 */
	public void processComplete(boolean isTimeout, T t);
}
