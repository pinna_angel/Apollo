/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/** 
* @ClassName: FixedThreadPool 
* @Description: 应用中唯一能创建的公用线程池的工具，在系统启动时创建
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午7:41:55 
* @version V1.0 
*/
public class FixedThreadPool {

	private static FixedThreadPool instance = null;
	
	private ExecutorService pool;

	private FixedThreadPool() {
		pool = Executors.newFixedThreadPool(36);
    }

	private FixedThreadPool(int threadNum) {
        if (threadNum <= 0) threadNum = 1;
        pool = Executors.newFixedThreadPool(threadNum);
    }

	/**
	 * 获取ThreadPool对象
	 *
	 * @return instance
	 */
	public static FixedThreadPool getInstance() {
		if (instance == null) {
			synchronized (FixedThreadPool.class) {
				if (instance == null)
					instance = new FixedThreadPool();
			}
		}
		return instance;
	}

	/**
	 * 获取ThreadPool对象
	 *
	 * @return instance
	 */
	public static FixedThreadPool getInstance(int threadNum) {
		if (instance == null) {
			synchronized (FixedThreadPool.class) {
				if (instance == null)
					instance = new FixedThreadPool(threadNum);
			}
		}
		return instance;
	}

	/**
	 * 获取ExecutorService
	 * @return
	 */
	public ExecutorService get() {
		return pool;
	}

	/**
	 * 销毁ExecutorService
	 */
	public void destroy() {
		pool.shutdown();
	}

	public void finalize() throws Throwable {
		this.destroy();
		super.finalize();
	}

}
