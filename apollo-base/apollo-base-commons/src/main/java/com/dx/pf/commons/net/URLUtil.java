/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月16日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.net;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.dx.pf.commons.log.Logger;
import com.dx.pf.commons.utils.StringUtil;

/** 
* @ClassName: URLUtil 
* @Description:统一资源定位符相关工具类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月16日 下午1:11:31 
* @version V1.0 
*/
public class URLUtil {

	private static Logger logger = Logger.getLogger(URLUtil.class);

	/**
	 * 获得URL
	 * @param pathBaseClassLoader 相对路径（相对于classes）
	 * @return URL
	 */
	public static URL getURL(String pathBaseClassLoader) {
		return URLUtil.class.getClassLoader().getResource(pathBaseClassLoader);
	}

	/**
	 * 获得URL
	 * @param path 相对给定 class所在的路径
	 * @param clazz 指定class
	 * @return URL
	 */
	public static URL getURL(String path, Class<?> clazz) {
		return clazz.getResource(path);
	}

	/**
	 * 获得URL，常用于使用绝对路径时的情况
	 * @param configFile URL对应的文件对象
	 * @return URL
	 */
	public static URL getURL(File configFile) {
		try {
			return configFile.toURI().toURL();
		} catch (MalformedURLException e) {
			logger.error("Error occured when get URL!", e);
		}
		return null;
	}
	
	/**
	 * 格式化URL链接
	 * @param url 需要格式化的URL
	 * @return 格式化后的URL，如果提供了null或者空串，返回null
	 */
	public static String formatUrl(String url) {
		if (StringUtil.isBlank(url)){
			return null;
		}
		if (url.startsWith("http://") || url.startsWith("https://")){
			return url;
		}
		return "http://" + url;
	}

	/**
	 * 补全相对路径
	 * 
	 * @param baseUrl 基准URL
	 * @param relativePath 相对URL
	 * @return 相对路径
	 * @throws Exception 
	 * @exception UtilException MalformedURLException
	 */
	public static String complateUrl(String baseUrl, String relativePath) throws Exception {
		baseUrl = formatUrl(baseUrl);
		if (StringUtil.isBlank(baseUrl)) {
			return null;
		}

		try {
			final URL absoluteUrl = new URL(baseUrl);
			final URL parseUrl = new URL(absoluteUrl, relativePath);
			return parseUrl.toString();
		} catch (MalformedURLException e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 编码URL
	 * @param url URL
	 * @param charset 编码
	 * @return 编码后的URL
	 * @throws Exception 
	 * @exception UtilException UnsupportedEncodingException
	 */
	public static String encode(String url, String charset) throws Exception {
		try {
			return URLEncoder.encode(url, charset);
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 解码URL
	 * @param url URL
	 * @param charset 编码
	 * @return 解码后的URL
	 * @throws Exception 
	 * @exception UtilException UnsupportedEncodingException
	 */
	public static String decode(String url, String charset) throws Exception {
		try {
			return URLDecoder.decode(url, charset);
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 获得path部分<br>
	 * URI -> http://www.aaa.bbb/search?scope=ccc&q=ddd
	 * PATH -> /search
	 * @param uriStr URI路径
	 * @return path
	 * @throws Exception 
	 * @exception UtilException URISyntaxException
	 */
	public static String getPath(String uriStr) throws Exception{
		URI uri = null;
		try {
			uri = new URI(uriStr);
		} catch (URISyntaxException e) {
			throw new Exception(e);
		}
		
		return uri == null ? null : uri.getPath();
	}
}
