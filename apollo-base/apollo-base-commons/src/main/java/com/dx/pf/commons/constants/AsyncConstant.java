/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月25日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.constants;

/** 
* @ClassName: AsyncConstant 
* @Description:异步处理框架的常量
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月25日 下午1:40:04 
* @version V1.0 
*/
public class AsyncConstant {

	public static final String ASYNC_DEFAULT_THREAD_NAME = "Async-Pool"; //线程名称

    public static final long ASYNC_DEFAULT_TIME_OUT= 0; //默认执行任务超时时间-单位毫秒（0表示不限制超时）

    public static final long ASYNC_DEFAULT_SCAN_TIME_OUT= -1; //默认执行任务超时时间-单位毫秒（0表示不限制超时）

    public static final long ASYNC_DEFAULT_KEEPALIVETIME= 10000l; //默认线程空闲超时时间

    public static boolean ASYNC_DEFAULT_TRACE_LOG = false; //默认跟踪日志关闭
}
