/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

import java.util.concurrent.ThreadLocalRandom;

/** 
* @ClassName: RandomString 
* @Description: 随机字符串 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午7:59:34 
* @version V1.0 
*/
public class RandomString {


	private static ThreadLocalRandom random = ThreadLocalRandom.current();
	
	private static char[] numbers = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
	
	private static char[] charAndNumbers = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A',
			'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', '0' };

	/**
	 * 得到一个随机的数字串，位数由参数指定
	 * @param size
	 * @return
	 */
	public static String getNumber(int size) {
		char[] tmp = new char[size];
		for (int i = 0; i < size; i++) {
			tmp[i] = numbers[random.nextInt(10)];
		}
		return new String(tmp);
	}

	/**
	 * 返回一个长度为size的随机字符串，可能包含数字和字母
	 * @param size
	 * @return
	 */
	public static String randomString(int size) {
		char[] tmp = new char[size];
		for (int i = 0; i < size; i++) {
			tmp[i] = charAndNumbers[random.nextInt(62)];
		}
		return new String(tmp);
	}

}
