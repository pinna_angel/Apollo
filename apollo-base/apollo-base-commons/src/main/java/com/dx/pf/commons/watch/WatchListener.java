/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月16日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.watch;

import java.nio.file.WatchEvent;

/** 
* @ClassName: WatchListener 
* @Description: 监听器
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月16日 下午7:23:38 
* @version V1.0 
*/
public interface WatchListener {

	/**
	 * 文件创建时执行的方法
	 * @param event 事件
	 */
	public void onCreate(WatchEvent<?> event);

	/**
	 * 文件修改时执行的方法<br>
	 * 文件修改可能触发多次
	 * @param event 事件
	 */
	public void onModify(WatchEvent<?> event);

	/**
	 * 文件删除时执行的方法
	 * @param event 事件
	 */
	public void onDelete(WatchEvent<?> event);

	/**
	 * 事件丢失或出错时执行的方法
	 * @param event 事件
	 */
	public void onOverflow(WatchEvent<?> event);
}
