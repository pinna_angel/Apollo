/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.collection.set;

/**
 * @ClassName: Node
 * @Description: 节点
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年3月30日 下午4:33:13
 * @version V1.0
 */
public class Node<T> {

	private volatile Node<T> next;
	
	private T value;

	public Node(T value) {
		this.value = value;
	}

	public Node(Node<T> next, T value) {
		this.next = next;
		this.value = value;
	}

	public Node<T> next() {
		return next;
	}

	public void setNext(Node<T> next) {
		this.next = next;
	}

	public T value() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
}
