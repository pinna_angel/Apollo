/**
 * Project: core.common
 * 
 * File Created at 2016年4月5日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.utils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** 
* @ClassName: UrlUtil 
* @Description:uri工具类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月5日 上午12:27:38 
* @version V1.0 
*/
public class UrlUtil {
	/**
     * 判断是否是合法url
     * @param urlString url字符串
     * @return
     */
    public static boolean isValidUrl(String urlString) {
        String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        Pattern patt = Pattern.compile(regex);
        Matcher matcher = patt.matcher(urlString);
        return matcher.matches();
    }
    
    /**
	 * 获得URL
	 * @param pathBaseClassLoader 相对路径（相对于classes）
	 * @return URL
	 */
	public static URL getURL(String pathBaseClassLoader){
		return UrlUtil.class.getClassLoader().getResource(pathBaseClassLoader);
	}
	
	/**
	 * 获得URL
	 * @param path 相对给定 class所在的路径
	 * @param clazz 指定class
	 * @return URL
	 */
	public static URL getURL(String path, Class<?> clazz){
		return clazz.getResource(path);
	}
	
	/**
	 * 获得URL，常用于使用绝对路径时的情况
	 * @param configFile URL对应的文件对象
	 * @return URL
	 */
	public static URL getURL(File configFile){
		try {
			return configFile.toURI().toURL();
		} catch (MalformedURLException e) {
		}
		return null;
	}
}
