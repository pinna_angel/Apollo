/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.collection.set;

import java.util.Iterator;

/**
 * @ClassName: SetIterator
 * @Description: set迭代器
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年3月30日 下午4:32:39
 * @version V1.0
 */
public class SetIterator<T> implements Iterator<T> {

	private Node<T> node;

	private LightSet<T> set;

	public SetIterator(LightSet<T> set) {
		this.set = set;
		node = set.getHead();
	}

	@Override
	public boolean hasNext() {
		if (node.next() != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public T next() {
		node = node.next();
		return node.value();
	}

	@Override
	public void remove() {
		set.removeNode(node);
	}
}
