/**
 * Project: apollo-base-commons
 * 
 * File Created at 2016年10月16日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.constants;

/** 
* @ClassName: HttpConstant 
* @Description: HTTP常量
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年10月16日 下午6:06:41 
* @version V1.0 
*/
public class HttpConstant {

	/** HTTP Method */
	public static String REQUEST_METHOD_GET = "get";
	public static String REQUEST_METHOD_POST = "post";

	/** HTTP Prefix */
	public static String HTTP_PREFIX = "http://";
	public static String HTTPS_PREFIX = "https://";

	public static String REQUEST_ENCODING_GET = "request-encoding-get";
	public static String REQUEST_ENCODING_POST = "request-encoding-post";

}
