/**
 * Project: core.common
 * 
 * File Created at 2016年3月30日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.commons.collection;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @ClassName: ByteCachePool
 * @Description: byte数组缓存池
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年3月30日 下午4:20:49
 * @version V1.0
 */
public class ByteCachePool {

	private static Queue<ByteCache> queue = new ConcurrentLinkedQueue<>();

	/**
	 * 获取一个cache
	 * @return
	 */
	public static ByteCache get() {
		ByteCache cache = queue.poll();
		if (cache == null) {
			cache = new ByteCache();
		}
		return cache;
	}

	/**
	 * 归还使用的cache
	 * @param cache
	 */
	public static void returnCache(ByteCache cache) {
		cache.clear();
		queue.offer(cache);
	}

	/**
	 * 缩小当前的缓存池
	 */
	public static void shink() {
		queue.clear();
	}
}
