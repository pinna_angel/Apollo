/**
 * Project: apollo-base-mvc
 * 
 * File Created at 2016年6月3日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.constant;

/** 
* @ClassName: DataType 
* @Description: 定义数据类型
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年6月3日 下午3:56:21 
* @version V1.0 
*/
public class DataType {
	public static final String STRING = "java.lang.String";
	public static final String BOOLEAN = "java.lang.Boolean";
	public static final String INTEGER = "java.lang.Integer";
	public static final String DOUBLE = "java.lang.Double";
	public static final String FLOAT = "java.lang.Float";
	public static final String LONG = "java.lang.Long";
	public static final String BIGDECIMAL = "java.math.BigDecimal";
	public static final String DATE = "java.util.Date";
	public static final String TIME = "java.sql.Time";
	public static final String TIMESTAMP = "java.sql.Timestamp";
}
