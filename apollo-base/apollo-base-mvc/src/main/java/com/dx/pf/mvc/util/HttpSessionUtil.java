/**
 * Project: apollo-base-mvc
 * 
 * File Created at 2016年4月29日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.mvc.util;

import javax.servlet.http.HttpSession;

/** 
* @ClassName: HttpSessionUtil 
* @Description: HTTP.SESSION操作
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月29日 下午10:24:31 
* @version V1.0 
*/
public class HttpSessionUtil {

	/**
	 * 存入
	 * @param session
	 * @param key
	 * @param value
	 */
	public static void set(HttpSession session, String key, Object value) {
		session.setMaxInactiveInterval(30 * 60); // 失效时间: 30分钟
		session.setAttribute(key, value);
	}

	/**
	 * 取出
	 * @param session
	 * @param key
	 * @return
	 */
	public static Object get(HttpSession session, String key) {
		return session.getAttribute(key);
	}

	/**
	 * 移除
	 * @param session
	 * @param key
	 */
	public static void remove(HttpSession session, String key) {
		session.removeAttribute(key);
	}

}
