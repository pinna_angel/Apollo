/**
 * Project: apollo-base-mvc
 * 
 * File Created at 2016年5月18日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.core;

import java.util.HashMap;
import java.util.Map;

/** 
* @ClassName: FileUploadProgress 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月18日 上午8:51:28 
* @version V1.0 
*/
public class FileUploadProgress {

	private long bytesRead;
	private long contentLength;
	private int items;

	public int getProgress() {
		Double a = Double.parseDouble(getBytesRead() + "");
		Double b = Double.parseDouble(getContentLength() + "");
		return (int) (a / b * 100);
	}

	public boolean isCompleted() {
		return getBytesRead() >= getContentLength();
	}

	public Map<String, Object> getStatusMap() {
		Map<String, Object> statusMap = new HashMap<>();
		statusMap.put("progress", getProgress());
		statusMap.put("completed", isCompleted());
		statusMap.put("item", items);
		return statusMap;
	}

	public long getBytesRead() {
		return bytesRead;
	}

	public void setBytesRead(long bytesRead) {
		this.bytesRead = bytesRead;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}

	public int getItems() {
		return items;
	}

	public void setItems(int items) {
		this.items = items;
	}
}
