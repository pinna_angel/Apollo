/**
 * Project: apollo-base-mvc
 * 
 * File Created at 2016年6月3日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.mvc;

import com.dx.pf.commons.reflect.ClassUtil;
import com.dx.pf.commons.reflect.BeanUtil;
import com.dx.pf.commons.reflect.BeanUtil.ValueProvider;
import com.dx.pf.commons.utils.StringUtil;

/** 
* @ClassName: BaseController 
* @Description:基础控制器类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年6月3日 下午4:02:36 
* @version V1.0 
*/
public abstract class BaseController {

	
	/**
	 * ServletRequest 参数转Bean
	 * @param request ServletRequest
	 * @param beanClass Bean Class
	 * @return Bean
	 * @throws Exception 
	 */
	public static <T> T requestParamToBean(javax.servlet.ServletRequest request, Class<T> beanClass) throws Exception {
		return fillBeanWithRequestParam(request, ClassUtil.newInstance(beanClass));
	}
	/**
	 * ServletRequest 参数转Bean
	 * @param request ServletRequest
	 * @param bean Bean
	 * @return Bean
	 * @throws Exception 
	 */
	public static <T> T fillBeanWithRequestParam(final javax.servlet.ServletRequest request, T bean) throws Exception {
		final String beanName = StringUtil.lowerFirst(bean.getClass().getSimpleName());
		return BeanUtil.fill(bean, new ValueProvider(){
			@Override
			public Object value(String name) {
				String value = request.getParameter(name);
				if (StringUtil.isEmpty(value)) {
					// 使用类名前缀尝试查找值
					value = request.getParameter(beanName + StringUtil.DOT + name);
					if (StringUtil.isEmpty(value)) {
						// 此处取得的值为空时跳过，包括null和""
						value = null;
					}
				}
				return value;
			}
		});
	}
}
