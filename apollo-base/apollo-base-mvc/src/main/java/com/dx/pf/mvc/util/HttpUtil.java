/**
 * Project: apollo-base-mvc
 * 
 * File Created at 2016年4月30日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.mvc.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;

/** 
* @ClassName: HttpUtil 
* @Description: 有关HTTP请求的工具类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月30日 上午7:30:16 
* @version V1.0 
*/
public class HttpUtil {


	/**
	 * 判断来的请求是否是异步请求
	 * @param request
	 * @return
	 */
    public static boolean isAsynRequest(HttpServletRequest request) {
    	return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

  //Class<T> beanClass可以接受任何类型的javaBean,使用泛型调用者不用进行强转  
    public static <T> T request2Bean(HttpServletRequest request,Class<T> beanClass){  
        try{  
            //创建封装数据的bean  
            T bean = beanClass.newInstance();  
            Map map = request.getParameterMap();  
            BeanUtils.populate(bean, map);    
            return bean;  
        }catch (Exception e) {  
            throw new RuntimeException(e);  
        }  
    }  
}
