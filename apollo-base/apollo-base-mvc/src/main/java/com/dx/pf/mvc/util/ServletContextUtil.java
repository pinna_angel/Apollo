/**
 * Project: apollo-base-mvc
 * 
 * File Created at 2016年4月29日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.pf.mvc.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;

/** 
* @ClassName: ServletContextUtil 
* @Description: ServletContext操作 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月29日 下午10:02:16 
* @version V1.0 
*/
public class ServletContextUtil {

	/**
	 * 存入
	 * @param application
	 * @param key
	 * @param value
	 */
	public static void set(ServletContext application, String key, Object value) {
		application.setAttribute(key, value);
	}

	/**
	 * 取出
	 * @param application
	 * @param key
	 * @return
	 */
	public static Object get(ServletContext application, String key) {
		return application.getAttribute(key);
	}

	/**
	 * 移除
	 * @param application
	 * @param key
	 */
	public static void remove(ServletContext application, String key) {
		application.removeAttribute(key);
	}

	/**
     * servletContext.log servletContext相关信息,一般启动时调用.
     * @param servletContext the servlet context
     * @return the servlet context info map for log
     */
    public static Map<String, Object> getServletContextInfoMapForLog(ServletContext servletContext){
        Map<String, Object> map = new HashMap<>();
        // 返回servlet运行的servlet 容器的版本和名称.
        map.put("servletContext.getServerInfo()", servletContext.getServerInfo());
        // 返回这个servlet容器支持的Java Servlet API的主要版本.所有符合2.5版本的实现,必须有这个方法返回的整数2.
        // 返回这个servlet容器支持的Servlet API的次要版本.所有符合2.5版本的实现,必须有这个方法返回整数5.
        map.put("servlet version:", servletContext.getMajorVersion() + "." + servletContext.getMinorVersion());
        map.put("servletContext.getContextPath()", servletContext.getContextPath());
        map.put("servletContext.getServletContextName()", servletContext.getServletContextName());
        return map;
    }

	
	/**
     * 遍历显示servletContext的 {@link javax.servlet.ServletContext#getAttributeNames() ServletContext.getAttributeNames()},
     * 将 name/attributeValue存入到map.
     * @param servletContext the servlet context
     * @return 如果{@link javax.servlet.ServletContext#getAttributeNames() ServletContext.getAttributeNames()} 
     * 是null或者empty,返回 {@link Collections#emptyMap()}<br>
     */
    public static Map<String, Object> getAttributeMap(ServletContext servletContext){
        Enumeration<String> attributeNames = servletContext.getAttributeNames();
        if (isNullOrEmpty(attributeNames)){
            return emptyMap();
        }

        Map<String, Object> map = new TreeMap<>();
        while (attributeNames.hasMoreElements()){
            String name = attributeNames.nextElement();

            map.put(name, servletContext.getAttribute(name));
        }
        return map;
    }

	
	/**
     * 遍历显示servletContext的 {@link ServletContext#getInitParameterNames()},将 name /attributeValue 存入到map返回.
     * @param servletContext  the servlet context
     * @return 如果 {@link javax.servlet.ServletContext#getInitParameterNames() ServletContext.getInitParameterNames()} 是null或者empty,返回
     *         {@link Collections#emptyMap()}<br>
     * @see javax.servlet.ServletContext#getInitParameterNames()
     * @see "org.springframework.web.context.support#registerEnvironmentBeans(ConfigurableListableBeanFactory, ServletContext)"
     */
    public static Map<String, String> getInitParameterMap(ServletContext servletContext){
        Enumeration<String> initParameterNames = servletContext.getInitParameterNames();
        if (isNullOrEmpty(initParameterNames)){
            return emptyMap();
        }

        Map<String, String> map = new TreeMap<>();
        while (initParameterNames.hasMoreElements()){
            String name = initParameterNames.nextElement();
            map.put(name, servletContext.getInitParameter(name));
        }
        return map;
    }
	
}
