/**
 * Project: dx.zk.client
 * 
 * File Created at 2016年4月13日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.zkClient.watch;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

/** 
* @ClassName: ZKWatchRegister 
* @Description: 注册监听事件 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月13日 下午2:40:44 
* @version V1.0 
*/
public class ZKWatchRegister implements Watcher {

	private String registerNode;

	private byte[] registerData;

	public ZKWatchRegister(String registerNode, byte[] registerData) {
		this.registerNode = registerNode;
		this.registerData = registerData;
	}

	public void process(WatchedEvent event) {
		// TODO Auto-generated method stub
		
	}

}
