/**
 * Project: dx.zk.client
 * 
 * File Created at 2016年4月12日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.zkClient.acl;

/** 
* @ClassName: AclAuth 
* @Description: 不需要Id，通过auth的用户都能够访问
* ACL只用于一个节点，注意不能用于该节点的子节点，即每个节点的访问权限由其自身的ACL决定
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月12日 下午4:28:08 
* @version V1.0 
*/
public class AclAuth {

}
