/**
 * Project: dx.zk.client
 * 
 * File Created at 2016年4月12日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.zkClient.acl;

import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;

/** 
* @ClassName: AclIp 
* @Description: 使用客户端的Ip作为Acl的
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月12日 下午4:33:28 
* @version V1.0 
*/
public class AclIp {

	private final static String IP = "ip";
	
	private AclIp(){}
	
	/**
	 * 
	 * @param permission Perms.ALL |Perms.CREATE | Perms.READ | Perms.WRITE |Perms.DELETE
	 *        CREATE(c): 创建权限，可以在在当前node下创建child node
	 *        DELETE(d): 删除权限，可以删除当前的node 
	 *        READ(r): 读权限，可以获取当前node的数据，可以list当前node所有的child nodes
	 *        WRITE(w): 写权限，可以向当前node写数据
	 *        ADMIN(a): 管理权限，可以设置当前node的permission
	 * @param ipAddress IP地址 "10.xxx.xxx.23/27"
	 * @return
	 */
	public static ACL newInstance(int permission,String ipAddress){
		return new ACL(permission,new Id(IP, ipAddress));
	}
	
}
