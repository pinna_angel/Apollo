/**
 * Project: dx.zk.client
 * 
 * File Created at 2016年4月12日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.zkClient.cfg;

/** 
* @ClassName: ConfigConstants 
* @Description: 配置常量 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月12日 下午5:42:03 
* @version V1.0 
*/
public final class ConfigConstants {

	public static String ZK_CONFIG_FILE_NAME = "zk.properties";
	
	public final static String CONNECTSTRING = "connectString";
	
	public final static String SESSIONTIMEOUTMS = "sessionTimeoutMs";//Session超时时间限制
	
	public final static String CONNECTIONTIMEOUTMS = "connectionTimeoutMs";//连接超时时间限制
	
	public final static String CANBEREADONLY =  "canBeReadOnly";//只读，不能改数据
	
	public final static String BASESLEEPTIMEMS = "baseSleepTimeMs";//连接不上重试睡眠时间
	
	public final static String NAMESPACE = "namespace";//操作的根路径命名空间
}
