/**
 * Project: dx.zk.client
 * 
 * File Created at 2016年4月12日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.zkClient.cfg;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/** 
* @ClassName: ZkConfigManager 
* @Description: zookeeper客户端配置信息
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月12日 下午5:41:25 
* @version V1.0 
*/
public class ZkConfigManager {

	/**
	 * 配置文件
	 */
	private static Properties properties = null;

	public String configFilePath;// 配置文件路径

	public ZkConfigManager() {
	}

	public ZkConfigManager(String configFilePath) {
		this.configFilePath = configFilePath;
	}

	/**
	 * 从properties中获取配置属性
	 * @return
	 */
	public static Map<String,String> getProperties(){
		if(properties != null){
			Map<String,String> proMap = new HashMap<String,String>();
			if(properties.get(ConfigConstants.CONNECTSTRING) != null){
				proMap.put(ConfigConstants.CONNECTSTRING, String.valueOf(properties.get(ConfigConstants.CONNECTSTRING)));
			} else if(properties.get(ConfigConstants.SESSIONTIMEOUTMS) != null){
				proMap.put(ConfigConstants.SESSIONTIMEOUTMS, String.valueOf(properties.get(ConfigConstants.SESSIONTIMEOUTMS)));
			} else if(properties.get(ConfigConstants.CONNECTIONTIMEOUTMS) != null){
				proMap.put(ConfigConstants.CONNECTIONTIMEOUTMS, String.valueOf(properties.get(ConfigConstants.CONNECTIONTIMEOUTMS)));
			} else if(properties.get(ConfigConstants.CANBEREADONLY) != null){
				proMap.put(ConfigConstants.CANBEREADONLY, String.valueOf(properties.get(ConfigConstants.CANBEREADONLY)));
			} else if(properties.get(ConfigConstants.BASESLEEPTIMEMS) != null){
				proMap.put(ConfigConstants.BASESLEEPTIMEMS, String.valueOf(properties.get(ConfigConstants.BASESLEEPTIMEMS)));
			} else if(properties.get(ConfigConstants.NAMESPACE) != null){
				proMap.put(ConfigConstants.NAMESPACE, String.valueOf(properties.get(ConfigConstants.NAMESPACE)));
			}
			return proMap;
		}else{
			throw new NullPointerException("zk.properties配置文件为空，请检查配置文件");
		}
	}
	/**
	 * 初始化属性文件
	 * @param properties
	 * @throws Exception
	 */
	public static void init(Properties pros) throws Exception {
		properties = pros;
	}

	/**
	 * 设置配置文件路径，如果路径为空直接返回失败
	 * @param configFilePath
	 * @throws Exception
	 */
	public static void init(String configFilePath) throws Exception {
		FileInputStream fis = null;
		Reader fReader = null;
		try {
			fis = new FileInputStream(configFilePath + "/" + ConfigConstants.ZK_CONFIG_FILE_NAME);
			fReader = new InputStreamReader(fis, "UTF-8");
			properties = new Properties();
			properties.load(fReader);
			// log.info("系统初始化zk客户端。");
		} catch (Exception e) {
			throw new Exception("jedis初始化失败，请检查配置文件路径。", e);
		} finally {
			if (fReader != null) {
				fReader.close();
			}
			if (fis != null) {
				fis.close();
			}
		}
	}

}
