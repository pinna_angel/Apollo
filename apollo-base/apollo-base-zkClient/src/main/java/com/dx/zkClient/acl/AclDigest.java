/**
 * Project: dx.zk.client
 * 
 * File Created at 2016年4月12日
 * 
 * Copyright 2015-2015 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.zkClient.acl;

import java.security.NoSuchAlgorithmException;

import org.apache.zookeeper.ZooDefs.Perms;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.auth.DigestAuthenticationProvider;

/** 
* @ClassName: AclDigest 
* @Description: 通过用户名密码方式的auth验证，
* Id的格式为 username:base64  encoded  SHA1  password  digest 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年4月12日 下午4:32:16 
* @version V1.0 
*/
public class AclDigest {

	private final static String DIGEST = "digest";

	private AclDigest(){}
	
	/**
	 * 
	 * @param permission Perms.ALL |Perms.CREATE | Perms.READ | Perms.WRITE |Perms.DELETE
	 *        CREATE(c): 创建权限，可以在在当前node下创建child node
	 *        DELETE(d): 删除权限，可以删除当前的node 
	 *        READ(r): 读权限，可以获取当前node的数据，可以list当前node所有的child nodes
	 *        WRITE(w): 写权限，可以向当前node写数据
	 *        ADMIN(a): 管理权限，可以设置当前node的permission
	 * @param username
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static ACL newInstance(int permission,String username,String password) throws NoSuchAlgorithmException{
		return new ACL(permission, new Id(DIGEST, DigestAuthenticationProvider.generateDigest(username+":"+password)));
	}
}
