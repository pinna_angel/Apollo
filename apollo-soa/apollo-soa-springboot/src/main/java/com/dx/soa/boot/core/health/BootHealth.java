/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月14日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/** 
* @ClassName: BootHealth 
* @Description: 用户就能通过 health 查看 Spring Boot 应用的基本状态
* 看到服务的状态是 UP，不过也许这个检查太简单了，
* 例如我的服务依赖其他外部服务，其中一个 Tair，一个是 TFS，这两个都是强依赖，
* 如果它们有问题，我的服务就应该是 DOWN 的状态
* http://localhost:8080/health
* $ curl http://localhost:8080/health
* {
*     "status":"UP"
* }
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月14日 下午10:40:07 
* @version V1.0 
*/
@Component
public class BootHealth implements HealthIndicator {

	@Override
	public Health health() {
		int errorCode = check(); // perform some specific health check
		if (errorCode != 0) {
			//return Health.down().withDetail("Error Code", errorCode).build();
			return Health.up()
			.withDetail("details", "{ 'internals' : 'getting close to limit', 'profile' : 'yuan' }")
			.withDetail("tair", "timeout") // some logic check tair
			.withDetail("tfs", "ok") // some logic check tfs
			.status("itsok!")//"500"
			.build();
		}
		return Health.up().build();
	}

	private int check() {
		return 1;
	}
}
