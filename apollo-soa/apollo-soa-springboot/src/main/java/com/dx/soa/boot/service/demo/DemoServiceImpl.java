/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月13日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.service.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dx.soa.boot.dao.demo.IDemoDao;
import com.dx.soa.boot.entity.SysUser;

/** 
* @ClassName: DemoServiceImpl 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月13日 下午1:05:03 
* @version V1.0 
*/
@Service
public class DemoServiceImpl implements IDemoService {

	@Autowired
	private IDemoDao demoDao;

	@Override
	public int count() {
		try {
			return demoDao.getEntityCount(null, SysUser.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
