///**
// * Project: apollo-soa-springboot
// * 
// * File Created at 2016年5月15日
// * 
// * Copyright 2015-2016 dx.com Croporation Limited.
// * All rights reserved.
// *
// * This software is the confidential and proprietary information of
// * DongXue software Company. ("Confidential Information").  You shall not
// * disclose such Confidential Information and shall use it only in
// * accordance with the terms of the license agreement you entered into
// * with dx.com.
// */
//package com.dx.soa.boot.core.datasource.moniter;
//
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.annotation.WebInitParam;
//
//import com.alibaba.druid.support.http.WebStatFilter;
//
///** 
//* @ClassName: DruidStatFilter 
//* @Description: druid过滤器.
//* @author wuzhenfang(wzfbj2008@163.com)
//* @date 2016年5月15日 下午12:06:15 
//* @version V1.0 
//*/
//@WebFilter(filterName="druidWebStatFilter",urlPatterns="/*",
//initParams={
//        @WebInitParam(name="exclusions",value="*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*")//忽略资源
// }
//)
//public class DruidStatFilter extends WebStatFilter{
//
//}
