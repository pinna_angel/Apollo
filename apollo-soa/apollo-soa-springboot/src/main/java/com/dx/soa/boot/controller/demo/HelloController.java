/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月13日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.controller.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dx.soa.boot.service.demo.IDemoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/** 
* @ClassName: HelloController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月13日 上午10:37:08 
* @version V1.0 
*/
@Api(tags = "hello", produces = "application/json", consumes = "application/json")
@RestController
@RequestMapping("/hello")
public class HelloController {

	private final static Logger logger = Logger.getLogger(HelloController.class);
	
	@Autowired
	private IDemoService demoService;
	
	@ApiOperation(value = "hello", response = String.class, responseContainer = "string")
	@RequestMapping(method = RequestMethod.GET)
    public String hello() {
        return "Hello Spring-Boot";
    }

	@ApiOperation(value = "count", response = Integer.class, responseContainer = "int")
	@RequestMapping(value = "/count" ,method = RequestMethod.GET)
	public int count(){
		return demoService.count();
	}
	
	@ApiOperation(value="测试-getCount", notes="getCount更多说明")
    @RequestMapping(value="/info",method = RequestMethod.GET)
    public Map<String, String> getInfo(@RequestParam String name) {
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        return map;
    }

    @RequestMapping(value="/list",method = RequestMethod.GET)
    public List<Map<String, String>> getList() {
    	logger.info("execute --> getList()");
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = null;
        for (int i = 1; i <= 5; i++) {
            map = new HashMap<>();
            map.put("name", "Shanhy-" + i);
            list.add(map);
        }
        return list;
    }
}
