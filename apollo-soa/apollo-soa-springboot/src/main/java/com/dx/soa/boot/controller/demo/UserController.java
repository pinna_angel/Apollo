/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月14日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.controller.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import  org.springframework.http.HttpStatus;

import com.dx.soa.boot.entity.UserSummary;

import io.swagger.annotations.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/** 
* @ClassName: UserController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月14日 上午9:35:45 
* @version V1.0 
*/
@RestController
@Api(tags = "User", produces = "application/json", consumes = "application/json")
@RequestMapping(value = "users", produces = "application/json")
public class UserController {

//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private MapperFacade mapper;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "get user list", response = UserSummary.class, responseContainer = "List")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "authorization header", required = true,
                    dataType = "string", paramType = "header", defaultValue = "bearer "),
            @ApiImplicitParam(name = "page", value = "the page index", required = false,
                    dataType = "int", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "count", value = "the page count", required = false,
                    dataType = "int", paramType = "query", defaultValue = "30")
    })
    public List<UserSummary> getUsers() {
        //return mapper.mapAsList(userService.list(), UserSummary.class);
    	return null;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "create a new user", 
                  nickname = "createUser",
                  responseHeaders = {@ResponseHeader(name = "link", description = "the user link")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "authorization header", required = true,
                    dataType = "string", paramType = "header", defaultValue = "bearer ")
    })
    @ApiResponses({@ApiResponse(code = 400, message = "BadRequest")})
    public void addUser(@RequestBody UserSummary userSummary) {
    	
        return;
    }

}