/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年6月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core;

import org.springframework.ui.ModelMap;

/** 
* @ClassName: BaseController 
* @Description:Controller抽象类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年6月5日 上午9:05:29 
* @version V1.0 
*/
public abstract class BaseController {

	protected static final String SUCCESS_FLAG = "success";

	protected void writeSuccess(ModelMap modelMap, Object data) {
		modelMap.put(SUCCESS_FLAG, true);
		modelMap.put("data", data);
	}

	protected void writeError(ModelMap modelMap, String message) {
		modelMap.put(SUCCESS_FLAG, false);
		modelMap.put("msg", message);
	}

	protected <T> void writeData(ModelMap modelMap, ServiceResult<T> result) {
		if (result.isSuccess())
			writeSuccess(modelMap, result.getData());
		else
			writeError(modelMap, result.getMessage());
	}
}
