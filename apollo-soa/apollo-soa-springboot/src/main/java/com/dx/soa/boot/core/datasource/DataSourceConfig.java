/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月13日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.datasource;


import org.apache.log4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.alibaba.druid.pool.DruidDataSource;

/** 
* @ClassName: DataSourceConfig 
* @Description: 自定义数据源
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月13日 下午1:53:11 
* @version V1.0 
*/
@Configuration
//@EnableConfigurationProperties(DruidDataSource.class)
public class DataSourceConfig {

	 private static final Logger logger = Logger.getLogger(DataSourceConfig.class);
	 
	@Primary //默认数据源 
	@Bean(initMethod="init",destroyMethod="close")
    @ConfigurationProperties(prefix = "spring.druid")
	public DruidDataSource mysqlDataSource() {
		return new DruidDataSource();
    } 
}
