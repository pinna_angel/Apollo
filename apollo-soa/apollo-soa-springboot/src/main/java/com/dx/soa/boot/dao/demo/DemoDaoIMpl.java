/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月13日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.dao.demo;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.dx.soa.boot.core.dao.BootDaoImpl;
import com.dx.soa.boot.entity.SysUser;

/** 
* @ClassName: DemoDaoIMpl 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月13日 下午1:06:08 
* @version V1.0 
*/
@Service
public class DemoDaoIMpl extends BootDaoImpl<SysUser> implements IDemoDao{

	private static final Logger log = Logger.getLogger(DemoDaoIMpl.class);
	
	
}
