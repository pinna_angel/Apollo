/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月15日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.health;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

/** 
* @ClassName: ShowEndpoints 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月15日 上午11:45:15 
* @version V1.0 
*/
@Component
public class ShowEndpoints extends AbstractEndpoint<List<Endpoint>> {

	private List<Endpoint> endpoints;

	@Autowired
	public ShowEndpoints(List<Endpoint> endpoints) {
		super("showEndpoints");
		this.endpoints = endpoints;
	}

	@Override
	public List<Endpoint> invoke() {
		return this.endpoints;
	}

}
