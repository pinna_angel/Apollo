/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月15日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.health;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

/** 
* @ClassName: ServerEndpoint 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月15日 上午11:43:59 
* @version V1.0 
*/
@Component
public class ServerEndpoint implements Endpoint<List<String>> {

	@Override
	public String getId() {
		return "server";
	}

	@Override
	public List<String> invoke() {
		List<String> serverDetails = new ArrayList<String>();
	    try {
	      serverDetails.add("Server IP Address : " + InetAddress.getLocalHost().getHostAddress());
	      serverDetails.add("Server OS : " + System.getProperty("os.name").toLowerCase());
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    return serverDetails;
	}

	@Override
	public boolean isEnabled() {
		 return true;
	}

	@Override
	public boolean isSensitive() {
		return false;
	}

}
