/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年6月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.api;

/**
 * @ClassName: ApiCodeEnum
 * @Description:返回码枚举
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年6月5日 上午9:15:14
 * @version V1.0
 */
public enum ApiCodeEnum {
	/** 成功 */
	SUCCESS(0, "成功"),

	/** 失败 */
	FAIL(1, "失败"),
	/** 业务异常 */
	BIZERROR(2, "业务异常");
	
	private ApiCodeEnum(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	private int code;
	private String desc;

	/**
	 * Getter method for property <tt>code</tt>.
	 *
	 * @return property value of code
	 */
	public final int getCode() {
		return code;
	}

	/**
	 * Setter method for property <tt>code</tt>.
	 *
	 * @param code
	 *            value to be assigned to property code
	 */
	public final void setCode(int code) {
		this.code = code;
	}

	/**
	 * Getter method for property <tt>desc</tt>.
	 *
	 * @return property value of desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Setter method for property <tt>desc</tt>.
	 *
	 * @param desc
	 *            value to be assigned to property desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static ApiCodeEnum getEnumByCode(int code) {
		for (ApiCodeEnum statu : values()) {
			if (code == statu.code) {
				return statu;
			}
		}
		return null;
	}
}
