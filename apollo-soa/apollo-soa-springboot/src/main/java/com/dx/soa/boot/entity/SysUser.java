package com.dx.soa.boot.entity;
import java.io.Serializable;
import java.util.Date;
import com.bioeh.sp.hm.dal.anno.Column;
import com.bioeh.sp.hm.dal.anno.Id;
import com.bioeh.sp.hm.dal.anno.Skip;
import com.bioeh.sp.hm.dal.anno.Table;

import io.swagger.annotations.ApiModel;
/**
 * @ClassName:SysUser
 * @Description:系统用户 
 * @author
 * @date 2015-08-10 16:30:55
 * @version V1.0
 */
@ApiModel(description = "user information")
@Table(value="sys_user")
public class SysUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 *编号
	 */	 
	@Id
	@Column("pk_id")
	private long pkId;
	
	/**
	 *登录名
	 */	 
	@Column("login_name")
	private String loginName;
	
	/**
	 *密码
	 */	 
	@Column("password")
	private String password;
	
	/**
	 *姓名
	 */	 
	@Column("name")
	private String name;
	
	/**
	 *邮箱
	 */	 
	@Column("email")
	private String email;
	
	/**
	 *生日
	 */	 
	@Column("birthday")
	private Date birthday;
	
	@Skip
	private int age;
	/**
	 *身份证
	 */	 
	@Column("idcard")
	private String idcard;
	
	/**
	 *街道
	 */	 
	@Column("street")
	private String street;
	/**
	 * 地区
	 */
	@Column("District")
	private String district;
	
	@Column("city")
	private String city;
	
	@Column("province")
	private String province;
	
	/**
	 *学历
	 */	 
	@Column("degrees")
	private String degrees;
	
	/**
	 *血型
	 */	 
	@Column("blood")
	private String blood;
	
	@Column("imageurl")
	private String imageurl;
	
	
	//job	int	1	0	-1	0	0	0	0		0					0	0
	//defects_type	int	1	0	-1	0	0	0	0		0					0	0
	//defects_code	varchar	30	0	-1	0	0	0	0		0		utf8	utf8_general_ci		0	0

	
	/**
	 * 工人
离退休者
专业技术人员
行政管理员
办事人员
军人
企业家
商业服务页员工
学生
其他
	 */
	@Column("job")
	public Integer job; //工作
	
	/**
	 * 听力残、言语残、肢体残、智力残、眼残、精神残

残疾证号
	 */
	@Column("defects_type")
	public Integer  defectsType; //残疾类型
	
	
	
	
	@Column("defects_code")
	public String  defectsCode; //残疾号
	
	
	/**
	 *是否结婚
	 */	 
	@Column("marriage")
	private String marriage;
	
	/**
	 *固话
	 */	 
	@Column("phone")
	private long phone;
	
	/**
	 * 数据来源
	 */
	@Column("datasource")
	private String datasource;
	
	/**
	 *移动电话
	 */	 
	@Column("mobile")
	private String mobile;
	
	/**
	 * 0管理员或者用户,1分本人会员
	 */
	@Column("usertype")
	private Integer usertype;
	
	/**
	 *最后登陆IP
	 */	 
	@Column("login_ip")
	private String loginIp;
	
	/**
	 *最后登陆时间
	 */	 
	@Column("login_date")
	private Date loginDate;
	
	/**
	 *创建者
	 */	 
	@Column("create_by")
	private long createBy;
	
	/**
	 *创建时间
	 */	 
	@Column("create_date")
	private Date createDate;
	
	/**
	 *更新者
	 */	 
	@Column("update_by")
	private long updateBy;
	
	/**
	 *更新时间
	 */	 
	@Column("update_date")
	private Date updateDate;
	
	@Column("departmentid")
	private long departmentid;
	
	@Column("regionid")
	private long regionid;
	
	/**
	 *机构id
	 */	 
	@Column("organinationid")
	private long organinationid;
	
	/**
	 * 体力值
	 */
	@Column("pal")
	private double pal;

	
	@Skip
	private String birthdaystring;
	
	@Skip
	private String sysemployeeaptitudeid;//当前用户绑定的Id
	
	
	@Skip
	private int status;

	

	
	@Skip
	private String provincestr;
	/**
	 * 机构名称
	 */
	@Skip
	private String organization;
	/**
	 * 部门数组，从上到下，一次部门递减
	 */
	@Skip
	private String[]  department;
	/**
	 * 角色数组，从上到下依次递减
	 */
	@Skip
	private String[] role;
	@Skip
	private int isadmin;//1是管理员，0是用户 
	
	
	/**
	 *备注信息
	 */	 
	@Column("remarks")
	private String remarks;
	
	/**
	 *删除标记-1删除1正常
	 */	 
	@Column("del_flag")
	private Integer delFlag;
	
	@Column("roleid")
	private long roleid;
	
	@Column("sex")
	private Integer sex;
	
	@Skip
	private String organinationidstr;
	
	@Skip
	private String departmentidstr;
	
	@Skip
	private String roleidstr;
	
	/**
	 *学士学位
	 */	 
	@Column("educational")
	private Integer educational;
	
	
	/**
	 * 邮寄地址
	 */
	@Column("expressaAdress")
 	private String 	expressaAdress;
	
	/**
	 * 籍贯
	 */
	@Column("birth")
	private String birth; 
	/**
	 * 腰围
	 */
	@Column("waistline")
	private String waistline; 
	/**
	 * 体重	
	 */
	@Column("weight")
	private String weight; 
	/**
	 * 身高
	 */
	@Column("height")
	private String height;
	/**
	 * 名族
	 */
	@Column("nation")
	private String nation;
	
	
	@Column("cardtype")
	private String cardtype;
	
	@Skip
	private String isuploademployeeaptitude; // //是否拥有从业证的标识
	
	@Skip
	private String sysRoleName; //系统角色名称
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * 账户状态
	 */
	@Column("is_active")
	private Integer isActive;
	
	/**
	 * 个人信息完整度
	 */
	@Column("detail_percent")
	private Integer detailPercent;
	
	@Skip
	private String tagIds;
	
	public SysUser(){}
	
	
	public String getTagIds() {
		return tagIds;
	}

	public void setTagIds(String tagIds) {
		this.tagIds = tagIds;
	}

	public long getPkId() {
		return pkId;
	}

	public void setPkId(long pkId) {
		this.pkId = pkId;
	}
	
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getDegrees() {
		return degrees;
	}

	public void setDegrees(String degrees) {
		this.degrees = degrees;
	}
	
	public String getBlood() {
		return blood;
	}

	public void setBlood(String blood) {
		this.blood = blood;
	}
	
	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	
	public String getMarriage() {
		return marriage;
	}

	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}
	
	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}
	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public Integer getUsertype() {
		return usertype;
	}

	public void setUsertype(Integer usertype) {
		this.usertype = usertype;
	}
	
	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	
	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}
	
	public long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(long createBy) {
		this.createBy = createBy;
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(long updateBy) {
		this.updateBy = updateBy;
	}
	
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public long getDepartmentid() {
		return departmentid;
	}

	public void setDepartmentid(long departmentid) {
		this.departmentid = departmentid;
	}
	
	public long getRegionid() {
		return regionid;
	}

	public void setRegionid(long regionid) {
		this.regionid = regionid;
	}
	
	public long getOrganinationid() {
		return organinationid;
	}

	public void setOrganinationid(long organinationid) {
		this.organinationid = organinationid;
	}
	
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}
	
	public long getRoleid() {
		return roleid;
	}

	public void setRoleid(long roleid) {
		this.roleid = roleid;
	}
	
	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}
	
	public Integer getEducational() {
		return educational;
	}

	public void setEducational(Integer educational) {
		this.educational = educational;
	}
		public String getBirthdaystring() {
		return birthdaystring;
	}

	public void setBirthdaystring(String birthdaystring) {
		this.birthdaystring = birthdaystring;
	}

	public String getOrganinationidstr() {
		return organinationidstr;
	}

	public void setOrganinationidstr(String organinationidstr) {
		this.organinationidstr = organinationidstr;
	}

	public String getDepartmentidstr() {
		return departmentidstr;
	}

	public void setDepartmentidstr(String departmentidstr) {
		this.departmentidstr = departmentidstr;
	}

	public String getProvincestr() {
		return provincestr;
	}

	public void setProvincestr(String provincestr) {
		this.provincestr = provincestr;
	}

	public String getRoleidstr() {
		return roleidstr;
	}

	public void setRoleidstr(String roleidstr) {
		this.roleidstr = roleidstr;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String[] getDepartment() {
		return department;
	}

	public void setDepartment(String[] department) {
		this.department = department;
	}

	public String[] getRole() {
		return role;
	}

	public void setRole(String[] role) {
		this.role = role;
	}

	public String getDatasource() {
		return datasource;
	}

	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}

	public int getIsadmin() {
		return isadmin;
	}

	public void setIsadmin(int isadmin) {
		this.isadmin = isadmin;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public String getIsuploademployeeaptitude() {
		return isuploademployeeaptitude;
	}

	public void setIsuploademployeeaptitude(String isuploademployeeaptitude) {
		this.isuploademployeeaptitude = isuploademployeeaptitude;
	}

	public String getSysRoleName() {
		return sysRoleName;
	}

	public void setSysRoleName(String sysRoleName) {
		this.sysRoleName = sysRoleName;
	}

	public String getSysemployeeaptitudeid() {
		return sysemployeeaptitudeid;
	}

	public void setSysemployeeaptitudeid(String sysemployeeaptitudeid) {
		this.sysemployeeaptitudeid = sysemployeeaptitudeid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getExpressaAdress() {
		return expressaAdress;
	}

	public void setExpressaAdress(String expressaAdress) {
		this.expressaAdress = expressaAdress;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getWaistline() {
		return waistline;
	}

	public void setWaistline(String waistline) {
		this.waistline = waistline;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getCardtype() {
		return cardtype;
	}

	public void setCardtype(String cardtype) {
		this.cardtype = cardtype;
	}

	public Integer getDetailPercent() {
		return detailPercent;
	}

	public void setDetailPercent(Integer detailPercent) {
		this.detailPercent = detailPercent;
	}

	public double getPal() {
		return pal;
	}

	public void setPal(double pal) {
		this.pal = pal;
	}

	public Integer getJob() {
		return job;
	}

	public void setJob(Integer job) {
		this.job = job;
	}

	public Integer getDefectsType() {
		return defectsType;
	}

	public void setDefectsType(Integer defectsType) {
		this.defectsType = defectsType;
	}

	public String getDefectsCode() {
		return defectsCode;
	}

	public void setDefectsCode(String defectsCode) {
		this.defectsCode = defectsCode;
	}
	
}