/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月14日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.web.schedule;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/** 
* @ClassName: SchedulingConfig 
* @Description: 定时任务配置类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月14日 下午3:25:33 
* @version V1.0 
*/
@Configuration
@EnableScheduling // 启用定时任务
public class SchedulingConfig {

	private final Logger logger = Logger.getLogger(SchedulingConfig.class);
	
	@Scheduled(cron = "0/20 * * * * ?") // 每20秒执行一次
    public void scheduler() {
        logger.info(">>>>>>>>>>>>> scheduled ... ");
    }
}
