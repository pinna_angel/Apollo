/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月15日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.health;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Component;

/** 
* @ClassName: BootAppMetric 
* @Description: metrics 我们能得到很多信息
* 包括 JVM 的线程数、内存、GC 数据等等……，这些都是系统级别的数据，但其实我们可以通过 metrics 
* 收集实时的业务数据，例如每分钟用户登陆数量、每分钟文件同步数量、实时的缓存命中率……等等
* http://localhost:8080/metrics
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月15日 上午11:17:54 
* @version V1.0 
*/
@Component
public class BootAppMetric {

	private final CounterService counterService;
	
	private final GaugeService gaugeService;

	@Autowired
	public BootAppMetric(CounterService counterService, GaugeService gaugeService) {
		this.counterService = counterService;
		this.gaugeService = gaugeService;
	}

	public void exampleCounterMethod() {
		this.counterService.increment("login.count");
		// reset each minute
	}

	public void exampleGaugeMethod() {
		this.gaugeService.submit("cache.hit", 80.0);
	}
}
