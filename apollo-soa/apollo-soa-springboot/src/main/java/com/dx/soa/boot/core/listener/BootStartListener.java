/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月14日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.listener;

import org.apache.log4j.Logger;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;

/** 
* @ClassName: BootStartListener 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月14日 下午10:30:14 
* @version V1.0 
*/
public class BootStartListener implements ApplicationListener<ApplicationStartedEvent> {
	
	private static final Logger logger = Logger.getLogger(BootStartListener.class);
	
	@Override
	public void onApplicationEvent(ApplicationStartedEvent event) {
		// TODO Auto-generated method stub
		
	}

}
