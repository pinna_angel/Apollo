/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月7日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: BootApplication
 * @Description:Boot启动
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年5月7日 下午12:01:53
 * @version V1.0
 */

@SpringBootApplication
@RestController
// @Import({DynamicDataSourceRegister.class}) // 注册动态多数据源
// @ImportResource("classpath:spring/application-config.xml")
@ComponentScan("com.dx")
@ServletComponentScan
@EntityScan("com.dx.entity")
//@EnableAdminServer//注册监控
public class BootApplication extends SpringBootServletInitializer {

	private static final Class<BootApplication> bootApplicationClass = BootApplication.class;

	public static void main(String[] args) {
		SpringApplication.run(bootApplicationClass, args);// 不依赖其他容器启动
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(bootApplicationClass);
	}

	@RequestMapping("/")
	public String home() {
		return "hello ,spring boot.";
	}
}
