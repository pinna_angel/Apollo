///**
// * Project: apollo-soa-springboot
// * 
// * File Created at 2016年5月13日
// * 
// * Copyright 2015-2016 dx.com Croporation Limited.
// * All rights reserved.
// *
// * This software is the confidential and proprietary information of
// * DongXue software Company. ("Confidential Information").  You shall not
// * disclose such Confidential Information and shall use it only in
// * accordance with the terms of the license agreement you entered into
// * with dx.com.
// */
//package com.dx.soa.boot.core;
//
//import org.apache.log4j.Logger;
//import org.springframework.context.EnvironmentAware;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//import org.springframework.web.servlet.DispatcherServlet;
//import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//import org.springframework.web.servlet.view.InternalResourceViewResolver;
//
///** 
//* @ClassName: WebConfig 
//* @Description: TODO(这里用一句话描述这个类的作用) 
//* @author wuzhenfang(wzfbj2008@163.com)
//* @date 2016年5月13日 上午11:49:42 
//* @version V1.0 
//*/
//@EnableWebMvc
//@ComponentScan
//@Configuration
//public class WebConfig extends WebMvcConfigurerAdapter implements EnvironmentAware{
//
//	private static final Logger logger = Logger.getLogger(WebConfig.class);
//	@Override
//	public void addViewControllers(ViewControllerRegistry registry) {
//		registry.addViewController("/").setViewName("home");
//	}
//
//	@Bean
//	public InternalResourceViewResolver viewResolver() {
//		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//		viewResolver.setPrefix("/WEB-INF/views/");
//		viewResolver.setSuffix(".jsp");
//		return viewResolver;
//	}
//
//	//Only used when running in embedded servlet
//	@Bean
//	public DispatcherServlet dispatcherServlet() {
//		return new DispatcherServlet();
//	}
//
//	@Override
//	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//		configurer.enable();
//	}
//
//	@Override
//	public void setEnvironment(Environment environment) {
//		for (String ps : environment.getDefaultProfiles()) {
//			logger.info(ps);;
//		}
//	}
//}
