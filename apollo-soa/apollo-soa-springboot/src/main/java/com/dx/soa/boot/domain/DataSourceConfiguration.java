/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月14日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.domain;

import org.springframework.beans.factory.annotation.Value;

/** 
* @ClassName: DataSourceConfiguration 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月14日 下午3:31:19 
* @version V1.0 
*/
//@Configuration
public class DataSourceConfiguration {

	@Value("${custom.datasource.driver}")  
    private String driverClassName;  
  
    @Value("${custom.datasource.url}")  
    private String driverUrl;  
  
    @Value("${custom.datasource.user}")  
    private String driverUsername;  
  
    @Value("${custom.datasource.password}")  
    private String driverPassword;  
  
//    @Bean(name = "dataSource")  
//    public DataSource dataSource() {  
//        BasicDataSource dataSource = new BasicDataSource();  
//        dataSource.setDriverClassName(driverClassName);  
//        dataSource.setUrl(driverUrl);  
//        dataSource.setUsername(driverUsername);  
//        dataSource.setPassword(driverPassword);  
//        return dataSource;  
//    }  
  
//    @Bean  
//    public PlatformTransactionManager transactionManager() {  
//        return new DataSourceTransactionManager(dataSource());  
//    }  
}
