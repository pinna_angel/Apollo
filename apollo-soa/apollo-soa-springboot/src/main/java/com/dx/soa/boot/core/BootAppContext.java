/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月15日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/** 
* @ClassName: BootAppContext 
* @Description: bean装配，该类是一个拥有 bean 定义和依赖项的配置类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月15日 上午10:45:34 
* @version V1.0 
*/
@Configuration
public class BootAppContext {
	
	private static final Logger logger = Logger.getLogger(BootAppContext.class);

	@Bean(name = "workExecutor")
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setMaxPoolSize(10);
		taskExecutor.setQueueCapacity(10);
		taskExecutor.afterPropertiesSet();
		return taskExecutor;
	}

	@Bean(name = "appBean")
	public BootAppBeanUtil appBean() {
		logger.info("BootAppContext load 【appBean】 init ");
		return new BootAppBeanUtil();
	}
}
