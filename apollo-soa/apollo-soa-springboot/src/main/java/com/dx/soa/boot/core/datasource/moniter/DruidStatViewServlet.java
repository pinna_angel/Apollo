///**
// * Project: apollo-soa-springboot
// * 
// * File Created at 2016年5月15日
// * 
// * Copyright 2015-2016 dx.com Croporation Limited.
// * All rights reserved.
// *
// * This software is the confidential and proprietary information of
// * DongXue software Company. ("Confidential Information").  You shall not
// * disclose such Confidential Information and shall use it only in
// * accordance with the terms of the license agreement you entered into
// * with dx.com.
// */
//package com.dx.soa.boot.core.datasource.moniter;
//
//import javax.servlet.annotation.WebInitParam;
//import javax.servlet.annotation.WebServlet;
//
//import com.alibaba.druid.support.http.StatViewServlet;
//
///** 
//* @ClassName: DruidStatViewServlet 
//* @Description: druid数据源状态监控.
//* @author wuzhenfang(wzfbj2008@163.com)
//* @date 2016年5月15日 下午12:04:54 
//* @version V1.0 
//*/
//@WebServlet(urlPatterns="/druid/*",
//initParams={
//        @WebInitParam(name="allow",value="192.168.1.72,127.0.0.1"),// IP白名单(没有配置或者为空，则允许所有访问)
//         @WebInitParam(name="deny",value="192.168.1.73"),// IP黑名单 (存在共同时，deny优先于allow)
//         @WebInitParam(name="loginUsername",value="admin"),// 用户名
//         @WebInitParam(name="loginPassword",value="123456"),// 密码
//         @WebInitParam(name="resetEnable",value="false")// 禁用HTML页面上的“Reset All”功能
//}
//)
//public class DruidStatViewServlet extends StatViewServlet{
//	
//	private static final long serialVersionUID = -6603412529396121490L;
//
//}
