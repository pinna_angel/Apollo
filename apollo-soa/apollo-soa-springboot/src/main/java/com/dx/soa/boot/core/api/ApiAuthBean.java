/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年6月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.api;

/** 
* @ClassName: ApiAuthBean 
* @Description: API鉴权数据实体
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年6月5日 上午9:22:09 
* @version V1.0 
*/
public class ApiAuthBean {
	/** */
	private static final long serialVersionUID = -7176406155692157064L;
	/** 服务名称 */
	private String srvname;
	/** 执行事件 */
	private String actName;
	/** 版本号 */
	private String apiVersion;

	/**
	 * Getter method for property <tt>srvname</tt>.
	 *
	 * @return property value of srvname
	 */
	public final String getSrvname() {
		return srvname;
	}

	/**
	 * Setter method for property <tt>srvname</tt>.
	 *
	 * @param srvname
	 *            value to be assigned to property srvname
	 */
	public final void setSrvname(String srvname) {
		this.srvname = srvname;
	}

	/**
	 * Getter method for property <tt>actName</tt>.
	 *
	 * @return property value of actName
	 */
	public final String getActName() {
		return actName;
	}

	/**
	 * Setter method for property <tt>actName</tt>.
	 *
	 * @param actName
	 *            value to be assigned to property actName
	 */
	public final void setActName(String actName) {
		this.actName = actName;
	}

	/**
	 * Getter method for property <tt>apiVersion</tt>.
	 *
	 * @return property value of apiVersion
	 */
	public final String getApiVersion() {
		return apiVersion;
	}

	/**
	 * Setter method for property <tt>apiVersion</tt>.
	 *
	 * @param apiVersion
	 *            value to be assigned to property apiVersion
	 */
	public final void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
}
