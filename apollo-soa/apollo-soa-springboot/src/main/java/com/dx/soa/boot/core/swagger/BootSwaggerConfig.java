/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月14日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.swagger;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.StopWatch;

import io.swagger.annotations.Api;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.AuthorizationScopeBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import static com.google.common.collect.Lists.newArrayList;
import com.google.common.collect.Sets;

/** 
* @ClassName: BootSwaggerConfig 
* @Description: 系统swagger的接口展示配置
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月14日 上午9:39:23 
* @version V1.0 
*/
@Configuration
@EnableSwagger2
public class BootSwaggerConfig {

	private static final Logger logger = Logger.getLogger(BootSwaggerConfig.class);
	
	@Autowired(required = false)
	private ApiInfo apiInfo;

	@Bean
	public Docket petApi() {
		logger.info("init docket...");
		StopWatch watch = new StopWatch();
		watch.start();
		
		AuthorizationScope[] authScopes = new AuthorizationScope[1];
        authScopes[0] = new AuthorizationScopeBuilder()
                .scope("read")
                .description("read access")
                .build();
        
		SecurityReference securityReference = SecurityReference.builder()
                .reference("test")
                .scopes(authScopes)
                .build();

        ArrayList<SecurityContext> securityContexts = newArrayList(SecurityContext.builder()
        		.securityReferences(newArrayList(securityReference))
        		.build());
		
		Docket docket = new Docket(DocumentationType.SWAGGER_2)//文档类型为Swagger 2
				.produces(Sets.newHashSet("application/json"))
                .consumes(Sets.newHashSet("application/json"))
                .protocols(Sets.newHashSet("http", "https"))
                .forCodeGeneration(true)
                .securitySchemes(newArrayList(new BasicAuth("test")))
                .securityContexts(securityContexts)
                .groupName("full-api")
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))//api过滤，这边表示收集所有带注解Api的API，如果不过滤的话Spring Boot内置的healthcheck, actuator将混乱文档，更多查询RequestHandlerSelectors
				.paths(PathSelectors.any())//通过path过滤api，更多查询PathSelectors
				.build()
				.pathMapping("/")
				.directModelSubstitute(LocalDate.class, Date.class)//Java8时间类型替换方案
				.useDefaultResponseMessages(false);
		
		watch.stop();
		if (!Objects.isNull(apiInfo)) {
			docket.apiInfo(apiInfo);
		}
		return docket;
	}

	@Bean
	public ApiInfo apiInfo() {
		Contact contact = new Contact("wuzhenfang", "http://www.yuan.com", "wzfbj2008@163.com");
		return new ApiInfoBuilder()
				.title("愿App接口说明书:")
				.description("愿Api说明文档详细描述。")
				.termsOfServiceUrl("http://springfox.io")
				.contact(contact)
				.license("The Apache License, Version 2.0")
				.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
				.version("1.0")
				.build();
	}
}
