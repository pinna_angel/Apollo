/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月13日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.datasource;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/** 
* @ClassName: DynamicDataSourceContextHolder 
* @Description: 动态数据源上下文 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月13日 下午2:42:13 
* @version V1.0 
*/
public class DynamicDataSourceContextHolder {

	private static final Logger logger = Logger.getLogger(DynamicDataSourceContextHolder.class);

	private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

	public static List<String> dataSourceIds = new ArrayList<>();

	public static void setDataSourceType(String dataSourceType) {
		contextHolder.set(dataSourceType);
	}

	public static String getDataSourceType() {
		return contextHolder.get();
	}

	public static void clearDataSourceType() {
		logger.debug("移除当前数据源。" + getDataSourceType());
		contextHolder.remove();
	}

	/**
	 * 判断指定DataSrouce当前是否存在
	 * 
	 * @param dataSourceId
	 * @return
	 */
	public static boolean containsDataSource(String dataSourceId) {
		return dataSourceIds.contains(dataSourceId);
	}
}
