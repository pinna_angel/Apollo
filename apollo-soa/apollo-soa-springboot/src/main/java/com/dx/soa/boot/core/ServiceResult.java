/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年6月5日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core;

import java.io.Serializable;

/**
 * @ClassName: ServiceResult
 * @Description: 调用接口的响应结果存放类
 * @author wuzhenfang(wzfbj2008@163.com)
 * @date 2016年6月5日 上午9:12:43
 * @version V1.0
 */
public class ServiceResult<T> implements Serializable {

	private static final long serialVersionUID = -7815723967101753647L;
	private ResultCodeEnum resultCode;
	private String message;
	private T data;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return resultCode != null && resultCode == ResultCodeEnum.SUCCESS;
	}

	public void setSuccess(boolean success) {
		this.resultCode = success ? ResultCodeEnum.SUCCESS : ResultCodeEnum.FAIL;
	}

	/**
	 * Getter method for property <tt>resultCode</tt>.
	 *
	 * @return property value of resultCode
	 */
	public final ResultCodeEnum getResultCode() {
		return resultCode;
	}

	/**
	 * Setter method for property <tt>resultCode</tt>.
	 *
	 * @param resultCode
	 *            value to be assigned to property resultCode
	 */
	public final void setResultCode(ResultCodeEnum resultCode) {
		this.resultCode = resultCode;
	}

}
