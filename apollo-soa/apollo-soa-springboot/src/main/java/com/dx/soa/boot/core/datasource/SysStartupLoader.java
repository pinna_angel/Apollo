/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月13日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.datasource;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/** 
* @ClassName: SysStartupLoad 
* @Description: 系统启动加载
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月13日 下午4:43:09 
* @version V1.0 
*/
@Component
@Order(value=2)//注解的执行优先级是按value值从小到大顺序
public class SysStartupLoader implements CommandLineRunner{

	private static final Logger logger = Logger.getLogger(SysStartupLoader.class);
	
	@Override
	public void run(String... args) throws Exception {
		 System.out.println(">>>>>>>>>>>>>>>服务启动执行，执行加载数据等操作<<<<<<<<<<<<<");
	}

}
