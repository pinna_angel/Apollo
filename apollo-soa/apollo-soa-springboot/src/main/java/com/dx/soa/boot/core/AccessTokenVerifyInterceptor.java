/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月15日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/** 
* @ClassName: AccessTokenVerifyInterceptor 
* @Description: Access Token拦截器 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月15日 下午12:39:03 
* @version V1.0 
*/
@Component
public class AccessTokenVerifyInterceptor extends HandlerInterceptorAdapter {

}
