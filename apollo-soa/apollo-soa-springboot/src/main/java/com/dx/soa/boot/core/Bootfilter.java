/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月13日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core;

import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.CharacterEncodingFilter;

/** 
* @ClassName: Bootfilter 
* @Description: 添加filter
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月13日 下午8:15:11 
* @version V1.0 
*/
public class Bootfilter {

	@Bean
	public CharacterEncodingFilter characterEncodingFilter() {
	    CharacterEncodingFilter filter = new CharacterEncodingFilter();
	    filter.setEncoding("UTF-8");
	    filter.setForceEncoding(true);
	    return filter;
	}
	
//	@Bean
//	@ConditionalOnBean(name = AbstractSecurityWebApplicationInitializer.DEFAULT_FILTER_NAME)
//	public FilterRegistrationBean securityFilterChainRegistration() {
//		DelegatingFilterProxy delegatingFilterProxy = new DelegatingFilterProxy();
//		delegatingFilterProxy.setTargetBeanName(AbstractSecurityWebApplicationInitializer.DEFAULT_FILTER_NAME);
//		FilterRegistrationBean registrationBean = new FilterRegistrationBean(delegatingFilterProxy);
//		registrationBean.addUrlPatterns("/*");
//		registrationBean.setName("springSecurityFilterChain");
//		return registrationBean;
//	}
	
	/**
	 * @Bean
    public FilterRegistrationBean someFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(someFilter());
        registration.addUrlPatterns("/url/*");
        registration.addInitParameter("paramName", "paramValue");
        registration.setName("someFilter");
        return registration;
    }

    @Bean(name = "someFilter")
    public Filter someFilter() {
        return new SomeFilter();
    }

	 */
}
