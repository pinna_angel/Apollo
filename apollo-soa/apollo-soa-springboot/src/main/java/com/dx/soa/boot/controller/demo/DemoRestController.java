/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月7日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.controller.demo;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dx.soa.boot.service.demo.IDemoService;

/** 
* @ClassName: DemoRestController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月7日 下午12:11:34 
* @version V1.0 
*/
@RestController
@RequestMapping("/demo")
public class DemoRestController {

	private static final Logger log = Logger.getLogger(DemoRestController.class);
	
	@Autowired
	private IDemoService demoService;
	
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/createUserInGroup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String createUserInGroup(@RequestBody Map<String, String> data) {

		log.info("Inside createUserInGroup :: Input Data == " + data.values().toString());
		String userId = data.get("userId");
		String password = data.get("password");
		String firstName = data.get("firstName");
		String lastName = data.get("lastName");
		String email = data.get("email");
		String groupId = data.get("groupId");

//		User newUser = identityService.newUser(userId);
//		newUser.setPassword(password);
//		newUser.setFirstName(firstName);
//		newUser.setLastName(lastName);
//		newUser.setEmail(email);
//
//		identityService.saveUser(newUser);
//
//		identityService.createMembership(newUser.getId(), groupId);

		String jsonString = ("{ \"status\" : \"success\" }");

		log.info("Completing createUserInGroup :: returns == " + jsonString);

		return jsonString;
	}
	
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/authenticateUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String authenticateUser(@RequestBody Map<String, String> data) {

		log.info("Inside authenticateUser :: Input Data == " + data.values().toString());

		String userId = data.get("userId");
		String password = data.get("password");

//		boolean isAuthenticated = identityService.checkPassword(userId, password);
//
		String userGroupId = null;
//
//		if (isAuthenticated) {
//			userGroupId = identityService.createGroupQuery().groupMember(userId).singleResult().getId();
//		} else {
//			userGroupId = "NoGroup";
//		}

		String jsonString = ("{ \"userId\" : \"" + userId + "\" , \"groupId\" : \"" + userGroupId + "\"}");

		log.info("Completing authenticateUser :: returns == " + jsonString);

		return jsonString;

	}

}
