/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月15日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/** 
* @ClassName: SpringUtil 
* @Description: 普通类调用Spring bean对象：
* 说明： 1、此类需要放到App.Java同包或者子包下才能被扫描，否则失效。
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月15日 下午12:09:17 
* @version V1.0 
*/
@Component
public class BootAppBeanUtil implements ApplicationContextAware{

	private static ApplicationContext applicationContext = null;
	 
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		if(BootAppBeanUtil.applicationContext == null){
			BootAppBeanUtil.applicationContext  = applicationContext;
	       }
	      System.out.println("---------------------------------------------------------------------");
	      System.out.println("---------------------------------------------------------------------");
	      System.out.println("---------------com.kfit.base.util.SpringUtil------------------------------------------------------");
	      System.out.println("========ApplicationContext配置成功,在普通类可以通过调用SpringUtils.getAppContext()获取applicationContext对象,applicationContext="+BootAppBeanUtil.applicationContext+"========");
	      System.out.println("---------------------------------------------------------------------");
   }
	
	//获取applicationContext
    public static ApplicationContext getApplicationContext() {
       return applicationContext;
    }
   
    //通过name获取 Bean.
    public static Object getBean(String name){
       return getApplicationContext().getBean(name);
    }
   
    //通过class获取Bean.
    public static <T> T getBean(Class<T> clazz){
       return getApplicationContext().getBean(clazz);
    }
   
    //通过name,以及Clazz返回指定的Bean
    public static <T> T getBean(String name,Class<T> clazz){
       return getApplicationContext().getBean(name, clazz);
    }
	
}
