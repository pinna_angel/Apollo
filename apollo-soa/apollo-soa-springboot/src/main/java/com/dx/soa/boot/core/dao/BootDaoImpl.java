/**
 * Project: apollo-soa-springboot
 * 
 * File Created at 2016年5月13日
 * 
 * Copyright 2015-2016 dx.com Croporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * DongXue software Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with dx.com.
 */
package com.dx.soa.boot.core.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.bioeh.sp.hm.dal.CommonDaoImpl;
import com.bioeh.sp.hm.dal.SimpleJdbc;

/** 
* @ClassName: BootDaoImpl 
* @Description: boot基础类
* @author wuzhenfang(wzfbj2008@163.com)
* @date 2016年5月13日 下午8:20:59 
* @version V1.0 
*/
@SuppressWarnings("unchecked")
public class BootDaoImpl<T extends Serializable> extends CommonDaoImpl implements IBootDao<T> {

	private static final Logger log = Logger.getLogger(BootDaoImpl.class);
			
	private Class<T> clazz;

	protected BootDaoImpl() {
		clazz = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	private DataSource druidDataSource;

	@Bean
	public SimpleJdbc simpleJdbc() {
		SimpleJdbc simpleJdbc = new SimpleJdbc(druidDataSource);
		super.setSimpleJdbc(simpleJdbc);
		return simpleJdbc;
	}

	@Autowired // 这里注解只能写到set方法上，不然druidDataSource获取不到，也是坑了我很久。请留意。
	public void setDruidDataSource(DataSource druidDataSource) {
		this.druidDataSource = druidDataSource;
	}
}
