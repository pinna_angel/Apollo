为应用加上@EnableSwagger2，标示应用支持Swagger, 同时定义API信息，比如标题，版本之类：

DocumentationType.SWAGGER_2 —— 文档类型为Swagger 2
apis —— api过滤，这边表示收集所有带注解Api的API，如果不过滤的话Spring Boot内置的healthcheck, actuator将混乱文档，更多查询RequestHandlerSelectors
paths —— 通过path过滤api，更多查询PathSelectors
directModelSubstitute(LocalDate.class, Date.class), Java8时间类型替换方案
当然还有更多的配置，请查阅用户指南

@Api —— 提示Swagger收集API(还记得配置中的api过滤么)，同时produces会给出正确的类型，否则默认为*.*
@ApiOperation —— 标示一个API，同时自定义这个API
@ApiImplicitParams —— 描述Query，Path，Header，Body params，我这边强加了一个authorization标示用户的token输入
@ApiResponses —— 表示返回状态，如NotFound, NotAuthorized, Forbidden之类

Model定义
为了更好的定义模型，可优化模型如下：

spring boot默认加载文件的路径是 
/META-INF/resources/ 
/resources/ 
/static/ 
/public/

@ApiModel(description = "user information")
@Data
public class UserSummary {

    @ApiModelProperty(value = "user name", required = true)
    private String name;

    @ApiModelProperty(value = "user bob", required = true)
    private LocalDate bob;
}

文档测试
启动服务后：

http://localhost:8080/v2/api-docs 打开文档JSON API
http://localhost:8080/swagger-ui.html 启动Swagger UI查看文档

Method	HTTP request	Description
delete	DELETE /calendars/calendarId/acl/ruleId	Deletes an access control rule.
get	GET /calendars/calendarId/acl/ruleId	Returns an access control rule.
insert	POST /calendars/calendarId/acl	Creates an access control rule.

详细描述每个资源操作

http request形式，如
POST https://www.googleapis.com/calendar/v3/calendars/{calendarId}/clear

http request parameter

这里需要注意的是什么放在Path上，什么放在query parameter上。一般而言，一个资源的子资源放在path上，定位资源的条件放在query paramenter上

Path parameter	Value	Description
calendarId	string	Calendar identifier
…	 	 
Query parameter	Value	Description	Required?
…	 	 	 
Authorization
定义是否需要授权

Request Body (通常只用在Post操作上)，定义方式如下：

Property name	Value	Description	Notes
email	string	user’s email address	 
…	 	 	 
http response representations(针对RPC的接口，如果是REST原则是公用一个resource的描述，只是不同的操作某些参数可以被忽略)，如

{ "id": string, "status": string, "htmlLink": string, "created": datetime }

http reponse parameters(对于资源描述中使用到的参数的解释)

Property name	Value	Description	Notes
home address	string	user’s home address	 
…	 	 	 
