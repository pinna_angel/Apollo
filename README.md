### 1、Apollo 简介


希腊神话传说中太阳神，宙斯和勒托之子，月神和狩猎女神阿尔忒弥斯的孪生哥哥，希腊十二大神只之一，又名福波斯，闪光之神。
最了不起的是他的预言本领。他代表着光明，永远年轻，美貌和谐和沉静。掌管医药，文学，诗歌，音乐等。 月桂树（Laurel）是他的圣木,最喜欢的宠物是海豚和乌鸦.

### 2、apollo-base 作为阿波罗平台框架的基础项目


(1)、apollo-base-commons 项目的基础包
(2)、apollo-base-dal  数据库访问层
(3)、apollo-base-esclient es访问客户端，可以想访问关系型数据库一样访问，带ORM功能
(4)、apollo-base-redisClient redis封装客户端，包括分布式计数器、分布式锁等功能。
(5)、apollo-base-redismq 使用redis做一个简单的消息中间件
(6)、apollo-base-zkClient zk客户端的封装
(7)、apollo-base-kafkaClient kafka客户端封装